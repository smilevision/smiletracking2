# SmileTracking #

### ■ 概要 ###

イベント来場者情報を管理するシステム。  
機能としては、スマートフォンでQRコードを読み取り、イベントへの入場、退場を管理する。  
各ブース毎の受付を管理する。

--- 

### ■ 開発環境 構築手順 ###

* [開発環境 構築手順書](https://bitbucket.org/smilevision/smiletracking2/wiki/%E9%96%8B%E7%99%BA%E7%92%B0%E5%A2%83%20%E6%A7%8B%E7%AF%89%E6%89%8B%E9%A0%86)

### ■ 設計書 ###

* [画面設計書](https://bitbucket.org/smilevision/smiletracking2/src/master/document/application/%E7%94%BB%E9%9D%A2%E8%A8%AD%E8%A8%88%E6%9B%B8.md)

* [データベース設計書](https://bitbucket.org/smilevision/smiletracking2/src/master/document/database/smiletracking.md)

* [データベースER図](https://bitbucket.org/smilevision/smiletracking2/src/master/document/database/smiletracking.png)

### ■ 本番環境 ###
* [サーバー情報](https://bitbucket.org/smilevision/smiletracking2/src/master/document/server.md)
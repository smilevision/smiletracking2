<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['api']], function(){

    Route::get('dashboard/gender_ratio', 'Api\DashboardController@gender_ratio');

    // エントリー削除
    Route::delete('entry', 'Api\EntryController@delete');

    // 入退場一括操作
    Route::post('reception', 'Api\ReceptionController@save');

    // パスワード変更
    Route::post('password_reset', 'Api\PasswordController@reset');

    // パスワード変更（メールアドレスに）
    Route::post('password_reset/email', 'Api\ResetPasswordController@reset');

});

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/qrlogin', 'QrLoginController@login')->name('qrlogin');

Route::get('/', function () {
    return redirect(route('dashboard'));
});

/** ユーザー登録情報 */
Route::get('/user/detail', 'UserController@detail')->name('user.detail');
Route::post('/user/update', 'UserController@update')->name('user.update');

/** ダッシュボード */
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::post('/dashboard', 'DashboardController@find')->name('dashboard.find');

/** イベント管理 */
Route::get('/event', 'EventController@index')->name('event');
Route::post('/event', 'EventController@find')->name('event.find');
Route::get('/event/regist', 'EventController@regist')->name('event.regist');
Route::post('/event/regist', 'EventController@check')->name('event.regist.check');
Route::post('/event/complete', 'EventController@complete')->name('event.regist.complete');
Route::get('/event/thunks', 'EventController@thunks')->name('event.regist.thunks');
Route::get('/event/detail', 'EventController@detail')->name('event.detail');
Route::post('/event/detail', 'EventController@update')->name('event.update');

/** 入退場管理 */
Route::get('/reception', 'ReceptionController@index')->name('reception');
Route::post('/reception', 'ReceptionController@find')->name('reception.find');
Route::get('/reception/regist', 'ReceptionController@regist')->name('reception.regist');
Route::get('/reception/qr_regist', 'ReceptionController@qr_regist')->name('reception.qr_regist');
Route::post('/reception/complete', 'ReceptionController@complete')->name('reception.complete');
Route::post('/reception/qr_complete', 'ReceptionController@qr_complete')->name('reception.qr_complete');

/** 出展企業情報 */
Route::get('/exhibitors', 'ExhibitorsController@index')->name('exhibitors');
Route::post('/exhibitors', 'ExhibitorsController@find')->name('exhibitors.find');
Route::get('/exhibitors/regist', 'ExhibitorsController@regist')->name('exhibitors.regist');
Route::post('/exhibitors/regist', 'ExhibitorsController@check')->name('exhibitors.regist.check');
Route::post('/exhibitors/complete', 'ExhibitorsController@complete')->name('exhibitors.regist.complete');
Route::get('/exhibitors/thunks', 'ExhibitorsController@thunks')->name('exhibitors.regist.thunks');
Route::get('/exhibitors/detail', 'ExhibitorsController@detail')->name('exhibitors.detail');
Route::post('/exhibitors/update', 'ExhibitorsController@update')->name('exhibitors.update');
Route::get('/exhibitors/operation', 'ExhibitorsController@operation')->name('exhibitors.operation');
Route::post('/exhibitors/join', 'ExhibitorsController@join')->name('exhibitors.join');

/** エントリー情報 */
Route::get('/entry', 'EntryController@index')->name('entry');
Route::post('/entry', 'EntryController@find')->name('entry.find');
Route::get('/entry/detail', 'EntryController@detail')->name('entry.detail');
Route::post('/entry/update', 'EntryController@update')->name('entry.update');
Route::get('/entry/regist', 'EntryController@regist')->name('entry.regist');
Route::post('/entry/regist', 'EntryController@check')->name('entry.regist.check');
Route::post('/entry/complete', 'EntryController@complete')->name('entry.regist.complete');
Route::get('/entry/thunks', 'EntryController@thunks')->name('entry.regist.thunks');

/** エントリー情報 */
Route::group(['middleware' => 'auth'], function() {
    Route::get('/entries', 'EntriesController@index')->name('entries');
    Route::post('/entries', 'EntriesController@find')->name('entries.find');
});

/** アンケート情報 */
Route::get('/questionnaire', 'QuestionnaireController@index')->name('questionnaire');
Route::post('/questionnaire', 'QuestionnaireController@find')->name('questionnaire.find');

/** 訪問受付管理 */
Route::get('/visitor', 'VisitorController@index')->name('visitor');
Route::post('/visitor', 'VisitorController@find')->name('visitor.find');
Route::get('/visitor/regist', 'VisitorController@regist')->name('visitor.regist');
Route::get('/visitor/qr_regist', 'VisitorController@qr_regist')->name('visitor.qr_regist');
Route::post('/visitor/complete', 'VisitorController@complete')->name('visitor.complete');
Route::post('/visitor/qr_complete', 'VisitorController@qr_complete')->name('visitor.qr_complete');

Route::get('/qrcode/download', 'QRcodeDownloadController@download')->name('qrcode.download');

Route::get('/csv_download/visit', 'CsvDownloadController@visitors')->name('csv_download.visit');
Route::get('/csv_download/entry', 'CsvDownloadController@entry')->name('csv_download.entry');
Route::get('/csv_download/reception', 'CsvDownloadController@reception')->name('csv_download.reception');
Route::get('/csv_download/questionnaire', 'CsvDownloadController@questionnaire')->name('csv_download.questionnaire');
Route::get('/csv_import/entry', 'CsvImportController@entry')->name('csv_import.entry');

/** 新規エントリー */
Route::get('/new_entry', 'NewEntryController@regist')->name('new_entry.regist');
Route::post('/new_entry/regist', 'NewEntryController@check')->name('new_entry.regist.check');
Route::post('/new_entry/complete', 'NewEntryController@complete')->name('new_entry.regist.complete');
Route::get('/new_entry/thunks', 'NewEntryController@thunks')->name('new_entry.regist.thunks');

/** アンケート管理 */
Route::get('/questionnaire_management', 'QuestionnaireManagementController@index')->name('questionnaire_management');
Route::post('/questionnaire_management', 'QuestionnaireManagementController@find')->name('questionnaire_management.find');
Route::get('/questionnaire_management/detail', 'QuestionnaireManagementController@detail')->name('questionnaire_management.detail');

/** アンケート入力 */
Route::get('/questionnaire', 'QuestionnaireController@regist')->name('questionnaire.regist');
Route::post('/questionnaire/regist', 'QuestionnaireController@check')->name('questionnaire.regist.check');
Route::post('/questionnaire/complete', 'QuestionnaireController@complete')->name('questionnaire.regist.complete');
Route::get('/questionnaire/thunks', 'QuestionnaireController@thunks')->name('questionnaire.regist.thunks');

//Route::get('/event_form', 'EventFormController@index')->name('event_form');
Route::get('/event_form/{slug}', 'EventFormController@index')->name('event_form');
Route::get('/event_form/confirm/{slug}', 'EventFormController@redirect')->name('event_form.confirm');
Route::post('/event_form/confirm/{slug}', 'EventFormController@confirm')->name('event_form.confirm');
Route::get('/event_form/thanks', 'EventFormController@redirect')->name('event_form.thanks');
Route::post('/event_form/thanks/{slug}', 'EventFormController@finish')->name('event_form.thanks');

/** API */
Route::prefix('api')->group(function() {

    Route::delete('exhibitors', 'Api\ExhibitorsApiController@delete');
});

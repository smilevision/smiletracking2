<?php

    return array(
        /**
         * API レスポンスコード・メッセージ
         */
        'status' => array(
            'code' => array(
                'OK' => 200,
                'INVALID_ARGUMENT' => 400,
                'FAILED_PRECONDITION' => 400,
                'OUT_OF_RANGE' => 400,
                'UNAUTHENTICATED' => 401,
                'PERMISSION_DENIED' => 403,
                'NOT_FOUND' => 404,
                'ABORTED' => 409,
                'ALREADY_EXISTS' => 409,
                'RESOURCE_EXHAUSTED' => 429,
                'CANCELLED' => 499,
                'DATA_LOSS' => 500,
                'UNKNOWN' => 500,
                'INTERNAL' => 500,
                'NOT_IMPLEMENTED' => 501,
                'UNAVAILABLE' => 503,
                'DEADLINE_EXCEEDED' => 504
            ),
            'message' => array(
                'OK' => 'OK',
                'INVALID_ARGUMENT' => 'INVALID ARGUMENT',
                'FAILED_PRECONDITION' => 'FAILED PRECONDITION',
                'OUT_OF_RANGE' => 'OUT OF RANGE',
                'UNAUTHENTICATED' => 'UNAUTHENTICATED',
                'PERMISSION_DENIED' => 'PERMISSION DENIED',
                'NOT_FOUND' => 'NOT FOUND',
                'ALREADY_EXISTS' => 'ALREADY EXISTS',
                'RESOURCE_EXHAUSTED' => 'RESOURCE EXHAUSTED',
                'CANCELLED' => 'CANCELLED',
                'DATA_LOSS' => 'DATA LOSS',
                'UNKNOWN' => 'UNKNOWN SERVER ERROR',
                'INTERNAL' => 'INTERNAL SERVER ERROR',
                'NOT_IMPLEMENTED' => 'NOT IMPLEMENTED',
                'UNAVAILABLE' => 'UNAVAILABLE',
                'DEADLINE_EXCEEDED' => 'DEADLINE EXCEEDED'
            )
        ),
        // リスト最大表示件数
        'limit' => array(
            'DASHBOARD_EVENT_LIST' => 5,
            'DASHBOARD_EXHIBITORS_LIST' => 10,
            'DASHBOARD_ENTRY_LIST' => 5,
            'DASHBOARD_RECEPTION_LIST' => 50,
            'DASHBOARD_QUESTIONNAIRE_LIST' => 5,
            'EVENT_LIST' => 5,
            'EXHIBITORS_LIST' => 5,
            'ENTRY_LIST' => 10,
            'RECEPTION_LIST' => 5,
            'VISITOR_LIST' => 100,
            'QUESTIONNAIRE_LIST' => 25,
        ),
        // 権限
        'authority' => array(
            'KANRISHA' => 10,
            'EVENTOR' => 20,
            'EXHIBITORS' => 30,
            'VISITOR' => 40
        ),
        'reception_type' => array(
            'ENTER' => 1,
            'EXIT'  => 9,
        ),
        'event_status' => array(
            'NONE'  => -1,
            'BEFOR' => 1,
            'NOW'   => 2,
            'ENDED' => 9
        ),
        'ACCOUNT_CATEGORY' => array(
            1 => 'vip',
            2 => 'business',
            3 => 'standard',
        ),
        'ACCOUNT_CATEGORY_ID' => array(
            'vip' => 1,
            'business' => 2,
            'standard' => 3,
        ),
    );

?>

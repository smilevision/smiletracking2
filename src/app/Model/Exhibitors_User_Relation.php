<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Exhibitors_User_Relation extends Model
{
    /**
     * テーブル
     */
    protected $table = 'exhibitors_user_relation';

    /**
     * モデルのタイムスタンプを更新をしない
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * イベント情報
     */
    public function exhibitors()
    {
        return $this->hasOne('App\Model\Exhibitors', 'id', 'exhibitors_id');
    }

    /**
     * ユーザー情報情報
     */
    public function user()
    {
        return $this->hasOne('App\Model\User', 'id', 'users_id');
    }

}

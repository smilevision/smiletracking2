<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EntriesItemsOptions extends Model
{
    /**
     * テーブル
     */
    protected $table = 'entries_item_options';

    /**
     * 日付を変形する属性
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}

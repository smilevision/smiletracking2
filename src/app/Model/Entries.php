<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * エントリー情報
 */
class Entries extends Model
{
    /**
     * テーブル
     */
    protected $table = 'entries';

    /**
     * 日付を変形する属性
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * ユーザー
     */
    public function user() {
        return $this->hasOne('App\Model\User', 'id', 'users_id');
    }

    /**
     * イベント
     */
    public function event() {
        return $this->hasOne('App\Model\Event', 'id', 'event_id');
    }

    /**
     * カテゴリ
     */
    public function category() {
        return $this->hasOne('App\Model\Categories', 'id', 'categories_id');
    }
}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Visit;

/**
 * イベント Models
 */
class Event extends Model
{
    /**
     * テーブル
     */
    protected $table = 'events';

    /**
     * 複数代入指定カラム
     */
    protected $fillable = [
        'parent_id',
        'code',
        'name',
        'started_at',
        'ended_at',
        'reserv_started_at',
        'reserv_ended_at',
        'accepting',
    ];

    /**
     * 日付を変形する属性
     *
     * @var array
     */
    protected $dates = [
        'started_at',
        'ended_at',
        'reserv_started_at',
        'reserv_ended_at',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * 出展企業
     */
    public function place()
    {
        return $this->hasOne('App\Model\Place', 'event_id', 'id');
    }

    /**
     * 出展企業
     */
    public function exhibitors()
    {
        return $this->hasMany('App\Model\Exhibitors', 'event_id', 'id');
    }

    /**
     * エントリー情報
     */
    public function entry()
    {
        return $this->hasMany('App\Model\Entry', 'event_id', 'id');
    }

    /**
     * 受付情報
     */
    public function reception()
    {
        return $this->hasMany('App\Model\Reception', 'event_id', 'id')->whereNotNull('enter_at');
    }
}

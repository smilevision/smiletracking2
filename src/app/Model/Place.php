<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * 会場 Model
 */
class Place extends Model
{
    /**
     * テーブル
     */
    protected $table = 'place';

    /**
     * 複数代入指定カラム
     */
    protected $fillable = [
        'event_id',     // イベントID
        'name',         // 会場名
        'zip1',         // 郵便番号
        'zip2',         // 郵便番号
        'address',      // 住所
        'phone',        // 電話番号
    ];

    /**
     * 日付を変形する属性
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

}

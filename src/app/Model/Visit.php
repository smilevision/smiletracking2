<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * 企業訪問受付 Model
 */
class Visit extends Model
{
    /**
     * テーブル
     */
    protected $table = 'visit';

    /**
     * 複数代入指定カラム
     */
    protected $fillable = [
        'exhibitors_id',    // 出展企業ID
        'entry_id',         // エントリーID
        'event_id',         // イベントID
    ];

    /**
     * 日付を変形する属性
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * イベント情報
     */
    public function entry()
    {
        return $this->hasOne('App\Model\Entry', 'id', 'entry_id');
    }

    /**
     * 出展情報
     */
    public function exhibitors()
    {
        return $this->hasOne('App\Model\Exhibitors', 'id', 'exhibitors_id');
    }
}

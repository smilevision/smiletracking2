<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * エントリー項目
 */
class EntriesItems extends Model
{
    /**
     * テーブル
     */
    protected $table = 'entries_item';

    /**
     * 日付を変形する属性
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * イベント
     *
     * @return void
     */
    public function event() {
        return $this->hasOne('App\Model\Event', 'id', 'event_id');
    }

    /**
     * スタイル
     *
     * @return void
     */
    public function style() {
        return $this->hasOne('App\Model\EntriesItemsStyle', 'id', 'style_id');
    }


    public function options() {
        return $this->hasMany('App\Model\EntriesItemsOptions', 'entries_item_id', 'id');
    }
}

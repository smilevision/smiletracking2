<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * 出展企業情報 Model
 */
class Exhibitors extends Model
{
    /**
     * テーブル
     */
    protected $table = 'exhibitors';

    /**
     * 複数代入指定カラム
     */
    protected $fillable = [
        'event_id',     // イベントID
        'code',         // 出展企業コード
        'name',         // 企業・団体名
        'tanto',        // 担当者名
        'zip1',         // 郵便番号
        'zip2',         // 郵便番号
        'address',      // 住所
        'phone',        // 電話番号
        'email',        // メールアドレス
        'note',         // 備考
        'is_join'       // 参加フラグ
    ];

    /**
     * 日付を変形する属性
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * イベント情報
     */
    public function event()
    {
        return $this->hasOne('App\Model\Event', 'id', 'event_id');
    }

    /**
     * イベント情報
     */
    public function relation()
    {
        return $this->hasOne('App\Model\Exhibitors_User_Relation', 'exhibitors_id', 'id');
    }
}

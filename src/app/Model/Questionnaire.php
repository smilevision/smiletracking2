<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * アンケート Model
 */
class Questionnaire extends Model
{
    /**
     * テーブル
     */
    protected $table = 'questionnaire';

    /**
     * 複数代入指定カラム
     */
    protected $fillable = [
        'entry_id'
    ];

    /**
     * 日付を変形する属性
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * イベント情報
     */
    public function entry()
    {
        return $this->hasOne('App\Model\Entry', 'id', 'entry_id');
    }

    /**
     * 受付情報
     */
    public function detail()
    {
        return $this->hasMany('App\Model\QuestionnaireDetail', 'questionnaire_id', 'id');
    }
}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * エントリー Model
 */
class Entry extends Model
{
    /**
     * テーブル
     */
    protected $table = 'entry';

    /**
     * 複数代入指定カラム
     */
    protected $fillable = [
        'event_id',
        'users_id',
        'code',
        'name',
        'kname',
        'gender',
        'birth',
        'zip',
        'address1',
        'address2',
        'address3',
        'email',
        'phone',
        'division',
        'school',
        'gakubu',
        'school_year',
        'graduate_year',
        'introduction',
        'introduction_name',
        'introduction_school_name',
        'note',
        'is_deleted'
    ];

    /**
     * 日付を変形する属性
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * イベント情報
     */
    public function event()
    {
        return $this->hasOne('App\Model\Event', 'id', 'event_id');
    }

    /**
     * イベント情報
     */
    public function user()
    {
        return $this->hasOne('App\Model\User', 'id', 'users_id');
    }

    /**
     * 入退場情報
     */
    public function reception()
    {
        return $this->hasOne('App\Model\Reception', 'entry_id', 'id');
    }
}

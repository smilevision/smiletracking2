<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * ユーザー詳細情報 Model
 */
class UserDetail extends Model
{
    /**
     * テーブル
     */
    protected $table = 'users_detail';

    /**
     * 複数代入指定カラム
     */
    protected $fillable = [
        'event_id',     // イベントID
        'name',         // 担当者名
        'email',        // メールアドレス
    ];

    /**
     * 日付を変形する属性
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}

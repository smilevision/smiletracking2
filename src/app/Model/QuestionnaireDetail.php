<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class QuestionnaireDetail extends Model
{
    /**
     * テーブル
     */
    protected $table = 'questionnaire_detail';

    /**
     * 複数代入指定カラム
     */
    protected $fillable = [
        'questionnaire_id',
        'no',
        'value'
    ];

    /**
     * 日付を変形する属性
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

}

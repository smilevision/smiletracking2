<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * イベント受付 Model
 */
class Reception extends Model
{
    /**
     * テーブル
     */
    protected $table = 'reception';

    /**
     * 複数代入指定カラム
     */
    protected $fillable = [
        'entry_id',
        'event_id',
        'enter_at',
        'exit_at',
        'is_deleted'
    ];

    /**
     * 日付を変形する属性
     *
     * @var array
     */
    protected $dates = [
        'enter_at',
        'exit_at',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * イベント情報
     */
    public function entry()
    {
        return $this->hasOne('App\Model\Entry', 'id', 'entry_id');
    }

    /**
     * イベント情報
     */
    public function event()
    {
        return $this->hasOne('App\Model\Event', 'id', 'event_id');
    }

}

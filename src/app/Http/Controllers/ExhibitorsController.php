<?php

namespace App\Http\Controllers;

use stdClass;
use Exception;
use App\Model\User;
use App\Model\Event;
use App\Model\Exhibitors;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Mail\RegistExhibitor;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Model\Exhibitors_User_Relation;

/**
 * 出展企業情報コントローラ
 */
class ExhibitorsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * 出展企業情報 一覧画面
     *
     * @param Request $request
     * @return void
     */
    public function index(Request $request) {

        Log::info('['.Auth::user()->id.'] start ExhibitorsController index.');

        //　抽出条件
        $conditions = array();

        // 検索条件のアイテム
        $findItems = new stdClass;
        $findItems->event = Event::whereNull('is_view')->where('is_deleted', false)->get();

        if(empty($request->event_id)) {
            // 出展企業一覧
            $exhibitors = null;

            // 対象イベント
            $target_event = new Event;
        } else {
            Log::info('event_id : '.$request->event_id);
            $conditions['search_event'] = $request->event_id;

            // 出展企業一覧
            $query = Exhibitors::select('exhibitors.*', DB::raw('(select count(*) from visit where visit.exhibitors_id = exhibitors.id) as visit_count'));
            $query->whereNull('exhibitors.deleted_at');
            $query->where('exhibitors.event_id', $request->event_id);

            if(!empty($request->sort_item) && !empty($request->sort_mode)) {
                $column = $request->sort_item;
                $sort_type = $request->sort_mode;
            } else {
                $column = 'visit_count';
                $sort_type = 'desc';
            }
            $query->orderBy($column, $sort_type);
            $exhibitors = $query->paginate(config('const.limit.EXHIBITORS_LIST'));

            if(!empty($request->sort_item) && !empty($request->sort_mode)) {
                $query_string = array();
                $query_string['event_id'] = $request->event_id;
                $query_string['sort_mode'] = $request->sort_mode;
                $query_string['sort_item'] = $request->sort_item;
                $exhibitors->appends($query_string);
            } else {
                $query_string = array();
                $query_string['event_id'] = $request->event_id;
                $exhibitors->appends($query_string);
            }
            //dd($query->toSql(), $query->getBindings());

            // 対象イベント
            $target_event = Event::where('is_deleted', false)->where('id', $request->event_id)->first();
        }

        return view('exhibitors.index', compact('exhibitors', 'target_event', 'findItems', 'conditions'));
    }

    /**
     * 出展企業情報 検索処理
     *
     * @param Request $request
     * @return void
     */
    public function find(Request $request) {

        Log::info('['.Auth::user()->id.'] start ExhibitorsController find.');

        //　抽出条件
        $conditions = array();
        if(!empty($request->search_event)) {
            $conditions['search_event'] = $request->search_event;
        } elseif(!empty($request->event_id)) {
            $conditions['search_event'] = $request->event_id;
            $request->search_event = $request->event_id;
        }

        // 検索条件のアイテム
        $findItems = new stdClass;
        $findItems->event = Event::whereNull('is_view')->where('is_deleted', false)->get();

        // 出展企業一覧
        if(!empty($request->search_event)) {
            // 出展企業一覧
            $query = Exhibitors::select('exhibitors.*', DB::raw('(select count(*) from visit where visit.exhibitors_id = exhibitors.id) as visit_count'));
            //$query->where('exhibitors.is_deleted', false);
            $query->whereNull('exhibitors.deleted_at');
            $query->where('exhibitors.event_id', $request->search_event);
            $query->orderBy('visit_count', 'desc');
            $exhibitors = $query->paginate(config('const.limit.EXHIBITORS_LIST'));
            $exhibitors->params = array('event_id' => $request->search_event);  // クエリストリングのパラメータ
            //dd($query->toSql(), $query->getBindings());
        } else {
            $exhibitors = null;
        }

        // 対象イベント
        if(!empty($request->search_event)) {
            $target_event = Event::where('is_deleted', false)->where('id', $request->search_event)->first();
        } else {
            $target_event = new Event;
        }

        return view('exhibitors.index', compact('exhibitors', 'target_event', 'findItems', 'conditions'));
    }

    /**
     * 出展企業 詳細情報
     *
     * @param Request $request
     * @return void
     */
    public function detail(Request $request) {

        Log::info('['.Auth::user()->id.'] start ExhibitorsController detail. [id:'.$request->id.']');

        $exhibitors = Exhibitors::where('id', $request->id)->where('is_deleted', false)->first();

        if(empty($request->status)) {
            $status = null;
        } else {
            $status = $request->status;
        }

        return view('exhibitors.detail', compact('exhibitors', 'status'));
    }

    /**
     * 出展企業 更新処理
     *
     * @param Request $request
     * @return void
     */
    public function update(Request $request) {

        Log::info('['.Auth::user()->id.'] start ExhibitorsController update. [id:'.$request->id.']');

        // トランザクション開始
        DB::beginTransaction();

        try {

            // 出展企業情報の更新
            $exhibitors = Exhibitors::where('id', $request->id)->where('is_deleted', false)->first();

            $exhibitors->is_join = $request->is_join;
            $exhibitors->name = $request->name;
            $exhibitors->tanto = $request->tanto;
            $exhibitors->zip1 = $request->zip1;
            $exhibitors->zip2 = $request->zip2;
            $exhibitors->address = $request->address;
            $exhibitors->phone = $request->phone;
            $exhibitors->email = $request->email;
            $exhibitors->note = $request->note;

            $exhibitors->save();

            // ユーザー情報の更新
            $relation = Exhibitors_User_Relation::where('exhibitors_id', $exhibitors->id)->first();
            $user = User::where('id', $relation->users_id)->first();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->save();

            DB::commit();

            return redirect()->route('exhibitors.detail', array('id'=>$request->id, 'status'=>'complete'));

        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            DB::rollback();
            return redirect()->route('exhibitors.detail', array('id'=>$request->id, 'status'=>'error'));
        }
    }

    /**
     * 出展企業 新規登録画面
     *
     * @param Request $request
     * @return void
     */
    public function regist(Request $request) {
        $data = $request;
        return view('exhibitors.regist', compact('data'));
    }

    /**
     * 出展企業 新規登録確認画面
     *
     * @param Request $request
     * @return void
     */
    public function check(Request $request) {

        // バリデーション 実行
        $this->validate($request, $this->validation_rule(), $this->validation_message());

        $data = $request;
        $data->code = $this->createCode();
        return view('exhibitors.regist_check', compact('data'));
    }

    /**
     * 出展企業 登録処理
     *
     * @param Request $request
     * @return void
     */
    public function complete(Request $request) {

        Log::info('['.Auth::user()->id.'] start ExhibitorsController complete.');

        // トランザクション開始
        DB::beginTransaction();

        try {

            Log::info('event id : '.$request->event_id);

            // コード作成
            $code = $this->createCode();

            // ユーザー登録
            $user = new User;
            $user->is_authority = config('const.authority.EXHIBITORS');
            $user->code = $code;
            $user->uuid = (string) Str::uuid();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->email);
            $user->save();

            // 出展企業情報登録
            $exhibitors = new Exhibitors;
            $request->merge(array('code' => $code, 'users_id' => $user->id));
            $exhibitors->fill($request->all())->save();

            // リレーション登録
            $relation = new Exhibitors_User_Relation;
            $relation->exhibitors_id = $exhibitors->id;
            $relation->users_id = $user->id;
            $relation->save();

            DB::commit();

            // メール送信
            Mail::to($user->email)->send(new RegistExhibitor($user));

            return redirect()->route('exhibitors.regist.thunks', array('event_id'=>$request->event_id));
        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            DB::rollback();
        }
    }

    /**
     * 出展企業 新規登録完了
     *
     * @param Request $request
     * @return void
     */
    public function thunks(Request $request) {
        $event_id = $request->event_id;
        return view('exhibitors.regist_thunks', compact('event_id'));
    }

    /**
     * 一括出展登録画面
     *
     * @param Request $request
     * @return void
     */
    public function operation(Request $request) {

        Log::info('['.Auth::user()->id.'] start ExhibitorsController operation.');

        $query = Exhibitors::where('is_deleted', false);
        $query->where('event_id', $request->event_id);
        $query->where('is_join', false);
        $query->orderBy('created_at', 'desc');
        $exhibitors = $query->get();

        $target_event = Event::where('is_deleted', false)->where('id', $request->event_id)->first();

        return view('exhibitors.operation', compact('exhibitors', 'target_event'));
    }

    /**
     * 一括出展登録処理
     *
     * @param Request $request
     * @return void
     */
    public function join(Request $request) {
        try {
            $ids = $request->exhibitor_ids;

            if(empty($ids)) {
                return back();
            }

            DB::beginTransaction();

            foreach ($ids as $key => $id) {
                $exhibitor = Exhibitors::where('id', $id)->first();
                $exhibitor->is_join = true;
                $exhibitor->save();
            }

            DB::commit();

            return redirect()->route('exhibitors', array('event_id'=>$request->event_id));

        } catch (Exception $e) {
            DB::rollback();
            Log::error($e->getMessage());
            return back();
        }
    }

    /**
     * 出展企業コード発行
     *
     * @return string 予約コード
     */
    private function createCode() {
        $head_length = 3;
        $head = substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, $head_length);
        $code_length = 4;
        $code = substr(str_shuffle('1234567890'), 0, $code_length);
        return $head . $code;
    }

    /**
     * バリデーション ルール
     * @return string[]
     */
    private function validation_rule() {
        $rule = [
            'name'       => 'required|max:1024',
            'tanto'      => 'nullable|max:128',
            'zip1'       => 'nullable|numeric|max:9999',
            'zip2'       => 'nullable|numeric|max:9999',
            'address'    => 'nullable|max:1024',
            'phone'      => 'nullable|max:24',
            'email'      => 'nullable|max:128',
        ];

        return $rule;
    }

    /**
     * バリデーションメッセージ
     * @return string[]
     */
    private function validation_message() {
        return [
            'name.required'       => '企業名が入力されていません。',
            'name.max'            => '企業名の最大文字列数を超えています。',
            'tanto.max'           => '担当者の最大文字列数を超えています。',
            'zip1.numeric'        => '郵便番号は数字のみ入力可能です。',
            'zip1.max'            => '郵便番号の最大文字列数を超えています。',
            'zip2.numeric'        => '郵便番号は数字のみ入力可能です。',
            'zip2.max'            => '郵便番号の最大文字列数を超えています。',
            'address.max'         => '住所の最大数を超えています。',
            'phone.max'           => '電話番号の最大文字列数を超えています。',
            'email.max'           => 'メールアドレスの最大文字列数を超えています。',
        ];
    }
}

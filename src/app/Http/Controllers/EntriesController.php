<?php

namespace App\Http\Controllers;

use App\Model\Entries;
use App\Model\EntriesItems;
use stdClass;
use App\Model\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

/**
 * WEB申し込み
 */
class EntriesController extends Controller
{
    /**
     * 申し込み済み一覧
     *
     * @return void
     */
    public function index(Request $request) {

        Log::info('['.Auth::user()->id.'] index@EntriesController start.');

        $condition = null;
        $data = new stdClass;

        // 検索条件のアイテム
        $data->findItems = new stdClass;
        $data->findItems->event = Event::where('is_deleted', false)->get();
        $findItems = $data->findItems;

        $data->entries_items = EntriesItems::whereNull('deleted_at')->where('is_list_view', true)->where('event_id', 1)->get();
        $data->entries = Entries::whereNull('deleted_at')->get();
        foreach ($data->entries as $key => $entries) {
            $data->entries[$key]->values = json_decode($entries->values, true);
        }

        $request->event_id = 1;
        if(empty($request->event_id)) {
            // エントリーリスト
            $entries = null;

            // 対象イベント
            $target_event = new Event;
        } else {
            $conditions['search_event'] = $request->event_id;

            // エントリー一覧
            $query = Entry::where('is_deleted', false);
            $query->where('event_id', $request->event_id);
            $query->orderBy('kname', 'asc');
            $entries = $query->paginate(config('const.limit.ENTRY_LIST'));
            $entries->params = array('event_id' => $request->event_id);  // クエリストリングのパラメータ

            // 対象イベント
            $target_event = Event::where('is_deleted', false)->where('id', $request->event_id)->first();
        }

        return view('entries.index', compact('entries', 'target_event', 'findItems', 'conditions'));
    }

    /**
     * 申し込み済み内容詳細
     *
     * @param Request $request
     * @return void
     */
    public function detail(Request $request) {

    }


    public function insert(Request $request) {

    }


    public function update(Request $request) {

    }


    public function delete(Request $request) {

    }

    /**
     * 申し込み画面の表示
     *
     * @param Request $request
     * @return void
     */
    public function view(Request $request) {

    }

}

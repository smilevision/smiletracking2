<?php

namespace App\Http\Controllers;

use App\Model\Event;
use App\Model\Place;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use stdClass;

/**
 * イベント Controller
 */
class EventController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * イベント一覧画面
     *
     * @return void
     */
    public function index() {

        Log::info('['.Auth::user()->id.'] start EventController index.');

        //　抽出条件
        $conditions = array();

        // イベント取得
        $query = Event::where('is_deleted', false);
        $events = $query->get();

        $now = date('Y-m-d H:i');   // 現在日時

        foreach ($events as $key => $event) {

            $events[$key]->exhibitors_count = count($event->exhibitors);
            $events[$key]->entry_count = count($event->entry);
            $events[$key]->reception_count = count($event->reception);
            if($events[$key]->entry_count > 0) {
                $events[$key]->sanka_rate = ($events[$key]->reception_count / $events[$key]->entry_count) * 100;
            } else {
                $events[$key]->sanka_rate = 0;
            }

            // 状態
            if(empty($event->started_at)) {
                $events[$key]->status = '未定';
            } else {
                if($event->started_at->format('Y-m-d H:i') > $now) {
                    $events[$key]->status = '開催前';
                } else {
                    if($now < $event->ended_at->format('Y-m-d H:i')) {
                        $events[$key]->status = '開催中';
                    } else {
                        $events[$key]->status = '終了';
                    }
                }
            }

            // 応募URL
            $events[$key]->event_form_url = new stdClass;
            $slug = Crypt::encrypt('category=vip&code='.$event->code);
            $events[$key]->event_form_url->vip = route('event_form', array($slug));
            $slug = Crypt::encrypt('category=business&code='.$event->code);
            $events[$key]->event_form_url->business = route('event_form', array($slug));
            $slug = Crypt::encrypt('category=standard&code='.$event->code);
            $events[$key]->event_form_url->standard = route('event_form', array($slug));
            //$events[$key]->event_form_url = route('event_form', array('code'=>Crypt::encrypt($event->code)));
        }

        return view('event.index', compact('events', 'conditions'));
    }

    /**
     * イベント検索処理
     *
     * @param Request $request
     * @return void
     */
    public function find(Request $request) {

        Log::info('['.Auth::user()->id.'] start EventController index.');

        //　抽出条件
        $conditions = array();

        $now = date('Y-m-d H:i');   // 現在日時

        // イベント取得
        $query = Event::where('events.is_deleted', false);
        $query->leftJoin('place', 'place.event_id', '=', 'events.id');

        // 開催名
        if(!empty($request->search_name)) {
            $conditions['search_name'] = $request->search_name;
            $keyword = '%'.$request->search_name.'%';
            $query->where('events.name', 'like', $keyword);
        }

        // 開催日時
        if(!empty($request->search_started_at) || !empty($request->search_ended_at)) {
            $conditions['search_started_at'] = $request->search_started_at;
            $conditions['search_ended_at'] = $request->search_ended_at;
            if(!empty($request->search_started_at) && !empty($request->search_ended_at)) {
                $from = date("Y-m-d", strtotime($request->search_started_at));
                $to = date("Y-m-d", strtotime('+1 day', strtotime($request->search_ended_at)));
                $keyword = array(
                    $from,
                    $to
                );
                $query->where(function($query) use ($keyword){
                    $query->whereBetween('events.started_at', $keyword);
                    $query->orWhereBetween('events.ended_at', $keyword);
                });
            } else if(!empty($request->search_started_at) && empty($request->search_ended_at)) {
                $keyword = $request->search_started_at;
                $query->where(function($query) use ($keyword){
                    $query->where('events.started_at', '>=', $keyword);
                    $query->orWhere('events.ended_at', '>=', $keyword);
                });
            } else if(empty($request->search_started_at) && !empty($request->search_ended_at)) {
                $keyword = $request->search_ended_at;
                $query->where(function($query) use ($keyword){
                    $query->Where('events.started_at', '<=', $keyword);
                    $query->orWhere('events.ended_at', '<=', $keyword);
                });
            }
        }

        // 会場
        if(!empty($request->search_place_name)) {
            $conditions['search_place_name'] = $request->search_place_name;
            $keyword = '%'.$request->search_place_name.'%';
            $query->where('place.name', 'like', $keyword);
        }

        // 状態
        if(!empty($request->search_status)) {
            $conditions['search_status'] = $request->search_status;
            switch ($request->search_status) {
                case config('const.event_status.NONE'):
                    $query->whereNull('events.started_at');
                    break;

                case config('const.event_status.BEFOR'):
                    $query->where('events.started_at', '>=', $now);
                    break;

                case config('const.event_status.NOW'):
                    $query->where('events.started_at', '<=', $now);
                    $query->where('events.ended_at', '>=', $now);
                    break;

                case config('const.event_status.ENDED'):
                    $query->where('events.ended_at', '<', $now);
                    break;

                default:
                    break;
            }
        }

        $events = $query->get();

        foreach ($events as $key => $event) {

            $events[$key]->exhibitors_count = count($event->exhibitors);
            $events[$key]->entry_count = count($event->entry);
            $events[$key]->reception_count = count($event->reception);
            if($events[$key]->entry_count > 0) {
                $events[$key]->sanka_rate = ($events[$key]->reception_count / $events[$key]->entry_count) * 100;
            } else {
                $events[$key]->sanka_rate = 0;
            }

            // 状態
            if(empty($event->started_at)) {
                $events[$key]->status = '未定';
            } else {
                if($event->started_at->format('Y-m-d H:i') > $now) {
                    $events[$key]->status = '開催前';
                } else {
                    if($now < $event->ended_at->format('Y-m-d H:i')) {
                        $events[$key]->status = '開催中';
                    } else {
                        $events[$key]->status = '終了';
                    }
                }
            }
        }

        return view('event.index', compact('events', 'conditions'));
    }

    /**
     * イベント作成画面
     *
     * @return void
     */
    public function regist() {

        Log::info('['.Auth::user()->id.'] start EventController index.');

        return view('event.regist');
    }

    /**
     * イベント作成 確認画面
     */
    public function check(Request $request) {

        Log::info('['.Auth::user()->id.'] start EventController check.');

        // バリデーション 実行
        $this->validate($request, $this->validation_rule(), $this->validation_message());

        $data = $request;
        return view('event.regist_check', compact('data'));
    }

    /**
     * イベント作成 登録処理
     *
     * @param Request $request
     * @return void
     */
    public function complete(Request $request) {

        Log::info('['.Auth::user()->id.'] start EventController check.');

        // バリデーション 実行
        $this->validate($request, $this->validation_rule(), $this->validation_message());

        // トランザクション開始
        DB::beginTransaction();

        try {

            // コード作成
            $code = $this->createCode();

            // ユーザー登録
            $event = new Event;
            $event->code = $code;
            $event->name = $request->name;
            $event->started_at = empty($request->started_at)?null:date('Y-m-d H:i:s', strtotime($request->started_at));
            $event->ended_at = empty($request->ended_at)?null:date('Y-m-d H:i:s', strtotime($request->ended_at));
            $event->reserv_started_at = empty($request->reserv_started_at)?null:date('Y-m-d H:i:s', strtotime($request->reserv_started_at));
            $event->reserv_ended_at = empty($request->reserv_ended_at)?null:date('Y-m-d H:i:s', strtotime($request->reserv_ended_at));
            $event->accepting = $request->accepting;
            $event->tanto_name = $request->tanto_name;
            $event->tanto_email = $request->tanto_email;
            $event->tanto_contact = $request->tanto_contact;
            $event->save();

            $place = new Place;
            $place->event_id = $event->id;
            $place->name = $request->place_name;
            $place->address = $request->place_address;
            $place->phone = $request->place_phone;
            $place->save();

            DB::commit();

            return redirect()->route('event.regist.thunks', array('event_id'=>$event->id));

        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            DB::rollback();
        }
    }

    /**
     * イベント作成 完了画面
     *
     * @param Request $request
     * @return void
     */
    public function thunks(Request $request) {
        $event_id = $request->event_id;
        return view('event.regist_thunks', compact('event_id'));
    }

    /**
     * イベント概要画面
     *
     * @param Request $request
     * @return void
     */
    public function detail(Request $request) {

        Log::info('['.Auth::user()->id.'] start EventController delete. [event_id:'.$request->id.']');

        $event = Event::where('is_deleted', false)->where('id', $request->id)->first();

        if(empty($request->status)) {
            $status = null;
        } else {
            $status = $request->status;
        }

        return view('event.detail', compact('event', 'status'));
    }

    /**
     * イベント概要 更新処理
     *
     * @param Request $request
     * @return void
     */
    public function update(Request $request) {

        Log::info('['.Auth::user()->id.'] start EventController update. [event_id:'.$request->id.']');

        // トランザクション開始
        DB::beginTransaction();

        try {

            // イベント情報の更新
            $event = Event::where('id', $request->id)->where('is_deleted', false)->first();
            $event->is_view = $request->is_view;
            $event->name = $request->name;
            $event->started_at = empty($request->started_at)?null:date('Y-m-d H:i:s', strtotime($request->started_at));
            $event->ended_at = empty($request->ended_at)?null:date('Y-m-d H:i:s', strtotime($request->ended_at));
            $event->reserv_started_at = empty($request->reserv_started_at)?null:date('Y-m-d H:i:s', strtotime($request->reserv_started_at));
            $event->reserv_ended_at = empty($request->reserv_ended_at)?null:date('Y-m-d H:i:s', strtotime($request->reserv_ended_at));
            $event->accepting = $request->accepting;
            $event->tanto_name = $request->tanto_name;
            $event->tanto_email = $request->tanto_email;
            $event->tanto_contact = $request->tanto_contact;
            $event->save();

            // イベント会場の更新
            $place = Place::where('event_id', $event->id)->first();
            $place->name = $request->place_name;
            $place->address = $request->place_address;
            $place->phone = $request->place_phone;
            $place->save();

            DB::commit();

            return redirect()->route('event.detail', array('id'=>$request->id, 'status'=>'complete'));

        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            DB::rollback();
            return redirect()->route('event.detail', array('id'=>$request->id, 'status'=>'error'));
        }
    }

    /**
     * 出展企業コード発行
     *
     * @return string 予約コード
     */
    private function createCode() {
        $head_length = 3;
        $head = substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, $head_length);
        $code_length = 4;
        $code = substr(str_shuffle('1234567890'), 0, $code_length);
        return $head . $code;
    }

    /**
     * バリデーション ルール
     * @return string[]
     */
    private function validation_rule() {
        $rule = [
            'name'          => 'required|max:1024',
            'place_name'    => 'required|max:128',
            'place_address' => 'nullable|max:2048',
            'place_phone'   => 'nullable|max:24',
            'tanto_name'    => 'nullable|max:128',
            'tanto_email'   => 'nullable|max:128',
            'tanto_contact' => 'nullable|max:2048',
        ];

        return $rule;
    }

    /**
     * バリデーションメッセージ
     * @return string[]
     */
    private function validation_message() {
        return [
            'name.required'       => '開催名が入力されていません。',
            'name.max'            => '開催名の最大文字列数を超えています。',
            'place_name.required' => '開催名が入力されていません。',
            'place_name.max'      => '開催名の最大文字列数を超えています。',
            'tanto.max'           => '担当者の最大文字列数を超えています。',
            'zip1.numeric'        => '郵便番号は数字のみ入力可能です。',
            'zip1.max'            => '郵便番号の最大文字列数を超えています。',
            'zip2.numeric'        => '郵便番号は数字のみ入力可能です。',
            'zip2.max'            => '郵便番号の最大文字列数を超えています。',
            'address.max'         => '住所の最大数を超えています。',
            'phone.max'           => '電話番号の最大文字列数を超えています。',
            'email.max'           => 'メールアドレスの最大文字列数を超えています。',
        ];
    }
}

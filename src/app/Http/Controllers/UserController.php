<?php

namespace App\Http\Controllers;

use App\Model\Entry;
use App\Model\Exhibitors;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

/**
 * 運営者コントロール
 */
class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * 運営者 詳細画面
     *
     * @return void
     */
    public function detail(Request $request) {

        Log::info('['.Auth::user()->id.'] start UserController detail.');

        $user = Auth::user();

        // 情報取得
        if(Auth::user()->is_authority == config('const.authority.EXHIBITORS')) {
            $user->zip1 = $user->relation->exhibitors->zip1;
            $user->zip2 = $user->relation->exhibitors->zip2;
            $user->address = $user->relation->exhibitors->address;
            $user->phone = $user->relation->exhibitors->phone;
            $user->tanto = $user->relation->exhibitors->tanto;
            $user->note = $user->relation->exhibitors->note;
        }

        if(empty($request->status)) {
            $status = null;
        } else {
            $status = $request->status;
        }

        return view('user.detail', compact('user', 'status'));
    }

    /**
     * 運営者 情報更新
     *
     * @param Request $request
     * @return void
     */
    public function update(Request $request) {

        DB::beginTransaction();

        try {

            $user = Auth::user();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->save();

            // 企業情報
            if(Auth::user()->is_authority == config('const.authority.EXHIBITORS')) {
                $exhibitors = Exhibitors::where('code', $user->code)->where('is_deleted', false)->first();
                $exhibitors->zip1 = $request->zip1;
                $exhibitors->zip2 = $request->zip2;
                $exhibitors->address = $request->address;
                $exhibitors->phone = $request->phone;
                $exhibitors->tanto = $request->tanto;
                $exhibitors->note = $request->note;
                $exhibitors->save();
            }
            DB::commit();

            return redirect()->route('user.detail', array('status'=>'complete'));

        } catch (Exception $e) {
            Log::error($e->getMessage());
            // ロールバック
            DB::rollback();
            return redirect()->route('user.detail', array('status'=>'error'));
        }

    }
}

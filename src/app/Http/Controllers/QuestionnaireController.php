<?php

namespace App\Http\Controllers;

use App\Model\Entry;
use App\Model\Questionnaire;
use Illuminate\Http\Request;
use App\Model\QuestionnaireDetail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * アンケート Controller
 */
class QuestionnaireController extends Controller
{
    /**
     * WEB予約者情報  新規登録画面
     *
     * @param Request $request
     * @return void
     */
    public function regist(Request $request) {

        Log::info('start QuestionnaireController regist.');
        $data = $request;
        return view('questionnaire.regist', compact('data'));
    }

    /**
     * WEB予約者情報  新規登録確認画面
     *
     * @param Request $request
     * @return void
     */
    public function check(Request $request) {

        // バリデーション 実行
        $this->validate($request, $this->validation_rule(), $this->validation_message());

        $data = $request;
        $data->event_id = 6;
        $data->code = $this->createCode();
        return view('questionnaire.regist_check', compact('data'));
    }

    /**
     * WEB予約者情報  登録処理
     *
     * @param Request $request
     * @return void
     */
    public function complete(Request $request) {

        Log::info('start QuestionnaireController complete.');

        // バリデーション 実行
        $this->validate($request, $this->validation_rule(), $this->validation_message());

        // トランザクション開始
        DB::beginTransaction();

        try {

            //Log::info('entry_id : '.$request->event_id);
            Log::info('entry_id : '.'6');

            $entry = Entry::where('code', $request->code)->where('is_deleted', false)->first();

            if(empty($entry)) {
                // 未エントリー
                return redirect()->back()->withInput()->with('error', '予約者コードが予約されていません。');
            }

            //$chk = Questionnaire::where('entry_id', $entry->id)->where('event_id', $request->event_id)->first();
            $chk = Questionnaire::where('entry_id', $entry->id)->where('event_id', 6)->first();

            if(!empty($chk)) {
                // 未エントリー
                return redirect()->back()->withInput()->with('error', '既にアンケートに回答されています。');
            }

            $Questionnaire = new Questionnaire;
            //$Questionnaire->event_id = $request->event_id;
            $Questionnaire->event_id = 6;
            $Questionnaire->entry_id = $entry->id;
            $Questionnaire->save();

            $data = array(
                array('title'=>'q1', 'value'=>$request->q1),
                array('title'=>'q2_1', 'value'=>$request->q2_1),
                array('title'=>'q2_2', 'value'=>$request->q2_2),
                array('title'=>'q2_3', 'value'=>$request->q2_3),
                array('title'=>'q3_1', 'value'=>$request->q3_1),
                array('title'=>'q3_2', 'value'=>$request->q3_2),
                array('title'=>'q3_3', 'value'=>$request->q3_3),
                array('title'=>'q3_4', 'value'=>$request->q3_4),
                array('title'=>'q3_5', 'value'=>$request->q3_5),
                array('title'=>'q3_6', 'value'=>$request->q3_6),
                array('title'=>'q3_7', 'value'=>$request->q3_7),
                array('title'=>'q3_8', 'value'=>$request->q3_8),
                array('title'=>'q3_9', 'value'=>$request->q3_9),
                array('title'=>'q3_10', 'value'=>$request->q3_10),
                array('title'=>'q3_11', 'value'=>$request->q3_11),
                array('title'=>'q3_12', 'value'=>$request->q3_12),
                array('title'=>'q3_13', 'value'=>$request->q3_13),
                array('title'=>'q3_14', 'value'=>$request->q3_14),
                array('title'=>'q3_15', 'value'=>$request->q3_15),
                array('title'=>'q3_16', 'value'=>$request->q3_16),
                array('title'=>'q3_17', 'value'=>$request->q3_17),
                array('title'=>'q3_note', 'value'=>$request->q3_note),
                array('title'=>'q4_1', 'value'=>$request->q4_1),
                array('title'=>'q4_2', 'value'=>$request->q4_2),
                array('title'=>'q4_3', 'value'=>$request->q4_3),
                array('title'=>'q4_4', 'value'=>$request->q4_4),
                array('title'=>'q4_5', 'value'=>$request->q4_5),
                array('title'=>'q4_6', 'value'=>$request->q4_6),
                array('title'=>'q4_7', 'value'=>$request->q4_7),
                array('title'=>'q4_8', 'value'=>$request->q4_8),
                array('title'=>'q4_note', 'value'=>$request->q4_note),
                array('title'=>'q5_1', 'value'=>$request->q5_1),
                array('title'=>'q5_2', 'value'=>$request->q5_2),
                array('title'=>'q5_3', 'value'=>$request->q5_3),
                array('title'=>'q5_4', 'value'=>$request->q5_4),
                array('title'=>'q5_5', 'value'=>$request->q5_5),
                array('title'=>'q5_6', 'value'=>$request->q5_6),
                array('title'=>'q5_7', 'value'=>$request->q5_7),
                array('title'=>'q5_note', 'value'=>$request->q5_note),
                array('title'=>'amazon_gift', 'value'=>$request->amazon_gift),
                array('title'=>'note', 'value'=>$request->note),
            );

            foreach ($data as $key => $item) {
                $detail = new QuestionnaireDetail;
                $detail->questionnaire_id = $Questionnaire->id;
                $detail->title = $item['title'];
                $detail->value = $item['value'];
                $detail->save();
            }

            // commit
            DB::commit();

            return redirect()->route('questionnaire.regist.thunks', array('event_id'=>$request->event_id));
        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            // ロールバック
            DB::rollback();
        }
    }

    /**
     * WEB予約者情報  新規登録完了
     *
     * @param Request $request
     * @return void
     */
    public function thunks(Request $request) {
        $event_id = $request->event_id;
        return view('questionnaire.regist_thunks', compact('event_id'));
    }

    /**
     * バリデーション ルール
     * @return string[]
     */
    private function validation_rule() {
        $rule = [
            'code'          => 'required|max:255',
            'q1'            => 'required',
            'q2_1'          =>'required_without_all:q2_2,q2_3',
            'q3_1'          =>'required_without_all:q3_2,q3_3,q3_4,q3_5,q3_6,q3_7,q3_8,q3_9,q3_10,q3_11,q3_12,q3_13,q3_14,q3_15,q3_16,q3_17',
            'q4_1'          =>'required_without_all:q4_2,q4_3,q4_4,q4_5,q4_6,q4_7,q4_8',
            'q5_1'          =>'required_without_all:q5_2,q5_3,q5_4,q5_5,q5_6,q5_7',
        ];

        return $rule;
    }

    /**
     * バリデーションメッセージ
     * @return string[]
     */
    private function validation_message() {
        return [
            'code.required'             => '予約コードが入力されていません。',
            'code.max'                  => '予約コードの最大文字列数を超えています。',
            'q1.*.required'             => '選択してください。',
            'q2_1.required_without_all' => '入力してください。',
            'q3_1.required_without_all' => '選択してください。',
            'q4_1.required_without_all' => '選択してください。',
            'q5_1.required_without_all' => '選択してください。',
        ];
    }
}

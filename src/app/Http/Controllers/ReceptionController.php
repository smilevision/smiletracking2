<?php

namespace App\Http\Controllers;

use stdClass;
use App\Model\User;
use App\Model\Entry;
use App\Model\Event;
use App\Model\Visit;
use App\Model\Reception;
use App\Model\Questionnaire;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

/**
 * 入退場管理 Controller
 */
class ReceptionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * 入退場情報一覧画面
     *
     * @return void
     */
    public function index(Request $request) {

        Log::info('['.Auth::user()->id.'] start ReceptionController index.');

        //　抽出条件
        $conditions = array();

        // 検索条件のアイテム
        $findItems = new stdClass;
        $findItems->event = Event::whereNull('is_view')->where('is_deleted', false)->get();

        //$request->merge(array('event_id' => 1));

        if(empty($request->event_id)) {
            // エントリーリスト
            $entries = null;

            // 対象イベント
            $target_event = new Event;
        } else {
            $conditions['search_event'] = $request->event_id;

            // エントリー一覧
            $query = Entry::where('is_deleted', false);
            $query->where('event_id', $request->event_id);
            $query->orderBy('kname', 'asc');
            $entries = $query->paginate(config('const.limit.ENTRY_LIST'));
            $entries->params = array('event_id' => $request->event_id);  // クエリストリングのパラメータ

            // 対象イベント
            $target_event = Event::where('is_deleted', false)->where('id', $request->event_id)->first();
        }

        return view('reception.index', compact('entries', 'target_event', 'findItems', 'conditions'));
    }

    /**
     * 入退場情報一覧画面 検索処理
     *
     * @param Request $request
     * @return void
     */
    public function find(Request $request) {

        Log::info('['.Auth::user()->id.'] start ReceptionController find.');

        //　抽出条件
        $conditions = array();
        if(!empty($request->search_event)) {
            $conditions['search_event'] = $request->search_event;
        } elseif(!empty($request->event_id)) {
            $conditions['search_event'] = $request->event_id;
            $request->search_event = $request->event_id;
        }

        // 検索条件のアイテム
        $findItems = new stdClass;
        $findItems->event = Event::whereNull('is_view')->where('is_deleted', false)->get();

        // エントリー一覧
        if(!empty($request->search_event)) {
            $query = Entry::where('is_deleted', false);
            $query->where('event_id', $request->search_event);
            if(!empty($request->find_name)) {
                $query->where('name', 'like', '%'.$request->find_name.'%');
                $conditions['find_name'] = $request->find_name;
            }
            if(!empty($request->find_kname)) {
                $query->where('kname', 'like', '%'.$request->find_kname.'%');
                $conditions['find_kname'] = $request->find_kname;
            }
            if(!empty($request->find_school)) {
                $query->where('school', 'like', '%'.$request->find_school.'%');
                $conditions['find_school'] = $request->find_school;
            }
            $query->orderBy('kname', 'asc');
            $entries = $query->paginate(config('const.limit.ENTRY_LIST'));
            $entries->params = array('event_id' => $request->search_event);  // クエリストリングのパラメータ
        } else {
            $entries = null;
        }

        // 対象イベント
        if(!empty($request->search_event)) {
            $target_event = Event::where('is_deleted', false)->where('id', $request->search_event)->first();
        } else {
            $target_event = new Event;
        }

        return view('reception.index', compact('entries', 'target_event', 'findItems', 'conditions'));
    }

    /**
     * 入退場受付
     *
     * @param Request $request
     * @return void
     */
    public function regist(Request $request) {

        Log::info('['.Auth::user()->id.'] start ReceptionController regist.');

        $data = $request;
        return view('reception.regist', compact('data'));

    }

    /**
     * 入退場登録
     *
     * @param Request $request
     * @return void
     */
    public function complete(Request $request) {

        Log::info('['.Auth::user()->id.'] start ReceptionController regist.');

        try {
            // バリデーション 実行
            $this->validate($request, $this->validation_rule(), $this->validation_message());

            // エントリー情報
            $entry = Entry::where('code', $request->code)->where('event_id', $request->event_id)->where('is_deleted', false)->first();

            if(empty($entry)) {
                return redirect()->back()->withInput()->with('error', '予約者コードが予約されていません。');
            }

            // イベント受付情報
            $reception = Reception::where('entry_id', $entry->id)->where('event_id', $request->event_id)->first();

            if($request->type == config('const.reception_type.ENTER')) {
                // 入場
                if(!empty($reception->enter_at)) {
                    return redirect()->back()->withInput()->with('error', '既に入場受付が済んでいます。');
                }

                // 入場日時
                $reception->enter_at = date('Y-m-d H:i:s');

            } elseif($request->type == config('const.reception_type.EXIT')) {
                // 退場

                if(empty($reception->enter_at)) {
                    return redirect()->back()->withInput()->with('error', '入場受付が済んでいません。');
                }

                if(!empty($reception->exit_at)) {
                    return redirect()->back()->withInput()->with('error', '既に退場受付が済んでいます。');
                }

                // 退場日時
                $reception->exit_at = date('Y-m-d H:i:s');

                // 訪問数
                $vivist_count = Visit::where('entry_id', $entry->id)->count();
                $questionnaire = Questionnaire::where('entry_id', $entry->id)->get();
                if(count($questionnaire) > 0) {
                    $is_questionnaire = '回答済';
                } else {
                    $is_questionnaire = '未回答';
                }
            }

            // 登録処理
            $reception->save();

            Log::info('['.Auth::user()->id.'] save : [code:'.$request->code.'][enter_at:'.$reception->enter_at.'][exit_at:'.$reception->exit_at.']');

            if($request->type == config('const.reception_type.ENTER')) {
                return redirect()->back()->with(array('complete'=>'入場受付が完了しました。', 'type'=>'ENTER'));
            } elseif($request->type == config('const.reception_type.EXIT')) {
                return redirect()->back()->with(array('complete'=>'退場受付が完了しました。', 'type'=>'EXIT', 'vivist_count'=>$vivist_count, 'is_questionnaire'=>$is_questionnaire));
            }

        } catch (Exception $e) {

        }

    }

    /**
     * QRコードでの入退場受付
     *
     * @param Request $request
     * @return void
     */
    public function qr_regist(Request $request) {

        Log::info('['.Auth::user()->id.'] start ReceptionController qr_regist.');

        $data = $request;
        return view('reception.qr_regist', compact('data'));

    }

    /**
     * 入退場登録
     *
     * @param Request $request
     * @return void
     */
    public function qr_complete(Request $request) {

        Log::info('['.Auth::user()->id.'] start ReceptionController qr_complete.');

        try {
            // ユーザー情報の取得
            $user = User::where('uuid', $request->uuid)->where('is_deleted', false)->first();

            if(empty($user)) {
                return redirect()->back()->withInput()->with('error', 'WEB登録されていません。');
            }

            // エントリー情報
            $entry = Entry::where('code', $user->code)->where('event_id', $request->event_id)->where('is_deleted', false)->first();

            if(empty($entry)) {
                return redirect()->back()->withInput()->with('error', '予約者コードが予約されていません。');
            }

            // イベント受付情報
            $reception = Reception::where('entry_id', $entry->id)->where('event_id', $request->event_id)->first();

            if($request->type == config('const.reception_type.ENTER')) {
                // 入場
                if(!empty($reception->enter_at)) {
                    return redirect()->back()->withInput()->with('error', '既に入場受付が済んでいます。');
                }

                // 入場日時
                $reception->enter_at = date('Y-m-d H:i:s');

            } elseif($request->type == config('const.reception_type.EXIT')) {
                // 退場

                if(empty($reception->enter_at)) {
                    return redirect()->back()->withInput()->with('error', '入場受付が済んでいません。');
                }

                if(!empty($reception->exit_at)) {
                    return redirect()->back()->withInput()->with('error', '既に退場受付が済んでいます。');
                }

                // 退場日時
                $reception->exit_at = date('Y-m-d H:i:s');

                // 訪問数
                $vivist_count = Visit::where('entry_id', $entry->id)->count();
                $questionnaire = Questionnaire::where('entry_id', $entry->id)->get();
                if(count($questionnaire) > 0) {
                    $is_questionnaire = '回答済';
                } else {
                    $is_questionnaire = '未回答';
                }
                $created_dt = $entry->created_at->format('Y年m月d日 H:i:s');
            }

            // 登録処理
            $reception->save();

            Log::info('['.Auth::user()->id.'] save : [code:'.$request->code.'][enter_at:'.$reception->enter_at.'][exit_at:'.$reception->exit_at.']');

            if($request->type == config('const.reception_type.ENTER')) {
                return redirect()->back()->with('complete', '入場受付が完了しました。');
            } elseif($request->type == config('const.reception_type.EXIT')) {
                return redirect()->back()->with(array('complete'=>'退場受付が完了しました。', 'vivist_count'=>$vivist_count, 'is_questionnaire'=>$is_questionnaire, 'created_dt' => $created_dt));
            }

        } catch (Exception $e) {

        }

    }

    /**
     * バリデーション ルール
     * @return string[]
     */
    private function validation_rule() {
        $rule = [
            'type'  => 'required',
            'code'  => 'required|regex:/^[a-zA-Z0-9]+$/|max:7',
        ];

        return $rule;
    }

    /**
     * バリデーションメッセージ
     * @return string[]
     */
    private function validation_message() {
        return [
            'type.required' => '入退場を選択してください',
            'code.required' => '予約者コードを入力してください',
            'code.regex'    => '予約者コードは英数字のみ入力可能です',
            'code.max'      => '予約者コードは最大７文字です',
        ];
    }
}

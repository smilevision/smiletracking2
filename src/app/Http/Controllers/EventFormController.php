<?php

namespace App\Http\Controllers;

use stdClass;
use Exception;
use App\Model\User;
use App\Model\Entry;
use App\Model\Event;
use App\Model\Reception;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Mail\EntryThanksMail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

/**
 * イベント応募フォーム Controller
 */
class EventFormController extends Controller
{
    /**
     * 応募フォーム
     *
     * @param Request $request
     * @return void
     */
    public function index(Request $request) {

        Log::info("EventFormController@index start");

        $data = $this->getEventInfo($request->code);

        if(empty($data)) return redirect('/');
        return view('event_form.index', compact('data'));
    }

    /**
     * 応募内容確認
     *
     * @param Request $request
     * @return void
     */
    public function confirm(Request $request) {

        Log::info("EventFormController@confirm start");

        // // 生年月日
        // $request->merge(['birth'=>$request->birth_y.'-'.$request->birth_m.'-'.$request->birth_d]);

        // バリデーション 実行
        $this->validate($request, $this->validation_rule(), $this->validation_message());

        $data = new stdClass;

        $data = $this->getEventInfo($request->code);

        // 入力値の引き渡し
        $data->request = $request;

        return view('event_form.confirm', compact('data'));
    }

    /**
     * 応募完了
     *
     * @param Request $request
     * @return void
     */
    public function finish(Request $request) {

        Log::info("EventFormController@finish start");

        $action = $request->get('back');
        $input = $request->except('action');
        if($action=='back') {
            return redirect()->route('event_form', array('code'=>$request->code))->withInput($input);
        }

        // バリデーション 実行
        $this->validate($request, $this->validation_rule(), $this->validation_message());

        DB::beginTransaction();

        try {
            // 応募イベント情報
            $event = $this->getEventInfo($request->code);

            // ユーザー登録
            $user = new User;
            $user->is_authority = config('const.authority.VISITOR');
            $user->code = $this->createCode();
            $user->uuid = (string) Str::uuid();
            $user->name = trim($request->name);
            $user->email = trim($request->email);
            $user->password = Hash::make(trim($request->email));
            $user->save();

            // エントリー情報登録
            $entry = new Entry;
            $entry->event_id = $event->id;
            $entry->users_id = $user->id;
            $entry->code = $user->code;
            $entry->name = trim($request->name);
            $entry->kname = trim($request->kname);
            $entry->gender = trim($request->gender);
            // $entry->birth = trim($request->birth);
            // $entry->zip = trim($request->zip);
            // $entry->address1 = trim($request->address1);
            // $entry->address2 = trim($request->address2);
            // $entry->address3 = trim($request->address3);
            $entry->email = trim($request->email);
            $entry->phone = trim($request->phone);
            $entry->division = trim($request->division);
            $entry->school = trim($request->school);
            $entry->gakubu = trim($request->gakubu);
            // $entry->school_year = trim($request->school_year);
            $entry->graduate_year = trim($request->graduate_year);
            $entry->introduction = trim($request->introduction);
            $entry->introduction_name = trim($request->introduction_name);
            $entry->introduction_school_name = trim($request->introduction_school_name);
            $entry->note = trim($request->note);
            $entry->save();

            // イベント受付情報の初期登録
            $reception = new Reception;
            $reception->event_id = $event->id;
            $reception->entry_id = $entry->id;
            $reception->save();

            DB::commit();

            // QRコード画像の作成
            QrCode::format('png')->size(256)->generate('id='.$entry->user->uuid, public_path('images/qr/entry/'.$entry->user->uuid.'.png'));

            // 成功メール送信
            $res = $this->send_thanks_mail($entry);
            if(!$res) {
                Log::error("message");
            }

            // 完了ページへ遷移
            $data = $event;
            return view('event_form.thanks', compact('data'));

        } catch (Exception $e) {
            Log::error($e->getMessage());
            DB::rollback();
        }
    }

    /**
     * 応募フォーム
     *
     * @param Request $request
     * @return void
     */
    public function thanks(Request $request) {

        Log::info("EventFormController@thanks start");

        $previous = url()->previous();

        if($previous <> route('event_form.confirm')) {
            Log::warning("別画面からの遷移。");
            abort(404);
        }

        $data = $this->getEventInfo($request->code);

        if(empty($data)) return redirect('/');
        return view('event_form.thanks', compact('data'));
    }

    public function redirect(Request $request)
    {
        return redirect()->route('event_form', array('code'=>$request->code));
    }

    /**
     * 完了メール送信
     *
     * @param Entry $entry エントリー情報
     * @return bool 成功(true)・失敗(false)
     */
    private function send_thanks_mail(Entry $entry)
    {
        // パスワード再設定完了 メール送信（ユーザー宛）
        try {
            Mail::to($entry->email)->send(new EntryThanksMail($entry));
            if(count(Mail::failures()) > 0) {
                Log::error(Mail::failures());
                return false;
            }
            return true;
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return false;
        }

    }


    /**
     * イベント情報の取得
     *
     * @param string $code
     * @return mixed 成功：イベント情報、エラー：false
     */
    private function getEventInfo(string $code)
    {
        // コードの復元
        Log::info("code:".$code);
        try {
            $code = Crypt::decrypt($code);
        } catch (\DecryptException $th) {
            Log::error($th);
            return null;
        }
        // 対象イベント情報の取得
        $query = Event::whereNull('deleted_at')->where('code', $code);
        /*
        $today = date('Y-m-d H:i:00');
        $query->where(function($query) use ($today){
            $query->where('reserv_started_at', '<=', $today);
            $query->orWhere('reserv_ended_at', '>', $today);
        });
        */
        $event = $query->first();
        Log::debug("SQL:".$query->toSql());

        return $event;
    }

    /**
     * 予約コード発行
     *
     * @return string 予約コード
     */
    private function createCode() {
        $head_length = 3;
        $head = substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, $head_length);
        $code_length = 4;
        $code = substr(str_shuffle('1234567890'), 0, $code_length);
        return $head . $code;
    }

    /**
     * バリデーション ルール
     *
     * @return array
     */
    private function validation_rule() {
        $rule = [
            'name'          => 'required|max:255',
            'kname'         => 'required|max:255|regex:/^[ア-ン゛゜ァ-ォャ-ョー]+$/u',
            'gender'        => 'required',
            // 'birth'         => 'required|date',
            // 'zip'           => 'required|max:8',
            // 'address1'      => 'required',
            // 'address2'      => 'required|max:1024',
            // 'address3'      => 'nullable|max:1024',
            'email'         => 'required|max:255|email',
            'phone'         => 'required|max:16',
            'division'      => 'required',
            'school'        => 'required|max:128',
            'gakubu'        => 'nullable|max:128',
            // 'school_year'   => 'required',
            'graduate_year' => 'required',
            'introduction'  => 'required',
            'introduction_name'  => 'nullable|max:255',
            'introduction_school_name'  => 'nullable|max:255',
            'note'          => 'nullable|max:2048',
        ];

        return $rule;
    }

    /**
     * バリデーション エラーメッセージ
     *
     * @return array
     */
    private function validation_message() {
        return [
            'name.required'          => 'お名前が入力されていません。',
            'name.max'               => 'お名前の最大文字列数を超えています。',
            'kname.required'         => 'フリガナが入力されていません。',
            'kname.max'              => 'フリガナの最大文字列数を超えています。',
            'kname.regex'            => 'フリガナはカタカナのみ入力可能です。',
            'gender.required'        => '性別を選択してください。',
            // 'birth.required'         => '生年月日が入力されていません。',
            // 'birth.date'             => '生年月日は年月日で入力してください。',
            // 'zip.required'           => '郵便番号が入力されていません。',
            // 'zip.max'                => '郵便番号の最大文字列数を超えています。',
            // 'address1.required'      => '都道府県が選択されていません。',
            // 'address2.required'      => 'ご住所が入力されていません。',
            // 'address2.max'           => 'ご住所の最大文字列数を超えています。',
            // 'address3.max'           => 'マンション名などの最大文字列数を超えています。',
            'email.required'         => 'メールアドレスが入力されていません。',
            'email.max'              => 'メールアドレスの最大文字列数を超えています。',
            'email.email'            => 'メールアドレスが無効の形式です。',
            'phone.required'         => '電話番号が入力されていません。',
            'phone.max'              => '電話番号の最大文字列数を超えています。',
            'division.required'      => '在籍区分が選択されていません。',
            'school.required'        => '学校名が入力されていません。',
            'school.max'             => '学校名の最大文字列数を超えています。',
            'gakubu.max'             => '学部・コースなどの最大文字列数を超えています。',
            // 'school_year.required'   => '学年が選択されていません。',
            'graduate_year.required' => '卒業見込年度が選択されていません。',
            'introduction.required'  => 'このイベントを何で知りましたかが選択されていません。',
            'introduction_name.max'  => '紹介者名の最大文字列数を超えています。',
            'introduction_school_name.max' => '紹介者学校名の最大文字列数を超えています。',
            'note.max'               => '備考欄の最大文字列数を超えています。',
        ];
    }
}

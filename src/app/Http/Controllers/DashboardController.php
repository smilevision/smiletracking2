<?php

namespace App\Http\Controllers;

use stdClass;
use App\Model\Entry;
use App\Model\Event;
use App\Model\Visit;
use App\Model\Reception;
use App\Model\Exhibitors;
use App\Model\Questionnaire;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Model\Exhibitors_User_Relation;

/**
 * ダッシュボード Controller
 */
class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * ダッシュボード
     *
     * @return void
     */
    public function index(Request $request) {

        Log::info('['.Auth::user()->id.'] start DashboardController index.');

        //　抽出条件
        $conditions = array();

        // 検索条件のアイテム
        $findItems = new stdClass;
        switch (Auth::user()->is_authority) {
            case config('const.authority.KANRISHA'):
            case config('const.authority.EVENTOR'):
                $findItems->event = Event::whereNull('is_view')->where('is_deleted', false)->orderby('started_at')->get();
                break;
            case config('const.authority.EXHIBITORS'):

                $query = Event::select('events.id', 'events.code', 'events.name')->whereNull('events.is_view')->where('events.is_deleted', false)->orderby('events.started_at')
                ->join('exhibitors', function ($join) {
                    $join->on('exhibitors.event_id', '=', 'events.id')
                         ->where('exhibitors.code', '=', Auth::user()->code);
                });
                $findItems->event = $query->get();
                Log::debug("SQL:".$query->toSql());
                Log::debug("code:".Auth::user()->code);
                break;

            case config('const.authority.VISITOR'):
                $query = Event::select('events.id', 'events.code', 'events.name')->whereNull('events.is_view')->where('events.is_deleted', false)->orderby('events.started_at')
                ->join('entry', function ($join) {
                    $join->on('entry.event_id', '=', 'events.id')
                         ->where('entry.code', '=', Auth::user()->code);
                });
                $findItems->event = $query->get();
                Log::debug("SQL:".$query->toSql());
                Log::debug("code:".Auth::user()->code);
                break;
            default:
                # code...
                break;
        }

        if(empty($findItems->event[0])) {
            $target_event = new Event;
            $event_id = null;
            $findItems->event = array();
        } else {

            if($request->session()->has('event_id')) {
                $event_id = $request->session()->get('event_id');
            } else {
                if(!empty($findItems->event[0])) {
                    $event_id = $findItems->event[0]->id;
                }
            }

            $target_event = Event::where('id', $event_id)->first();
            $conditions['search_event'] = $event_id;
            $request->session()->put('event_id', $event_id);
        }

        // 権限毎の情報取得
        switch (Auth::user()->is_authority) {
            case config('const.authority.KANRISHA'):
                $page = 'dashboard.kanrisha';
                $data = $this->kanrisha($event_id);
                break;

            case config('const.authority.EVENTOR'):
                $page = 'dashboard.eventor';
                $data = $this->eventor($event_id);
                break;

            case config('const.authority.EXHIBITORS'):
                $page = 'dashboard.exhibitors';
                $data = $this->exhibitors($event_id);
                break;

            case config('const.authority.VISITOR'):
                $page = 'dashboard.visitor';
                $data = $this->visitor($event_id);
                break;

            default:
                $page = 'dashboard.visitor';
                $data = $this->visitor($event_id);
                break;
        }

        return view($page, compact('findItems', 'conditions', 'target_event', 'data'));
    }

    /**
     * ダッシュボード
     *
     * @return void
     */
    public function find(Request $request) {

        Log::info('['.Auth::user()->id.'] start DashboardController index.');

        //　抽出条件
        $conditions = array();

        // 検索条件のアイテム
        $findItems = new stdClass;
        switch (Auth::user()->is_authority) {
            case config('const.authority.KANRISHA'):
            case config('const.authority.EVENTOR'):
                $findItems->event = Event::where('is_deleted', false)->orderby('started_at')->get();
                break;
            case config('const.authority.EXHIBITORS'):

                $query = Event::select('events.id', 'events.code', 'events.name')->where('events.is_deleted', false)->orderby('events.started_at')
                ->join('exhibitors', function ($join) {
                    $join->on('exhibitors.event_id', '=', 'events.id')
                         ->where('exhibitors.code', '=', Auth::user()->code);
                });
                $findItems->event = $query->get();
                Log::debug("SQL:".$query->toSql());
                Log::debug("code:".Auth::user()->code);
                break;

            case config('const.authority.VISITOR'):
                $query = Event::select('events.id', 'events.code', 'events.name')->where('events.is_deleted', false)->orderby('events.started_at')
                ->join('entry', function ($join) {
                    $join->on('entry.event_id', '=', 'events.id')
                         ->where('entry.code', '=', Auth::user()->code);
                });
                $findItems->event = $query->get();
                Log::debug("SQL:".$query->toSql());
                Log::debug("code:".Auth::user()->code);
                break;
            default:
                # code...
                break;
        }

        if(empty($request->search_event)) {
            $event_id = $request->session()->get('event_id');
        } else {
            $event_id = $request->search_event;
        }

        $target_event = Event::where('id', $event_id)->first();
        $conditions['search_event'] = $request->search_event;

        switch (Auth::user()->is_authority) {
            case config('const.authority.KANRISHA'):
                $page = 'dashboard.kanrisha';
                $data = $this->kanrisha($event_id);
                break;

            case config('const.authority.EVENTOR'):
                $page = 'dashboard.eventor';
                $data = $this->eventor($event_id);
                break;

            case config('const.authority.EXHIBITORS'):
                $page = 'dashboard.exhibitors';
                $data = $this->exhibitors($event_id);
                break;

            case config('const.authority.VISITOR'):
                $page = 'dashboard.visitor';
                $data = $this->visitor($event_id);
                break;

            default:
                $page = 'dashboard.visitor';
                $data = $this->visitor($event_id);
                break;
        }

        return view($page, compact('findItems', 'conditions', 'target_event', 'data'));
    }

    /**
     * 管理者用ダッシュボード
     *
     * @param int $event_id 対象イベントID
     * @return object $data ダッシュボード表示用データ
     */
    public function kanrisha($event_id) {

        $data = new stdClass;

        $target_event = Event::where('id', $event_id)->first();

        $entry_count = Entry::select(DB::raw('date_format(created_at, "%Y-%m-%d")'), DB::raw('count("id")'))->groupBy(DB::raw('date_format(created_at, "%Y-%m-%d")'))->get();

        // 予約情報
        $data->entry = Entry::where('event_id', $event_id)->where('is_deleted', false)->limit(config('const.limit.DASHBOARD_EVENT_LIST'))->orderby('created_at', 'desc')->get();
        $data->entry_count = Entry::where('event_id', $event_id)->where('is_deleted', false)->count();

        // 入退場一覧
        $data->reception = Reception::where('event_id', $event_id)->where('is_deleted', false)->whereNotNull('enter_at')->limit(config('const.limit.DASHBOARD_RECEPTION_LIST'))->orderby('enter_at', 'desc')->get();
        $data->reception_count = Reception::where('event_id', $event_id)->where('is_deleted', false)->whereNotNull('enter_at')->count();

        // アンケート一覧
        $data->questionnaire = Questionnaire::where('is_deleted', false)->limit(config('const.limit.DASHBOARD_QUESTIONNAIRE_LIST'))->orderby('created_at', 'desc')->get();

        // 平均滞在時間（時間）
        $reception_stay_time = Reception::where('event_id', $event_id)->where('is_deleted', false)->whereNotNull('exit_at')->get();
        if(count($reception_stay_time) > 0) {

            $average_stay_time = 0;                         // 平均滞在時間
            $all_stay_time = 0;                             // 合計滞在時間
            $reception_count = count($reception_stay_time); // 退場済み人数

            foreach ($reception_stay_time as $key => $reception) {
                $diff_time = strtotime($reception->exit_at) - strtotime($reception->enter_at);
                $all_stay_time += $diff_time / 60;  // 分で加算
            }
            $average_stay_time = $all_stay_time / $reception_count;

            $data->average_stay_time = $average_stay_time / 60; // 時間に変換
        } else {
            $data->average_stay_time = null;
        }

        // 平均ブース来訪者
        $query = Visit::where('is_deleted', false);
        $visit = $query->get();
        $data->average_visitor_count = null;

        // 出展企業一覧
        $data->exhibitors = Exhibitors::where('event_id', $event_id)->whereNull('deleted_at')->limit(config('const.limit.DASHBOARD_EXHIBITORS_LIST'))->orderby('created_at', 'desc')->get();

        return $data;
    }

    /**
     * 開催者用ダッシュボード
     *
     * @param int $event_id 対象イベントID
     * @return object $data ダッシュボード表示用データ
     */
    private function eventor($event_id) {

        $data = new stdClass;

        $target_event = Event::where('id', $event_id)->first();

        $entry_count = Entry::select(DB::raw('date_format(created_at, "%Y-%m-%d")'), DB::raw('count("id")'))->groupBy(DB::raw('date_format(created_at, "%Y-%m-%d")'))->get();

        // 予約情報
        $data->entry = Entry::where('event_id', $event_id)->where('is_deleted', false)->limit(config('const.limit.DASHBOARD_EVENT_LIST'))->orderby('kname', 'asc')->get();
        $data->entry_count = Entry::where('event_id', $event_id)->where('is_deleted', false)->count();

        // 入退場一覧
        $data->reception = Reception::where('event_id', $event_id)->where('is_deleted', false)->whereNotNull('enter_at')->limit(config('const.limit.DASHBOARD_RECEPTION_LIST'))->orderby('updated_at', 'desc')->get();
        $data->reception_count = Reception::where('event_id', $event_id)->where('is_deleted', false)->whereNotNull('enter_at')->count();

        $data->visit = Visit::select(DB::raw(' count(exhibitors_id) as visit_count'), 'exhibitors.name')
                            ->leftJoin('exhibitors', 'visit.exhibitors_id', '=', 'exhibitors.id')
                            ->where('visit.event_id', $event_id)
                            ->groupBy('visit.exhibitors_id')
                            ->orderBy('visit_count', 'desc')
                            ->get();


        // アンケート一覧
        $data->questionnaire = Questionnaire::where('is_deleted', false)->limit(config('const.limit.DASHBOARD_QUESTIONNAIRE_LIST'))->orderby('created_at', 'desc')->get();

        // 平均滞在時間（時間）
        $reception_stay_time = Reception::where('event_id', $event_id)->where('is_deleted', false)->whereNotNull('exit_at')->get();
        if(count($reception_stay_time) > 0) {

            $average_stay_time = 0;                         // 平均滞在時間
            $all_stay_time = 0;                             // 合計滞在時間
            $reception_count = count($reception_stay_time); // 退場済み人数

            foreach ($reception_stay_time as $key => $reception) {
                $diff_time = strtotime($reception->exit_at) - strtotime($reception->enter_at);
                $all_stay_time += $diff_time / 60;  // 分で加算
            }
            $average_stay_time = $all_stay_time / $reception_count;

            $data->average_stay_time = $average_stay_time / 60; // 時間に変換
        } else {
            $data->average_stay_time = null;
        }

        // 平均ブース来訪者
        $total_visit_count = Visit::whereNull('deleted_at')->where('event_id', $event_id)->count();
        $exhibitors_count = Exhibitors::whereNull('deleted_at')->where('event_id', $event_id)->count();
        if(empty($exhibitors_count) || empty($total_visit_count)) {
            $data->average_visitor_count = 0;
        } else {
            $data->average_visitor_count = round($total_visit_count / $exhibitors_count, 2);
        }

        // 出展企業一覧
        $data->exhibitors = Exhibitors::where('event_id', $event_id)->whereNull('deleted_at')->limit(config('const.limit.DASHBOARD_EXHIBITORS_LIST'))->orderby('created_at', 'desc')->get();

        return $data;
    }

    /**
     * 出展者用ダッシュボード
     *
     * @param int $event_id 対象イベントID
     * @return object $data ダッシュボード表示用データ
     */
    private function exhibitors($event_id) {

        $data = new stdClass;
        $user = Auth::user();

        // 訪問者履歴
        $query = Visit::where('is_deleted', false);
        $query->where('event_id', $event_id);
        $query->where('exhibitors_id', $user->relation->exhibitors->id);
        $data->visitors = $query->get();

        return $data;
    }

    /*
    /**
     * 訪問者用ダッシュボード
     *
     * @param int $event_id 対象イベントID
     * @return object $data ダッシュボード表示用データ
     */
    private function visitor($event_id) {

        $data = new stdClass;

        // 出展企業一覧
        $data->exhibitors = Exhibitors::where('event_id', $event_id)->whereNull('deleted_at')->orderby('created_at', 'desc')->get();

        return $data;
    }
}

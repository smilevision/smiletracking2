<?php

namespace App\Http\Controllers;

use App\Model\User;
use App\Model\Entry;
use App\Model\Reception;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;

class CsvImportController extends Controller
{
    public function entry() {

        // 開始ログ出力
        Log::info('started CsvImportController.');

        // マスター情報CSVファイルパスの取得
        $path = storage_path('csv/entry.csv');
        $lines = file($path);

        // マスター情報をDB登録用の配列に変換（エンコードも変換）
        $entries = array();
        $cnt = 0;
        foreach($lines as $line){

            $line = str_replace("\"", "", $line);   // ダブルクォーテーションの削除
            $line = str_replace(array("\r\n", "\r", "\n"), '', $line);  // 改行コードの削除
            $data = explode(',', $line);    // カンマで分解
            //mb_convert_variables('UTF-8', 'sjis-win', $data);   // エンコードを、UTF-8に変更

            DB::beginTransaction();

            try {

                // ユーザー登録
                $user = new User;
                $user->is_authority = config('const.authority.VISITOR');
                $user->code = $this->createCode();
                $user->uuid = (string) Str::uuid();
                $user->name = trim($data[4]);
                $user->email = trim($data[12]);
                $user->password = Hash::make(trim($data[12]));
                $user->save();

                // エントリー情報登録
                $entry = new Entry;
                $entry->event_id = 1;
                $entry->users_id = $user->id;
                $entry->code = $user->code;
                $entry->name = trim($data[4]);
                $entry->kname = trim($data[5]);
                $entry->gender = trim($data[6]);
                $entry->birth = trim($data[7]);
                $entry->zip = trim($data[8]);
                $entry->address1 = trim($data[9]);
                $entry->address2 = trim($data[10]);
                $entry->address3 = trim($data[11]);
                $entry->email = trim($data[12]);
                $entry->phone = trim($data[13]);
                $entry->division = trim($data[14]);
                $entry->school = trim($data[15]);
                $entry->gakubu = trim($data[16]);
                $entry->school_year = trim($data[17]);
                $entry->graduate_year = trim($data[18]);
                $entry->note = trim($data[19]);
                $entry->save();

                // イベント受付情報の初期登録
                $reception = new Reception;
                $reception->event_id = 1;
                $reception->entry_id = $entry->id;
                $reception->save();

                DB::commit();

            } catch (Exception $e) {
                Log::error($ex->getMessage());
                DB::rollback();
            }
        }
            // ファイル削除
            unlink($path);

            echo('完了');
    }

    /**
     * 予約コード発行
     *
     * @return string 予約コード
     */
    private function createCode() {
        $head_length = 3;
        $head = substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, $head_length);
        $code_length = 4;
        $code = substr(str_shuffle('1234567890'), 0, $code_length);
        return $head . $code;
    }
}

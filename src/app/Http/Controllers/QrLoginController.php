<?php

namespace App\Http\Controllers;

use App\Model\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * QR自動ログイン Controller
 */
class QrLoginController extends Controller
{
    /**
     * QRコードでのログイン
     *
     * @param Request $request
     * @return void
     */
    public function login(Request $request) {

        $user = User::where('uuid', $request->uuid)->where('is_deleted', false)->first();

        if(!is_null($user)) {
            Auth::login($user);
            return redirect()->route('dashboard');
        } else {
            return view('error.login');
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Model\User;
use App\Model\Entry;
use App\Model\Reception;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;

/**
 * ログイン認証を介さないエントリー
 */
class NewEntryController extends Controller
{
    /**
     * WEB予約者情報  新規登録画面
     *
     * @param Request $request
     * @return void
     */
    public function regist(Request $request) {

        $data = $request;
        return view('new_entry.regist', compact('data'));
    }

    /**
     * WEB予約者情報  新規登録確認画面
     *
     * @param Request $request
     * @return void
     */
    public function check(Request $request) {

        // バリデーション 実行
        $this->validate($request, $this->validation_rule(), $this->validation_message());

        $data = $request;
        $data->event_id = 1;
        $data->code = $this->createCode();
        return view('new_entry.regist_check', compact('data'));
    }

    /**
     * WEB予約者情報  登録処理
     *
     * @param Request $request
     * @return void
     */
    public function complete(Request $request) {

        // トランザクション開始
        DB::beginTransaction();

        try {

            Log::info('event id : '.$request->event_id);

            // コード作成
            $code = $this->createCode();

            // ユーザー登録
            $user = new User;
            $user->is_authority = config('const.authority.VISITOR');
            $user->code = $code;
            $user->uuid = (string) Str::uuid();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->email);
            $user->save();

            // エントリー情報登録
            $entry = new Entry;
            $request->merge(array('code' => $code, 'users_id' => $user->id));
            $entry->fill($request->all())->save();

            // イベント受付情報の初期登録
            $reception = new Reception;
            $reception->event_id = $request->event_id;
            $reception->entry_id = $entry->id;
            $reception->save();

            // commit
            DB::commit();

            return redirect()->route('new_entry.regist.thunks', array('event_id'=>$request->event_id));
        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            // ロールバック
            DB::rollback();
        }
    }

    /**
     * WEB予約者情報  新規登録完了
     *
     * @param Request $request
     * @return void
     */
    public function thunks(Request $request) {
        $event_id = $request->event_id;
        return view('new_entry.regist_thunks', compact('event_id'));
    }

    /**
     * 予約コード発行
     *
     * @return string 予約コード
     */
    private function createCode() {
        $head_length = 3;
        $head = substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, $head_length);
        $code_length = 4;
        $code = substr(str_shuffle('1234567890'), 0, $code_length);
        return $head . $code;
    }

    /**
     * バリデーション ルール
     * @return string[]
     */
    private function validation_rule() {
        $rule = [
            'name'          => 'required|max:255',
            'kname'         => 'required|regex:/^[ア-ン゛゜ァ-ォャ-ョー]+$/u|max:255',
            'gender'        => 'nullable|numeric',
            'birth'         => 'required|max:255',
            'zip'           => 'required|max:8',
            'address1'      => 'required|max:128',
            'address2'      => 'required|max:1024',
            'address3'      => 'nullable|max:1024',
            'email'         => 'required|max:255',
            'phone'         => 'nullable|max:16',
            'division'      => 'required|max:64',
            'school'        => 'required|max:128',
            'gakubu'        => 'nullable|max:128',
            'school_year'   => 'required|max:64',
            'graduate_year' => 'required|max:64',
        ];

        return $rule;
    }

    /**
     * バリデーションメッセージ
     * @return string[]
     */
    private function validation_message() {
        return [
            'name.required'           => 'お名前が入力されていません。',
            'name.max'                => 'お名前の最大文字列数を超えています。',
            'kname.required'          => 'フリガナが入力されていません。',
            'kname.max'               => 'フリガナの最大文字列数を超えています。',
            'kname.regex'             => 'フリガナはカタカナ入力です。',
            'gender.required'         => '性別が選択されていません。',
            'birth.required'          => '生年月日が入力されていません。',
            'birth.max'               => '生年月日の最大文字列数を超えています。',
            'zip.required'            => '郵便番号が入力されていません。',
            'zip.max'                 => '郵便番号の最大文字列数を超えています。',
            'address1.required'       => '都道府県が入力されていません。',
            'address1.max'            => '都道府県の最大文字列数を超えています。',
            'address2.required'       => 'ご住所が入力されていません。',
            'address2.max'            => 'ご住所の最大文字列数を超えています。',
            'address3.max'            => 'マンション名などの最大文字列数を超えています。',
            'email.required'          => 'メールアドレスが入力されていません。',
            'email.max'               => 'メールアドレスの最大文字列数を超えています。',
            'phone.max'               => '電話番号の最大文字列数を超えています。',
            'division.required'       => '在籍区分が入力されていません。',
            'division.max'            => '在籍区分の最大文字列数を超えています。',
            'school.required'         => '学校名が入力されていません。',
            'school.max'              => '学校名の最大文字列数を超えています。',
            'gakubu.max'              => '学部・コースなどの最大文字列数を超えています。',
            'school_year.required'    => '学年が入力されていません。',
            'school_year.max'         => '学年の最大文字列数を超えています。',
            'graduate_year.required'  => '卒業見込み年度が入力されていません。',
            'graduate_year.max'       => '卒業見込み年度の最大文字列数を超えています。',
        ];
    }
}

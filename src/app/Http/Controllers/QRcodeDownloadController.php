<?php

namespace App\Http\Controllers;

use App\Model\User;
use App\Model\Entry;
use Illuminate\Http\Request;

class QRcodeDownloadController extends Controller
{
    public function download(Request $request) {

        $user = User::where('uuid', $request->uuid)->first();
        if($user->is_authority == config('const.authority.VISITOR')) {
            $user->entry = Entry::where('code', $user->code)->where('is_deleted', false)->first();
        }
        $pdf = \PDF::loadView('qrcode.download', compact('user'));
        return $pdf->download($user->uuid.'.pdf');
    }
}

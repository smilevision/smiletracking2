<?php

namespace App\Http\Controllers\Api;

use App\Model\User;
use App\Model\Entry;
use App\Model\Reception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;

class EntryController extends Controller
{
    /**
     * エントリー削除
     *
     * @param Request $request
     * @return void
     */
    public function delete(Request $request) {

        Log::info('id : '.$request->id);

        DB::beginTransaction();

        try {

            $delete_datetime = date('Y-m-d H:i:s');

            $entry = Entry::where('id', $request->id)->first();
            $entry->is_deleted = true;
            $entry->deleted_at = $delete_datetime;
            $entry->save();

            $reception = Reception::where('entry_id', $request->id)->where('event_id', $entry->event_id)->first();
            $reception->is_deleted = true;
            $reception->deleted_at = $delete_datetime;
            $reception->save();

            $user = User::where('id', $entry->users_id)->first();
            $user->is_deleted = true;
            $user->deleted_at = $delete_datetime;
            $user->save();

            DB::commit();

            $response = array(
                'status' => config('const.status.code.OK'),
                'message' => config('const.status.message.OK'),
                'result' => $entry
            );

        } catch(Exception $e) {
            DB::rollback();
            Log::error($e->getMessage());
            $response = array(
                'status' => config('const.status.code.INVALID_ARGUMENT'),
                'message' => config('const.status.message.INVALID_ARGUMENT'),
                'result' => $request
            );
        }
        return $response;
    }
}

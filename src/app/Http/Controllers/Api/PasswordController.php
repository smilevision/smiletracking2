<?php

namespace App\Http\Controllers\Api;

use App\Model\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class PasswordController extends Controller
{
    /**
     * パスワードリセット
     *
     * @param Request $request
     * @return void
     */
    public function reset(Request $request) {

        if($request->password <> $request->password_check) {
            $response = array(
                'status' => config('const.status.code.INVALID_ARGUMENT'),
                'message' => config('const.status.message.INVALID_ARGUMENT'),
                'result' => $request
            );
            return $response;
        }

        try {
            $user = User::where('uuid', $request->uuid)->first();

            if(empty($user)) {
                $response = array(
                    'status' => config('const.status.code.INVALID_ARGUMENT'),
                    'message' => config('const.status.message.INVALID_ARGUMENT'),
                    'result' => $request
                );
            } else {
                $user->password = Hash::make($request->password);
                $user->save();

                $response = array(
                    'status' => config('const.status.code.OK'),
                    'message' => config('const.status.message.OK'),
                    'result' => $request
                );

            }

            return $response;

        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            $response = array(
                'status' => config('const.status.code.INVALID_ARGUMENT'),
                'message' => config('const.status.message.INVALID_ARGUMENT'),
                'result' => $request
            );
        }

    }
}

<?php

namespace App\Http\Controllers\Api;

use Exception;
use App\Model\Exhibitors;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ExhibitorsDeleteApiRequest;

class ExhibitorsApiController extends Controller
{
    /**
     * 出展企業 削除
     *
     * @param ExhibitorsDeleteApiRequest $request
     * @return void
     */
    public function delete(ExhibitorsDeleteApiRequest $request)
    {
        Log::info('[uid:'.Auth::user()->id.'] ExhibitorsApiController@delete start. [id='.$request->id.']');

        try {
            DB::beginTransaction();

            $exhibitors = Exhibitors::where('id', $request->id)->first();
            $exhibitors->deleted_at = date('Y-m-d H:i:s');
            $exhibitors->save();

            DB::commit();

            $response = array(
                'status' => config('const.status.code.OK'),
                'message' => config('const.status.message.OK'),
                'result' => $exhibitors
            );

            $request->session()->put('status', 'delete_complete');

            return $response;

        } catch (Exception $e) {
            Log::error($e->getMessage());
            DB::rollback();

            $response = [
                'data'    => [],
                'status'  => 'NG',
                'summary' => 'Exception Error.',
                'errors'  => $e->getMessage(),
            ];

            $request->session()->put('status', 'delete_error');

            return new JsonResponse( $response, 500 );
        }
    }
}

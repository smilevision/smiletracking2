<?php

namespace App\Http\Controllers\Api;

use Exception;
use App\Model\User;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class ResetPasswordController extends Controller
{
    /**
     * 画像の新規登録
     *
     * @param Request $request
     * @return void
     */
    public function reset(Request $request)
    {
        Log::info('Api/ResetPasswordController@reset start[id : '.$request->id.']');

        DB::beginTransaction();

        try {
            $user = User::whereNull('deleted_at')->where('id', $request->id)->first();
            $user->password = Hash::make($user->email);
            $user->save();

            DB::commit();

            $response = array(
                'status' => config('const.status.code.OK'),
                'message' => config('const.status.message.OK'),
                'result' => $user
            );

            return $response;

        } catch (Exception $e) {
            Log::error($e->getMessage());
            DB::rollback();

            $response = [
                'data'    => [],
                'status'  => 'NG',
                'summary' => 'Exception Error.',
                'errors'  => $e->getMessage(),
            ];
            return new JsonResponse( $response, 500 );
        }
    }
}

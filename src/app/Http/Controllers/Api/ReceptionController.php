<?php

namespace App\Http\Controllers\Api;

use App\Model\Reception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;

/**
 * 入退場API Controller
 */
class ReceptionController extends Controller
{
    public function save(Request $request) {

        Log::info('event_id : '.$request->event_id);
        Log::info('entry_id : '.$request->entry_id);
        Log::info('enter_at : '.$request->enter_at);
        Log::info('exit_at : '.$request->exit_at);

        try {
            $reception = Reception::where('event_id', $request->event_id)->where('entry_id', $request->entry_id)->first();

            if(empty($reception)) {
                $reception = new Reception;
            }

            $reception->event_id = $request->event_id;
            $reception->entry_id = $request->entry_id;
            $reception->enter_at = empty($request->enter_at)?null:$request->enter_at.':00';
            $reception->exit_at = empty($request->exit_at)?null:$request->exit_at.':00';
            $reception->save();

            $response = array(
                'status' => config('const.status.code.OK'),
                'message' => config('const.status.message.OK'),
                'result' => $reception
            );

        } catch(Exception $ex) {
            Log::error($ex->getMessage());
            $response = array(
                'status' => config('const.status.code.INVALID_ARGUMENT'),
                'message' => config('const.status.message.INVALID_ARGUMENT'),
                'result' => $request
            );
        }
        return $response;
    }
}

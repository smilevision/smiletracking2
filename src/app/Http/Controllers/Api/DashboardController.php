<?php

namespace App\Http\Controllers\Api;

use stdClass;
use App\Model\Entry;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * 予約の男女比率 取得
     *
     * @param Request $request
     * @return void
     */
    public function gender_ratio(Request $request) {

        $event_id = $request->id;

        // 男女比
        $result = new stdClass;
        $entries = Entry::where('event_id', $event_id)->where('is_deleted', false)->get();
        $male_count = 0;
        $female_count = 0;
        $unknown_gender_count = 0;
        foreach ($entries as $key => $entry) {
            if($entry->gender == 1) {
                $male_count += 1;
            } else if ($entry->gender == 2) {
                $female_count += 1;
            } else {
                $unknown_gender_count += 1;
            }
        }

        $result = array(
            array(
                'value'     => $male_count,
                'color'     => '#00c0ef',
                'highlight' => '#00c0ef',
                'label'     => '男性'),
            array(
                'value'     => $female_count,
                'color'     => '#f56954',
                'highlight' => '#f56954',
                'label'     => '女性'),
            array(
                'value'     => $unknown_gender_count,
                'color'     => '#d2d6de',
                'highlight' => '#d2d6de',
                'label'     => 'その他'),
        );

        $response = array(
            'status' => config('const.status.code.OK'),
            'message' => config('const.status.message.OK'),
            'result' => $result
        );

        return $response;
    }
}

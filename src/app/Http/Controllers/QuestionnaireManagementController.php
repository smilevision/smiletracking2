<?php

namespace App\Http\Controllers;

use stdClass;
use App\Model\Event;
use App\Model\Questionnaire;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class QuestionnaireManagementController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * アンケート管理
     *
     * @param Request $request
     * @return void
     */
    public function index(Request $request) {

        Log::info('['.Auth::user()->id.'] start QuestionnaireManagementController index.');

        //　抽出条件
        $conditions = array();

        // 検索条件のアイテム
        $findItems = new stdClass;
        $findItems->event = Event::where('is_deleted', false)->whereNull('is_view')->get();

        if(empty($request->event_id)) {
            // エントリーリスト
            $entries = null;

            // 対象イベント
            $target_event = new Event;

            $questionnaires = null;
        } else {
            $conditions['search_event'] = $request->event_id;

            // エントリー一覧
            $query = Questionnaire::where('is_deleted', false);
            $query->where('event_id', $request->event_id);
            $query->orderBy('created_at', 'desc');
            $questionnaires = $query->paginate(config('const.limit.QUESTIONNAIRE_LIST'));
            $questionnaires->params = array('event_id' => $request->event_id);  // クエリストリングのパラメータ

            // 対象イベント
            $target_event = Event::where('is_deleted', false)->where('id', $request->event_id)->first();

        }

        return view('questionnaire_management.index', compact('questionnaires', 'target_event', 'findItems', 'conditions'));
    }

    /**
     * 検索結果
     *
     * @param Request $request
     * @return void
     */
    public function find(Request $request) {

        Log::info('['.Auth::user()->id.'] start QuestionnaireManagementController index.');

        //　抽出条件
        $conditions = array();
        if(!empty($request->search_event)) {
            $conditions['search_event'] = $request->search_event;
        } elseif(!empty($request->event_id)) {
            $conditions['search_event'] = $request->event_id;
            $request->search_event = $request->event_id;
        }

        // 検索条件のアイテム
        $findItems = new stdClass;
        $findItems->event = Event::where('is_deleted', false)->whereNull('is_view')->get();

        $request->merge(array('event_id' => 1));

        if(empty($request->event_id)) {
            // エントリーリスト
            $entries = null;

            // 対象イベント
            $target_event = new Event;
        } else {
            $conditions['search_event'] = $request->search_event;

            // エントリー一覧
            $query = Questionnaire::where('is_deleted', false);
            $query->where('event_id', $request->search_event);
            $query->orderBy('created_at', 'desc');
            $questionnaires = $query->paginate(config('const.limit.QUESTIONNAIRE_LIST'));
            $questionnaires->params = array('event_id' => $request->search_event);  // クエリストリングのパラメータ

            // 対象イベント
            $target_event = Event::where('is_deleted', false)->where('id', $request->search_event)->first();

        }

        return view('questionnaire_management.index', compact('questionnaires', 'target_event', 'findItems', 'conditions'));
    }

    /**
     * アンケート詳細画面
     *
     * @param Request $request
     * @return void
     */
    public function detail(Request $request) {

        $questionnaire = Questionnaire::where('id', $request->id)->first();

        return view('questionnaire_management.detail', compact('questionnaire'));
    }

}

<?php

namespace App\Http\Controllers;

use stdClass;
use App\Model\User;
use App\Model\Entry;
use App\Model\Event;
use App\Model\Reception;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

/**
 * エントリー Controller
 */
class EntryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * WEB予約者情報一覧画面
     *
     * @return void
     */
    public function index(Request $request) {

        Log::info('['.Auth::user()->id.'] start EntryController index.');

        //　抽出条件
        $conditions = array();

        // 検索条件のアイテム
        $findItems = new stdClass;
        $findItems->event = Event::whereNull('is_view')->where('is_deleted', false)->get();

        //$request->merge(array('event_id' => 1));

        if(empty($request->event_id)) {
            // エントリーリスト
            $entries = null;

            // 対象イベント
            $target_event = new Event;
        } else {
            $conditions['search_event'] = $request->event_id;

            // エントリー一覧
            $query = Entry::where('is_deleted', false);
            $query->where('event_id', $request->event_id);
            if(!empty($request->sort_item) && !empty($request->sort_mode)) {
                $column = $request->sort_item;
                $sort_type = $request->sort_mode;
            } else {
                $column = 'kname';
                $sort_type = 'asc';
            }
            $query->orderBy($column, $sort_type);
            $entries = $query->paginate(config('const.limit.ENTRY_LIST'));

            if(!empty($request->sort_item) && !empty($request->sort_mode)) {
                $query_string = array();
                $query_string['event_id'] = $request->event_id;
                $query_string['sort_mode'] = $request->sort_mode;
                $query_string['sort_item'] = $request->sort_item;
                $entries->appends($query_string);
            } else {
                $query_string = array();
                $query_string['event_id'] = $request->event_id;
                $entries->appends($query_string);
            }

            // 対象イベント
            $target_event = Event::where('is_deleted', false)->where('id', $request->event_id)->first();
        }

        return view('entry.index', compact('entries', 'target_event', 'findItems', 'conditions'));
    }

    /**
     * WEB予約者情報一覧画面 検索処理
     *
     * @param Request $request
     * @return void
     */
    public function find(Request $request) {

        Log::info('['.Auth::user()->id.'] start EntryController find.');

        //　抽出条件
        $conditions = array();
        if(!empty($request->search_event)) {
            $conditions['search_event'] = $request->search_event;
        } elseif(!empty($request->event_id)) {
            $conditions['search_event'] = $request->event_id;
            $request->search_event = $request->event_id;
        }

        // 検索条件のアイテム
        $findItems = new stdClass;
        $findItems->event = Event::whereNull('is_view')->where('is_deleted', false)->get();

        // エントリー一覧
        if(!empty($request->search_event)) {
            $query = Entry::where('is_deleted', false);
            $query->where('event_id', $request->search_event);
            //$query->orderBy('created_at', 'desc');
            $query->orderBy('kname', 'asc');
            $entries = $query->paginate(config('const.limit.ENTRY_LIST'));
            //$entries->params = array('event_id' => $request->search_event);  // クエリストリングのパラメータ

            $query_string = array();
            $query_string['event_id'] = $conditions['search_event'];
            $entries->appends($query_string);

        } else {
            $entries = null;
        }

        // 対象イベント
        if(!empty($request->search_event)) {
            $target_event = Event::where('is_deleted', false)->where('id', $request->search_event)->first();
        } else {
            $target_event = new Event;
        }

        return view('entry.index', compact('entries', 'target_event', 'findItems', 'conditions'));
    }

    /**
     * WEB予約者情報 詳細画面
     *
     * @param Request $request
     * @return void
     */
    public function detail(Request $request) {

        Log::info('['.Auth::user()->id.'] start EntryController detail. [user_id:'.$request->id.']');

        $entry = Entry::where('id', $request->id)->where('is_deleted', false)->first();

        if(empty($request->status)) {
            $status = null;
        } else {
            $status = $request->status;
        }

        return view('entry.detail', compact('entry', 'status'));
    }

    /**
     * WEB予約者情報 更新処理
     *
     * @param Request $request
     * @return void
     */
    public function update(Request $request) {

        Log::info('['.Auth::user()->id.'] start EntryController update. [user_id:'.$request->id.']');

        try {
            $entry = Entry::where('id', $request->id)->where('is_deleted', false)->first();

            $entry->name = $request->name;
            $entry->kname = $request->kname;
            $entry->gender = $request->gender;
            // $entry->birth = $request->birth;
            // $entry->zip = $request->zip;
            // $entry->address1 = $request->address1;
            // $entry->address2 = $request->address2;
            // $entry->address3 = $request->address3;
            $entry->email = $request->email;
            $entry->phone = $request->phone;
            $entry->division = $request->division;
            $entry->school = $request->school;
            $entry->gakubu = $request->gakubu;
            // $entry->school_year = $request->school_year;
            $entry->graduate_year = $request->graduate_year;
            $entry->introduction = $request->introduction;
            $entry->introduction_name = $request->introduction_name;
            $entry->introduction_school_name = $request->introduction_school_name;
            $entry->note = $request->note;

            $entry->save();

            return redirect()->route('entry.detail', array('id'=>$request->id, 'status'=>'complete'));
        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            return redirect()->route('entry.detail', array('id'=>$request->id, 'status'=>'error'));
        }
    }

    /**
     * WEB予約者情報  新規登録画面
     *
     * @param Request $request
     * @return void
     */
    public function regist(Request $request) {

        Log::info('['.Auth::user()->id.'] start EntryController regist.');

        $data = $request;
        return view('entry.regist', compact('data'));
    }

    /**
     * WEB予約者情報  新規登録確認画面
     *
     * @param Request $request
     * @return void
     */
    public function check(Request $request) {

        Log::info('['.Auth::user()->id.'] start EntryController check.');

        // バリデーション 実行
        $this->validate($request, $this->validation_rule(), $this->validation_message());

        $data = $request;
        $data->code = $this->createCode();
        return view('entry.regist_check', compact('data'));
    }

    /**
     * WEB予約者情報  登録処理
     *
     * @param Request $request
     * @return void
     */
    public function complete(Request $request) {

        Log::info('['.Auth::user()->id.'] start EntryController complete.');

        // トランザクション開始
        DB::beginTransaction();

        try {

            Log::info('event id : '.$request->event_id);

            // コード作成
            $code = $this->createCode();

            // ユーザー登録
            $user = new User;
            $user->is_authority = config('const.authority.VISITOR');
            $user->code = $code;
            $user->uuid = (string) Str::uuid();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->email);
            $user->save();

            // エントリー情報登録
            $entry = new Entry;
            $request->merge(array('code' => $code, 'users_id' => $user->id));
            $entry->fill($request->all())->save();

            // イベント受付情報の初期登録
            $reception = new Reception;
            $reception->event_id = $request->event_id;
            $reception->entry_id = $entry->id;
            $reception->save();

            // commit
            DB::commit();

            return redirect()->route('entry.regist.thunks', array('event_id'=>$request->event_id));
        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            // ロールバック
            DB::rollback();
        }
    }

    /**
     * WEB予約者情報  新規登録完了
     *
     * @param Request $request
     * @return void
     */
    public function thunks(Request $request) {
        $event_id = $request->event_id;
        return view('entry.regist_thunks', compact('event_id'));
    }

    /**
     * WEB予約者情報 削除
     *
     * @param Request $request
     * @return void
     */
    public function delete(Request $request) {

        Log::info('['.Auth::user()->id.'] start EntryController delete. [user_id:'.$request->id.']');

        try {
            $entry = Entry::where('id', $request->id)->where('is_deleted', false)->first();

            $entry->is_deleted = false;
            $entry->deleted_at = date('Y-m-d H:i:s');

            $entry->save();

            return redirect()->route('entry', array('event_id'=>$entry->event_id));
        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            return redirect()->route('entry.detail', array('id'=>$request->id, 'status'=>'error'));
        }
    }

    /**
     * 予約コード発行
     *
     * @return string 予約コード
     */
    private function createCode() {
        $head_length = 3;
        $head = substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, $head_length);
        $code_length = 4;
        $code = substr(str_shuffle('1234567890'), 0, $code_length);
        return $head . $code;
    }

    /**
     * バリデーション ルール
     * @return string[]
     */
    private function validation_rule() {
        $rule = [
            'name'          => 'required|max:255',
            'kname'         => 'required|max:255',
            'gender'        => 'required',
            //'birth'         => 'required',
            // 'zip'           => 'required|max:8',
            // 'address1'      => 'required|max:128',
            // 'address2'      => 'required|max:1024',
            // 'address3'      => 'nullable|max:1024',
            'email'         => 'required|max:128',
            'phone'         => 'required|max:16',
            'division'      => 'required|max:64',
            'school'        => 'required|max:128',
            'gakubu'        => 'nullable|max:128',
            // 'school_year'   => 'required|max:64',
            'graduate_year' => 'required|max:64',
            'introduction'  => 'required',
            'introduction_name'  => 'nullable|max:255',
            'introduction_school_name'  => 'nullable|max:255',
            'note'          => 'nullable|max:2048',
        ];

        return $rule;
    }

    /**
     * バリデーションメッセージ
     * @return string[]
     */
    private function validation_message() {
        return [
            'name.required'             => 'お名前が入力されていません。',
            'name.max'                  => 'お名前の最大文字列数を超えています。',
            'kname.required'            => 'フリガナが入力されていません。',
            'kname.max'                 => 'フリガナの最大文字列数を超えています。',
            'gender.required'           => '性別が選択されていません。',
            // 'birth.required'            => '生年月日が入力されていません。',
            // 'zip.required'              => '郵便番号が入力されていません。',
            // 'zip.max'                   => '郵便番号の最大文字列数を超えています。',
            // 'address1.required'         => '都道府県が入力されていません。',
            // 'address1.max'              => '都道府県の最大文字列数を超えています。',
            // 'address2.required'         => 'ご住所が入力されていません。',
            // 'address2.max'              => 'ご住所の最大文字列数を超えています。',
            // 'address3.max'              => 'マンション名などの最大文字列数を超えています。',
            'email.required'            => 'メールアドレスが入力されていません。',
            'email.max'                 => 'メールアドレスの最大文字列数を超えています。',
            'phone.required'            => '電話番号が入力されていません。',
            'phone.max'                 => '電話番号の最大文字列数を超えています。',
            'division.required'         => '在籍区分が入力されていません。',
            'division.max'              => '在籍区分の最大文字列数を超えています。',
            'school.required'           => '学校名が入力されていません。',
            'school.max'                => '学校名の最大文字列数を超えています。',
            'gakubu.max'                => '学部・コースなどの最大文字列数を超えています。',
            // 'school_year.required'      => '学年が入力されていません。',
            // 'school_year.max'           => '学年の最大文字列数を超えています。',
            'graduate_year.required'    => '卒業見込み年度が入力されていません。',
            'graduate_year.max'         => '卒業見込み年度の最大文字列数を超えています。',
            'introduction.required'     => 'このイベントを何で知りましたかが選択されていません。',
            'introduction_name.max'     => '紹介者名の最大文字列数を超えています。',
            'introduction_school_name.max' => '紹介者学校名の最大文字列数を超えています。',
            'note.max'                  => '備考欄の最大文字列数を超えています。',
        ];
    }
}

<?php

namespace App\Http\Controllers;

use App\Model\Entry;
use App\Model\Visit;
use App\Model\Reception;
use App\Model\Questionnaire;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

/**
 * CSVファイルダウンロード
 */
class CsvDownloadController extends Controller
{
    /**
     * 企業訪問者CSVダウンロード
     *
     * @param Request $request
     * @return void
     */
    public function visitors(Request $request) {

        Log::info('['.Auth::user()->id.'] start CsvDownloadController visitors. [exhibitors_id:'.$request->exhibitors_id.']');

        $exhibitors_id = $request->exhibitors_id;

        try {
            $vistors = Visit::where('event_id', $request->event_id)->where('exhibitors_id', $exhibitors_id)->where('is_deleted', false)->orderby('created_at', 'desc')->get();
            $entries = array();
            foreach ($vistors as $key => $vist) {
                $entry = Entry::where('id', $vist->entry_id)->where('is_deleted', false)->first();
                $entry->homon_ntz = $vist->created_at;
                array_push($entries, $entry);
            }

            $stream = fopen('php://temp', 'r+b');
            // CSVヘッダー
            fputcsv($stream, [
                'エントリーコード',
                '氏名',
                'フリガナ',
                '性別',
                // '生年月日',
                // '郵便番号',
                // '都道府県',
                // 'ご住所',
                // 'マンション名など',
                'メールアドレス',
                '電話番号',
                '在籍区分',
                '学校名',
                '学部',
                // '学年',
                '卒業見込み年度',
                '備考欄',
                '訪問日時',
            ]);
            foreach ($entries as $entry) {

                if($entry->gender == 1) {
                    $gender = '男性';
                } elseif($entry->gender == 2) {
                    $gender = '女性';
                } else {
                    $gender = '未回答';
                }

                fputcsv($stream, [
                    $entry->code,
                    $entry->name,
                    $entry->kname,
                    $gender,
                    // $entry->birth,
                    // $entry->zip,
                    // $entry->address1,
                    // $entry->address2,
                    // $entry->address3,
                    $entry->email,
                    $entry->phone,
                    $entry->division,
                    $entry->school,
                    $entry->gakubu,
                    // $entry->school_year,
                    $entry->graduate_year,
                    $entry->note,
                    $entry->homon_ntz,
                ]);
            }
            rewind($stream);
            $csv = str_replace(PHP_EOL, "\r\n", stream_get_contents($stream));
            $csv = mb_convert_encoding($csv, 'SJIS');

            $headers = array(
                'Content-Type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename*=UTF-8\'\''.rawurlencode('visitors.csv')
            );
            return response($csv, 200, $headers);

        } catch (Exception $e) {
            Log::error($e->getMessage());
        }
    }

    /**
     * アンケートCSVダウンロード
     *
     * @param Request $request
     * @return void
     */
    public function questionnaire(Request $request) {

        Log::info('['.Auth::user()->id.'] start CsvDownloadController questionnaire. [target event id:'.$request->event_id.']');

        try {
            $questionnaires = Questionnaire::where('event_id', $request->event_id)->where('is_deleted', false)->get();

            $stream = fopen('php://temp', 'r+b');
            // CSVヘッダー
            fputcsv($stream, [
                'エントリーコード',
                '氏名',
                'フリガナ',
                '学校名',
                '回答日時',
                'Q1.エントリーしたいと思える企業と出会えましたか？',
                'Q2.エントリーしたいと思った企業名を教えてください。',
                'Q3.今日の就職サミットについて、何でお知りになりましたか？',
                'Q4.就職サミットについて、魅力に感じた点を教えてください',
                'Q5.企業選びのポイントについて教えてください',
                'Q6.Amazonギフトカード (Eメールタイプ)を送信するメールアドレス',
                'Q7.その他ご意見、ご要望等があればご入力ください',
            ]);
            foreach ($questionnaires as $questionnaire) {

                if(count($questionnaire->detail) > 0) {

                    $q1 = '';
                    $q2 = '';
                    $q3 = '';
                    $q4 = '';
                    $q5 = '';
                    $q6 = '';
                    $q7 = '';

                    foreach ($questionnaire->detail as $key => $detail) {
                        if(strpos($detail->title, 'q1') !== false) {
                            if($detail->value == '1') {
                                $q1 = 'はい';
                            } elseif($detail->value == '2') {
                                $q1 = 'いいえ';
                            } else {
                                $q1 = 'どちらでもない';
                            }
                        } elseif(strpos($detail->title, 'q2') !== false) {
                            if(!empty($detail->value)) {
                                if(!empty($q2)) $q2 .= ', ';
                                $q2 .= $detail->value;
                            }
                        } elseif(strpos($detail->title, 'q3') !== false) {
                            if(!empty($detail->value)) {
                                if(!empty($q3)) $q3 .= ', ';
                                if($detail->title == 'q3_note') $q3 .= ', 紹介者名/その他の理由:' . $detail->value;

                                switch ($detail->title) {
                                    case 'q3_1':
                                        $q3 .= '柳本周介先生（将来塾・オフィスビギン）';
                                        break;
                                    case 'q3_2':
                                        $q3 .= '大学キャリアセンター';
                                        break;
                                    case 'q3_3':
                                        $q3 .= 'ネット検索';
                                        break;
                                    case 'q3_4':
                                        $q3 .= '合説ドットコム';
                                        break;
                                    case 'q3_5':
                                        $q3 .= '友達の紹介';
                                        break;
                                    case 'q3_6':
                                        $q3 .= 'Twitter';
                                        break;
                                    case 'q3_7':
                                        $q3 .= 'Instagram';
                                        break;
                                    case 'q3_8':
                                        $q3 .= 'Facebook';
                                        break;
                                    case 'q3_9':
                                        $q3 .= 'YouTube';
                                        break;
                                    case 'q3_10':
                                        $q3 .= 'ハガキDM・メール';
                                        break;
                                    case 'q3_11':
                                        $q3 .= '企業の採用ページでの案内';
                                        break;
                                    case 'q3_12':
                                        $q3 .= 'Jobway';
                                        break;
                                    case 'q3_13':
                                        $q3 .= 'ネットニュース（石渡嶺司氏）';
                                        break;
                                    case 'q3_14':
                                        $q3 .= '新聞記事（神戸新聞）';
                                        break;
                                    case 'q3_15':
                                        $q3 .= '佐保健太郎氏';
                                        break;
                                    case 'q3_16':
                                        $q3 .= '友達からの紹介';
                                        break;
                                    case 'q3_17':
                                        $q3 .= 'その他';
                                        break;
                                    default:
                                        break;
                                }
                            }
                        } elseif(strpos($detail->title, 'q4') !== false) {
                            if(!empty($detail->value)) {
                                if(!empty($q4)) $q4 .= ', ';
                                if($detail->title == 'q4_note') $q4 .= ', その他の理由:' . $detail->value;

                                switch ($detail->title) {
                                    case 'q4_1':
                                        $q4 .= '会場が近い';
                                        break;
                                    case 'q4_2':
                                        $q4 .= '魅力的な企業があった';
                                        break;
                                    case 'q4_3':
                                        $q4 .= '日程が参加しやすかった';
                                        break;
                                    case 'q4_4':
                                        $q4 .= '地元（兵庫県）で働きたい';
                                        break;
                                    case 'q4_5':
                                        $q4 .= '中小企業で働きたい';
                                        break;
                                    case 'q4_6':
                                        $q4 .= '経営者と話してみたい';
                                        break;
                                    case 'q4_7':
                                        $q4 .= '来場特典（Amazonギフトカード）が魅力的だった';
                                        break;
                                    case 'q4_8':
                                        $q4 .= 'その他';
                                        break;
                                    default:
                                        break;
                                }
                            }
                        } elseif(strpos($detail->title, 'q5') !== false) {
                            if(!empty($detail->value)) {
                                if(!empty($q5)) $q5 .= ', ';
                                if($detail->title == 'q5_note') $q5 .= ', その他の理由:' . $detail->value;

                                switch ($detail->title) {
                                    case 'q5_1':
                                        $q5 .= '興味ある企業と出会えた';
                                        break;
                                    case 'q5_2':
                                        $q5 .= '今まで考えていなかった企業と出会えた';
                                        break;
                                    case 'q5_3':
                                        $q5 .= '中小企業への理解が深まった';
                                        break;
                                    case 'q5_4':
                                        $q5 .= '社長との話が聞けてよかった';
                                        break;
                                    case 'q5_5':
                                        $q5 .= '社員さんの話が聞けて良かった';
                                        break;
                                    case 'q5_6':
                                        $q5 .= 'スタッフ(案内,誘導)の対応が助かった';
                                        break;
                                    case 'q5_7':
                                        $q5 .= 'その他';
                                        break;
                                    default:
                                        break;
                                }
                            }
                        } elseif(strpos($detail->title, 'amazon_gift') !== false) {
                            $q6 = $detail->value;

                        } elseif(strpos($detail->title, 'note') !== false) {
                            $q7 = $detail->value;
                        }
                    }

                    fputcsv($stream, [
                        $questionnaire->entry->code,
                        $questionnaire->entry->name,
                        $questionnaire->entry->kname,
                        $questionnaire->entry->school,
                        $questionnaire->created_at,
                        $q1,
                        $q2,
                        $q3,
                        $q4,
                        $q5,
                        $q6,
                        $q7,
                    ]);
                }

            }
            rewind($stream);
            $csv = str_replace(PHP_EOL, "\r\n", stream_get_contents($stream));
            $csv = mb_convert_encoding($csv, 'SJIS');

            $headers = array(
                'Content-Type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename*=UTF-8\'\''.rawurlencode('questionnaire.csv')
            );
            return response($csv, 200, $headers);

        } catch (Exception $e) {
            Log::error($e->getMessage());
        }
    }

    /**
     * エントリー情報CSVダウンロード
     *
     * @param Request $request
     * @return void
     */
    public function entry(Request $request) {

        Log::info('['.Auth::user()->id.'] start CsvDownloadController entry. [target event id:'.$request->id.']');

        try {
            $entries = Entry::where('event_id', $request->id)->where('is_deleted', false)->get();

            $stream = fopen('php://temp', 'r+b');
            // CSVヘッダー
            fputcsv($stream, [
                'エントリーコード',
                '氏名',
                'フリガナ',
                '性別',
                '生年月日',
                //'郵便番号',
                //'都道府県',
                //'ご住所',
                //'マンション名など',
                'メールアドレス',
                '電話番号',
                '在籍区分',
                '学校名',
                '学部',
                '学年',
                '卒業見込み年度',
                'このイベントを何で知りましたか',
                '紹介者名',
                '備考欄',
                'エントリー日時',
            ]);
            foreach ($entries as $entry) {

                if($entry->gender == 1) {
                    $gender = '男性';
                } elseif($entry->gender == 2) {
                    $gender = '女性';
                } else {
                    $gender = '未回答';
                }

                fputcsv($stream, [
                    $entry->code,
                    $entry->name,
                    $entry->kname,
                    $gender,
                    $entry->birth,
                    //$entry->zip,
                    //$entry->address1,
                    //$entry->address2,
                    //$entry->address3,
                    $entry->email,
                    $entry->phone,
                    $entry->division,
                    $entry->school,
                    $entry->gakubu,
                    $entry->school_year,
                    $entry->graduate_year,
                    $entry->introduction,
                    $entry->introduction_name,
                    $entry->note,
                    $entry->created_at,
                ]);
            }
            rewind($stream);
            $csv = str_replace(PHP_EOL, "\r\n", stream_get_contents($stream));
            $csv = mb_convert_encoding($csv, 'SJIS');

            $headers = array(
                'Content-Type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename*=UTF-8\'\''.rawurlencode('entry.csv')
            );
            return response($csv, 200, $headers);

        } catch (Exception $e) {
            Log::error($e->getMessage());
        }
    }

    /**
     * 入退場CSVダウンロード
     *
     * @param Request $request
     * @return void
     */
    public function reception(Request $request) {

        Log::info('['.Auth::user()->id.'] start CsvDownloadController reception. [target event id:'.$request->id.']');

        try {
            $visits = Reception::where('event_id', $request->id)->whereNotNull('enter_at')->where('is_deleted', false)->get();

            $stream = fopen('php://temp', 'r+b');
            // CSVヘッダー
            fputcsv($stream, [
                '氏名',
                'フリガナ',
                '学校名',
                '入場',
                '退場',
            ]);
            foreach ($visits as $visit) {

                fputcsv($stream, [
                    $visit->entry->name,
                    $visit->entry->kname,
                    $visit->entry->school,
                    $visit->enter_at,
                    $visit->exit_at,
                ]);
            }
            rewind($stream);
            $csv = str_replace(PHP_EOL, "\r\n", stream_get_contents($stream));
            $csv = mb_convert_encoding($csv, 'SJIS');

            $headers = array(
                'Content-Type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename*=UTF-8\'\''.rawurlencode('reception.csv')
            );
            return response($csv, 200, $headers);

        } catch (Exception $e) {
            Log::error($e->getMessage());
        }
    }
}

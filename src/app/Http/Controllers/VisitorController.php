<?php

namespace App\Http\Controllers;

use stdClass;
use App\Model\User;
use App\Model\Entry;
use App\Model\Event;
use App\Model\Visit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

/**
 * 訪問者管理 Contoller
 */
class VisitorController extends Controller
{
    /**
     * 訪問者管理 一覧画面
     *
     * @param Request $request
     * @return void
     */
    public function index(Request $request) {

        Log::info('['.Auth::user()->id.'] start VisitorController index.');

        //　抽出条件
        $conditions = array();

        // 検索条件のアイテム
        $findItems = new stdClass;
        $findItems->event = Event::whereNull('is_view')->where('is_deleted', false)->get();

        //$request->merge(array('event_id' => 1));

        if(empty($request->event_id)) {
            // エントリーリスト
            $visitors = null;

            // 対象イベント
            $target_event = new Event;
        } else {
            $conditions['search_event'] = $request->event_id;

            // 訪問者一覧
            $query = Visit::where('is_deleted', false);
            $query->where('event_id', $request->event_id);

            if(Auth::user()->is_authority == config('const.authority.EXHIBITORS')) {
                // 出展企業の場合、自社への訪問者を取得
                $query->where('exhibitors_id', Auth::user()->relation->exhibitors->id);
            }

            $query->orderBy('created_at', 'desc');
            $visitors = $query->paginate(config('const.limit.VISITOR_LIST'));
            $visitors->params = array('event_id' => $request->event_id);  // クエリストリングのパラメータ

            // 対象イベント
            $target_event = Event::where('is_deleted', false)->where('id', $request->event_id)->first();

        }

        return view('visitor.index', compact('visitors', 'target_event', 'findItems', 'conditions'));

    }

    /**
     * 訪問者管理 一覧画面（イベント選択）
     *
     * @param Request $request
     * @return void
     */
    public function find(Request $request) {

        Log::info('['.Auth::user()->id.'] start VisitorController find.');

        //　抽出条件
        $conditions = array();
        if(!empty($request->search_event)) {
            $conditions['search_event'] = $request->search_event;
        } elseif(!empty($request->event_id)) {
            $conditions['search_event'] = $request->event_id;
            $request->search_event = $request->event_id;
        }

        // 検索条件のアイテム
        $findItems = new stdClass;
        $findItems->event = Event::whereNull('is_view')->where('is_deleted', false)->get();

        // エントリー一覧
        if(!empty($request->search_event)) {

            // 訪問者一覧
            $query = Visit::where('is_deleted', false);
            $query->where('event_id', $request->search_event);

            if(Auth::user()->is_authority == config('const.authority.EXHIBITORS')) {
                // 出展企業の場合、自社への訪問者を取得
                $query->where('exhibitors_id', Auth::user()->relation->exhibitors->id);
            }

            $query->orderBy('created_at', 'desc');
            $visitors = $query->paginate(config('const.limit.VISITOR_LIST'));
            $visitors->params = array('event_id' => $request->search_event);  // クエリストリングのパラメータ

            // 対象イベント
            $target_event = Event::where('is_deleted', false)->where('id', $request->search_event)->first();

        } else {
            $visitors = null;
        }

        // 対象イベント
        if(!empty($request->search_event)) {
            $target_event = Event::where('is_deleted', false)->where('id', $request->search_event)->first();
        } else {
            $target_event = new Event;
        }

        return view('visitor.index', compact('visitors', 'target_event', 'findItems', 'conditions'));
    }


    /**
     * 訪問登録画面
     *
     * @param Request $request
     * @return void
     */
    public function regist(Request $request) {

        Log::info('['.Auth::user()->id.'] start VisitorController regist.');

        $data = $request;
        return view('visitor.regist', compact('data'));

    }

    /**
     * 訪問登録 処理
     *
     * @param Request $request
     * @return void
     */
    public function complete(Request $request) {

        Log::info('['.Auth::user()->id.'] start VisitorController regist.');

        try {
            // バリデーション 実行
            $this->validate($request, $this->validation_rule(), $this->validation_message());

            // エントリー情報
            $entry = Entry::where('code', $request->code)->where('event_id', $request->event_id)->where('is_deleted', false)->first();

            if(empty($entry)) {
                return redirect()->back()->withInput()->with('error', '予約者コードが予約されていません。');
            }

            // イベント受付情報
            $visitor = Visit::where('entry_id', $entry->id)->where('exhibitors_id', Auth::user()->relation->exhibitors->id)->where('event_id', $request->event_id)->first();

            if(empty($visitor)) {

                $visitor = new Visit;
                $visitor->event_id = $request->event_id;
                $visitor->exhibitors_id = Auth::user()->relation->exhibitors->id;
                $visitor->entry_id = $entry->id;

                // 登録処理
                $visitor->save();

            } else {
                return redirect()->back()->withInput()->with('error', '既に訪問受付が済んでいます。');
            }

            Log::info('['.Auth::user()->id.'] save : [event_id:'.$visitor->event_id.'][entry_id:'.$entry->id.'][exhibitors_id:'.$visitor->exhibitors_id.'][created_at:'.$visitor->created_at.']');

            return redirect()->back()->with('complete', '入退場登録が完了しました。');

        } catch (Exception $e) {

        }

    }

    /**
     * QRコードでの入退場受付
     *
     * @param Request $request
     * @return void
     */
    public function qr_regist(Request $request) {

        Log::info('['.Auth::user()->id.'] start VisitorController qr_regist.');

        $data = $request;
        return view('visitor.qr_regist', compact('data'));

    }

    /**
     * 入退場登録
     *
     * @param Request $request
     * @return void
     */
    public function qr_complete(Request $request) {

        Log::info('['.Auth::user()->id.'] start VisitorController qr_complete.');

        try {
            // ユーザー情報の取得
            $user = User::where('uuid', $request->uuid)->where('is_deleted', false)->first();

            if(empty($user)) {
                return redirect()->back()->withInput()->with('error', 'WEB登録されていません。');
            }

            // エントリー情報
            $entry = Entry::where('code', $user->code)->where('event_id', $request->event_id)->where('is_deleted', false)->first();

            if(empty($entry)) {
                return redirect()->back()->withInput()->with('error', '予約者コードが予約されていません。');
            }

            // イベント受付情報
            $visitor = Visit::where('entry_id', $entry->id)->where('exhibitors_id', Auth::user()->relation->exhibitors->id)->where('event_id', $request->event_id)->first();

            if(empty($visitor)) {

                $visitor = new Visit;
                $visitor->event_id = $request->event_id;
                $visitor->exhibitors_id = Auth::user()->relation->exhibitors->id;
                $visitor->entry_id = $entry->id;

                // 登録処理
                $visitor->save();

            } else {
                return redirect()->back()->withInput()->with('error', '既に訪問受付が済んでいます。');
            }

            Log::info('['.Auth::user()->id.'] save : [event_id:'.$visitor->event_id.'][entry_id:'.$entry->id.'][exhibitors_id:'.$visitor->exhibitors_id.'][created_at:'.$visitor->created_at.']');

            return redirect()->back()->with('complete', '入退場登録が完了しました。');


        } catch (Exception $e) {

        }

    }

    /**
     * バリデーション ルール
     * @return string[]
     */
    private function validation_rule() {
        $rule = [
            'code'  => 'required|regex:/^[a-zA-Z0-9]+$/|max:7',
        ];

        return $rule;
    }

    /**
     * バリデーションメッセージ
     * @return string[]
     */
    private function validation_message() {
        return [
            'code.required' => '予約者コードを入力してください',
            'code.regex'    => '予約者コードは英数字のみ入力可能です',
            'code.max'      => '予約者コードは最大７文字です',
        ];
    }
}

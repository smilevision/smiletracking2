<?php

namespace App\Mail;

use App\Model\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * 出店企業登録完了メール
 */
class RegistExhibitor extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;

    /**
     * インスタンス生成
     *
     * @param User $user ユーザー情報
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
               ->from(env('MAIL_FROM_ADDRESS'))
               ->subject(env('MAIL_REGIST_EXHIBITORS_TITLE'))
               ->view('mail.regist_exhibitor')
               ->with(['user'=>$this->user]);
    }
}

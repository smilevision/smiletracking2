<?php

namespace App\Mail;

use App\Model\Entries;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * エントリー完了メール
 */
class EntryThanksMail extends Mailable
{
    use Queueable, SerializesModels;

    /** エントリー情報 */
    protected $entry;

    /**
     * コンストラクタ
     *
     * @param Event $entry エントリー情報
     */
    public function __construct(Entries $entry)
    {
        $this->entry = $entry;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
               ->from(env('MAIL_FROM_ADDRESS'))
               ->subject(env('MAIL_ENTRY_THANKS_TITLE'))
               ->view('mail.entry_thanks')
               ->with(['entry'=>$this->entry]);
    }
}

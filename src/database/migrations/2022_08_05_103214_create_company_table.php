<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('company', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned()->comment('出展企業ID');
			$table->string('name', 1024)->default('')->comment('企業・団体名');
			$table->string('tanto', 128)->default('')->comment('担当者名');
			$table->string('zip', 10)->nullable()->default('')->comment('郵便番号');
			$table->string('address', 1024)->default('')->comment('住所');
			$table->string('phone', 24)->default('')->comment('電話番号');
			$table->string('email', 128)->default('')->comment('メールアドレス');
			$table->boolean('is_join')->default(0)->comment('参加フラグ');
			$table->timestamps(10);
			$table->boolean('is_deleetd')->default(0)->comment('削除フラグ');
			$table->softDeletes()->comment('削除日時');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('company');
	}

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('entries', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned()->comment('エントリーID');
			$table->bigInteger('users_id')->unsigned()->index('users_id')->comment('ユーザーID');
			$table->bigInteger('event_id')->unsigned()->index('event_id')->comment('イベントID');
			$table->string('code', 128)->nullable()->comment('コード');
			$table->text('values')->comment('エントリー内容（JSON）');
			$table->timestamps(10);
			$table->softDeletes()->comment('削除日時');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('entries');
	}

}

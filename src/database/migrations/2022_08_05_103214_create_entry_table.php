<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('entry', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->bigInteger('event_id')->unsigned()->comment('イベントID');
			$table->bigInteger('users_id')->unsigned()->index('users_id')->comment('ユーザーID');
			$table->string('code', 8)->index('code')->comment('予約コード');
			$table->string('name')->default('')->comment('氏名');
			$table->string('kname')->default('')->comment('氏名（カナ）');
			$table->boolean('gender')->comment('（１：男性、２：女性、０：未回答）');
			$table->date('birth')->comment('生年月日');
			$table->string('zip', 8)->nullable()->default('')->comment('郵便番号');
			$table->string('address1', 128)->nullable()->default('')->comment('都道府県');
			$table->string('address2', 1024)->nullable()->default('')->comment('住所');
			$table->string('address3', 1024)->nullable()->comment('住所（マンション名など）');
			$table->string('email')->default('')->comment('メールアドレス');
			$table->string('phone', 16)->nullable()->comment('電話番号');
			$table->string('division', 64)->default('')->comment('在籍区分（１：大学、２：大学院、３：短大・専門学校、４：既卒、９：その他）');
			$table->string('school', 128)->default('')->comment('学校名');
			$table->string('gakubu', 128)->nullable()->comment('学部');
			$table->string('school_year', 64)->default('')->comment('学年');
			$table->string('graduate_year', 64)->default('')->comment('卒業見込み年度');
			$table->string('introduction', 1024)->default()->comment('このイベントを何で知りましたか');
			$table->string('introduction_name', 255)->nullable()->comment('紹介者名');
			$table->string('introduction_school_name', 255)->nullable()->comment('紹介者学校名');
			$table->text('note')->nullable()->comment('備考欄');
			$table->timestamps(10);
			$table->boolean('is_deleted')->default(0)->comment('削除フラグ');
			$table->softDeletes()->comment('削除日時');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('entry');
	}

}

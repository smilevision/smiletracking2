<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToQuestionnaireDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('questionnaire_detail', function(Blueprint $table)
		{
			$table->foreign('questionnaire_id', 'questionnaire_detail_ibfk_1')->references('id')->on('questionnaire')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('questionnaire_detail', function(Blueprint $table)
		{
			$table->dropForeign('questionnaire_detail_ibfk_1');
		});
	}

}

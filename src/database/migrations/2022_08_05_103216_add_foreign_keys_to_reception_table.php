<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToReceptionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('reception', function(Blueprint $table)
		{
			$table->foreign('entry_id', 'reception_ibfk_1')->references('id')->on('entry')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('event_id', 'reception_ibfk_2')->references('id')->on('events')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('reception', function(Blueprint $table)
		{
			$table->dropForeign('reception_ibfk_1');
			$table->dropForeign('reception_ibfk_2');
		});
	}

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisitTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('visit', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned()->comment('訪問受付ID');
			$table->bigInteger('event_id')->unsigned()->index('event_id')->comment('イベントID');
			$table->bigInteger('exhibitors_id')->unsigned()->index('exhibitors_id')->comment('出展企業ID');
			$table->bigInteger('entry_id')->unsigned()->index('entry_id')->comment('エントリーID');
			$table->timestamps(10);
			$table->boolean('is_deleted')->default(0)->comment('削除フラグ');
			$table->softDeletes()->comment('削除日時');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('visit');
	}

}

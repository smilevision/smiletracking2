<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentsValuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contents_values', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned()->comment('ID');
			$table->bigInteger('contents_id')->unsigned()->index('contents_id')->comment('コンテンツID');
			$table->string('value', 1024)->default('')->comment('値');
			$table->timestamps(10);
			$table->boolean('is_deleted')->default(0)->comment('削除フラグ');
			$table->softDeletes()->comment('削除日時');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contents_values');
	}

}

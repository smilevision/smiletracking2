<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contents', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned()->comment('コンテンツID');
			$table->bigInteger('event_id')->unsigned()->index('event_id')->comment('イベントID');
			$table->string('title', 1024)->default('')->comment('タイトル');
			$table->boolean('is_require')->default(0)->comment('必須有無');
			$table->integer('tag')->comment('HTMLタグ');
			$table->string('type', 128)->nullable()->comment('type');
			$table->string('name', 1024)->nullable()->comment('name');
			$table->string('class', 1024)->nullable()->comment('クラス名');
			$table->string('placeholder', 1024)->nullable()->comment('placeholder');
			$table->string('rows', 128)->nullable()->comment('textareaのrows');
			$table->integer('number')->comment('順序');
			$table->timestamps(10);
			$table->boolean('is_deleted')->default(0)->comment('削除フラグ');
			$table->softDeletes()->comment('削除日時');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contents');
	}

}

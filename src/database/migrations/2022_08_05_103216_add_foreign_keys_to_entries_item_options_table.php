<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToEntriesItemOptionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('entries_item_options', function(Blueprint $table)
		{
			$table->foreign('entries_item_id', 'entries_item_options_ibfk_1')->references('id')->on('entries_item')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('entries_item_options', function(Blueprint $table)
		{
			$table->dropForeign('entries_item_options_ibfk_1');
		});
	}

}

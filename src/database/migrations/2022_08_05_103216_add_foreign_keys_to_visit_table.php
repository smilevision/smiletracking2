<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToVisitTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('visit', function(Blueprint $table)
		{
			$table->foreign('exhibitors_id', 'visit_ibfk_1')->references('id')->on('exhibitors')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('entry_id', 'visit_ibfk_2')->references('id')->on('entry')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('event_id', 'visit_ibfk_3')->references('id')->on('events')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('visit', function(Blueprint $table)
		{
			$table->dropForeign('visit_ibfk_1');
			$table->dropForeign('visit_ibfk_2');
			$table->dropForeign('visit_ibfk_3');
		});
	}

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntriesItemOptionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('entries_item_options', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned()->comment('オプションID');
			$table->bigInteger('entries_item_id')->unsigned()->index('entries_item_id')->comment('エントリー項目ID');
			$table->string('title', 2048)->default('')->comment('オプション名');
			$table->integer('sort_no')->nullable();
			$table->timestamps(10);
			$table->softDeletes()->comment('削除日時');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('entries_item_options');
	}

}

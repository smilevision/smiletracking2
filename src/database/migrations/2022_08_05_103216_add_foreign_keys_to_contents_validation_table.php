<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToContentsValidationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contents_validation', function(Blueprint $table)
		{
			$table->foreign('contents_id', 'contents_validation_ibfk_1')->references('id')->on('contents')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contents_validation', function(Blueprint $table)
		{
			$table->dropForeign('contents_validation_ibfk_1');
		});
	}

}

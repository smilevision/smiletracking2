<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('events', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned()->comment('イベントID');
			$table->bigInteger('parent_id')->unsigned()->nullable()->comment('親イベントID');
			$table->string('code', 8)->default('')->unique('code')->comment('主催者コード');
			$table->string('name', 256)->default('')->comment('イベント名');
			$table->string('tanto_name', 1024)->nullable()->comment('担当者/責任者');
			$table->string('tanto_email', 1024)->nullable()->comment('担当者メール');
			$table->string('tanto_contact', 1024)->nullable()->comment('担当者コンタクト（電話・SNS等）');
			$table->dateTime('started_at')->nullable()->comment('開始日時');
			$table->dateTime('ended_at')->nullable()->comment('終了日時');
			$table->dateTime('reserv_started_at')->nullable()->comment('予約 開始日時');
			$table->dateTime('reserv_ended_at')->nullable()->comment('予約 終了日時');
			$table->integer('accepting')->nullable()->comment('当日受付開始');
			$table->boolean('is_view')->nullable()->comment('表示・非表示');
			$table->timestamps(10);
			$table->boolean('is_deleted')->default(0)->comment('削除フラグ');
			$table->softDeletes()->comment('削除日時');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('events');
	}

}

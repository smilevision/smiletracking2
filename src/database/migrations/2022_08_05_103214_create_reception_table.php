<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReceptionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reception', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->bigInteger('entry_id')->unsigned()->index('entry_id')->comment('エントリーID');
			$table->bigInteger('event_id')->unsigned()->index('event_id')->comment('イベントID');
			$table->dateTime('enter_at')->nullable()->comment('入場日時');
			$table->dateTime('exit_at')->nullable()->comment('退場日時');
			$table->timestamps(10);
			$table->boolean('is_deleted')->default(0)->comment('削除フラグ');
			$table->softDeletes()->comment('削除日時');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reception');
	}

}

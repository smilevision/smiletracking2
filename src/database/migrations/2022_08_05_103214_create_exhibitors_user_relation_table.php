<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExhibitorsUserRelationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('exhibitors_user_relation', function(Blueprint $table)
		{
			$table->bigInteger('exhibitors_id')->unsigned()->index('exhibitor_id');
			$table->bigInteger('users_id')->unsigned()->index('users_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('exhibitors_user_relation');
	}

}

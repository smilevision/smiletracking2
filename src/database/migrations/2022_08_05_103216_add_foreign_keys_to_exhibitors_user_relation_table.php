<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToExhibitorsUserRelationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('exhibitors_user_relation', function(Blueprint $table)
		{
			$table->foreign('exhibitors_id', 'exhibitors_user_relation_ibfk_1')->references('id')->on('exhibitors')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('users_id', 'exhibitors_user_relation_ibfk_2')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('exhibitors_user_relation', function(Blueprint $table)
		{
			$table->dropForeign('exhibitors_user_relation_ibfk_1');
			$table->dropForeign('exhibitors_user_relation_ibfk_2');
		});
	}

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_detail', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned()->comment('担当者ID');
			$table->bigInteger('users_id')->unsigned()->index('users_id')->comment('ユーザーID');
			$table->string('contact', 1024)->nullable()->comment('担当者コンタクト（電話・SNS等）');
			$table->timestamps(10);
			$table->boolean('is_deleted')->default(0)->comment('削除フラグ');
			$table->softDeletes()->comment('削除日時');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_detail');
	}

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyEventsRelationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('company_events_relation', function(Blueprint $table)
		{
			$table->bigInteger('company_id')->unsigned()->index('company_id')->comment('企業・団体名ID');
			$table->bigInteger('events_id')->unsigned()->index('events_id')->comment('イベントID');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('company_events_relation');
	}

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlaceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('place', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned()->comment('会場ID');
			$table->bigInteger('event_id')->unsigned()->index('event_id')->comment('イベントID');
			$table->string('name', 128)->comment('会場名');
			$table->string('zip1', 5)->nullable()->comment('郵便番号');
			$table->string('zip2', 5)->nullable()->comment('郵便番号');
			$table->string('address', 2048)->nullable()->default('')->comment('会場住所');
			$table->string('phone', 24)->nullable()->default('')->comment('電話番号');
			$table->timestamps(10);
			$table->boolean('is_deleted')->default(0)->comment('削除フラグ');
			$table->softDeletes()->comment('削除日時');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('place');
	}

}

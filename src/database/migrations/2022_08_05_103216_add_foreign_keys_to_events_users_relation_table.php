<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToEventsUsersRelationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('events_users_relation', function(Blueprint $table)
		{
			$table->foreign('event_id', 'events_users_relation_ibfk_1')->references('id')->on('events')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('users_id', 'events_users_relation_ibfk_2')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('events_users_relation', function(Blueprint $table)
		{
			$table->dropForeign('events_users_relation_ibfk_1');
			$table->dropForeign('events_users_relation_ibfk_2');
		});
	}

}

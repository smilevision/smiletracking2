<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned()->comment('ユーザーID');
			$table->integer('is_authority')->comment('権限（１：管理者、２：イベンター、３：出展企業担当、４：ユーザー）');
			$table->string('code', 7)->default('')->unique('code')->comment('ユーザーコード');
			$table->string('uuid')->default('')->comment('UUID');
			$table->string('name')->default('')->comment('氏名');
			$table->string('email')->nullable()->comment('メールアドレス');
			$table->dateTime('email_verified_at')->nullable()->comment('メールアドレス確認');
			$table->string('password')->default('')->comment('パスワード');
			$table->string('remember_token', 100)->nullable()->comment('トークンキー');
			$table->timestamps(10);
			$table->boolean('is_deleted')->default(0)->comment('削除フラグ');
			$table->softDeletes()->comment('削除日時');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}

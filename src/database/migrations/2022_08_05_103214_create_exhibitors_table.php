<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExhibitorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('exhibitors', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned()->comment('出展企業ID');
			$table->bigInteger('event_id')->unsigned()->index('event_id')->comment('イベントID');
			$table->string('code', 8)->default('')->index('code')->comment('出展企業コード');
			$table->string('name', 1024)->default('')->comment('企業・団体名');
			$table->string('tanto', 128)->nullable()->default('')->comment('担当者名');
			$table->string('zip1', 5)->nullable()->comment('郵便番号');
			$table->string('zip2', 5)->nullable()->comment('郵便番号');
			$table->string('address', 1024)->nullable()->default('')->comment('住所');
			$table->string('phone', 24)->nullable()->default('')->comment('電話番号');
			$table->string('email', 128)->nullable()->default('')->comment('メールアドレス');
			$table->text('note')->nullable()->comment('その他備考、説明等');
			$table->boolean('is_join')->default(0)->comment('参加フラグ');
			$table->timestamps(10);
			$table->boolean('is_deleted')->default(0)->comment('削除フラグ');
			$table->softDeletes()->comment('削除日時');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('exhibitors');
	}

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntriesItemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('entries_item', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned()->comment('エントリー項目ID');
			$table->bigInteger('event_id')->unsigned()->index('event_id')->comment('イベントID');
			$table->string('title', 2048)->default('')->comment('タイトル');
			$table->bigInteger('style_id')->unsigned()->index('style_id')->comment('型ID');
			$table->integer('digits')->nullable()->comment('桁数');
			$table->boolean('is_ required')->nullable()->comment('必須条件');
			$table->integer('sort_no')->comment('表示順');
			$table->boolean('is_list_view')->nullable()->comment('管理画面のリスト表示');
			$table->timestamps(10);
			$table->softDeletes()->comment('削除日時');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('entries_item');
	}

}

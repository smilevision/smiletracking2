<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToEntriesItemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('entries_item', function(Blueprint $table)
		{
			$table->foreign('event_id', 'entries_item_ibfk_1')->references('id')->on('events')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('style_id', 'entries_item_ibfk_2')->references('id')->on('entries_item_style')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('entries_item', function(Blueprint $table)
		{
			$table->dropForeign('entries_item_ibfk_1');
			$table->dropForeign('entries_item_ibfk_2');
		});
	}

}

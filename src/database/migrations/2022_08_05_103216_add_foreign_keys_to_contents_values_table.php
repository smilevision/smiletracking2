<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToContentsValuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contents_values', function(Blueprint $table)
		{
			$table->foreign('contents_id', 'contents_values_ibfk_1')->references('id')->on('contents')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contents_values', function(Blueprint $table)
		{
			$table->dropForeign('contents_values_ibfk_1');
		});
	}

}

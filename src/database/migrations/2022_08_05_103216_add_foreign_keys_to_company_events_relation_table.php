<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToCompanyEventsRelationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('company_events_relation', function(Blueprint $table)
		{
			$table->foreign('company_id', 'company_events_relation_ibfk_1')->references('id')->on('company')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('events_id', 'company_events_relation_ibfk_2')->references('id')->on('events')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('company_events_relation', function(Blueprint $table)
		{
			$table->dropForeign('company_events_relation_ibfk_1');
			$table->dropForeign('company_events_relation_ibfk_2');
		});
	}

}

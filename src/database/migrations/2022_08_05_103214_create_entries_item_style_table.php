<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntriesItemStyleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('entries_item_style', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned()->comment('型ID');
			$table->string('name', 128)->default('')->comment('型名');
			$table->boolean('is_option')->nullable()->comment('オプション有無');
			$table->timestamps(10);
			$table->softDeletes()->comment('削除日時');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('entries_item_style');
	}

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToQuestionnaireTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('questionnaire', function(Blueprint $table)
		{
			$table->foreign('entry_id', 'questionnaire_ibfk_1')->references('id')->on('entry')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('event_id', 'questionnaire_ibfk_2')->references('id')->on('events')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('questionnaire', function(Blueprint $table)
		{
			$table->dropForeign('questionnaire_ibfk_1');
			$table->dropForeign('questionnaire_ibfk_2');
		});
	}

}

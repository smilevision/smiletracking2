/**
 * 登録情報 JavaScript
 */
$(function () {

    /**
     * モーダル初期化
     */
  $('#modal-reset_password').on('show.bs.modal', function (e) {
    $('#old_password').val("");
    $('#password').val("");
    $('#password_check').val("");
  });

  /**
   * パスワード再設定
   */
  $('#btn-reset_password').click(function(){

    $('#reset_password_fail').css('display', 'none');

    // 更新用データ作成
    var send_data = {
      'old_password' : $('#old_password').val(),
      'password' : $('#password').val(),
      'password_check' : $('#password_check').val(),
      'uuid' : $('#uuid').val()
    }

    // 登録・更新
    $.ajax({
        url:'/api/password_reset',
        type: 'post',
        data : JSON.stringify(send_data),
        contentType: 'application/json',
        dataType: "json",
    })
    .done(function(response){
      console.log(response);
      if(response.status == 200) {
        $('#modal-reset_password').modal('hide');
        alert('パスワードを再設定しました。');
      } else {
        $('#reset_password_fail').css('display', 'block');
      }
    })
    .fail(function(error){
      console.log(error);
      $('#reset_password_fail').css('display', 'block');
    });
  });
});

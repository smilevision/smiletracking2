/**
 * イベント応募画面 JavaScript
 */
 $(function () {

    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass   : 'iradio_flat-green'
      })

    //Datemask yyyy/mm/dd
    $('#birth').inputmask('yyyy/mm/dd', { 'placeholder': 'yyyy/mm/dd' });

    //Datemask 999-9999
    $('#zip').inputmask('999-9999', { 'placeholder': '000-0000' });

    $('#overlay').hide();
    $('#form').submit(function(){
      $('#overlay').show();
    });

 })

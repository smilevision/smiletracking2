/**
 * アンケート画面 JavaScript
 */
$(function () {
    // ラジオボタンのデザイン
    $('.radio_btn_operation').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    });

    // オーバーレイ
    $('#overlay').hide();
    $('#overlay_list').hide();
    $('#search_event').change(function(){
        $('#overlay').show();
        $('#overlay_list').show();
    });

    $('#form').submit(function(){
      $('#overlay').show();
    });

  });

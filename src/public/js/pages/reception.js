/**
 * 入退場情報画面 JavaScript
 */
$(function () {

    /** DateTimePicker 日本語化 */
    $.datetimepicker.setLocale('ja');

    $('.enter_at').datetimepicker({
      format:"Y-m-d H:i",
    });

    $('.exit_at').datetimepicker({
      format:"Y-m-d H:i",
    });

    // オーバーレイ
    $('#overlay').hide();
    $('#overlay_list').hide();
    $('#search_event').change(function(){
        $('#overlay').show();
        $('#overlay_list').show();
    });

    $('#form').submit(function(){
      $('#overlay').show();
    });

    /**
     *
     */
    $('.btn_save').click(function() {
        var event_id = $('#event_id').val();
        var entry_id = $(this).closest('tr').find('input[name=entry_id]').val();
        var enter_at = $(this).closest('tr').find('.enter_at').val();
        var exit_at = $(this).closest('tr').find('.exit_at').val();

        console.log('event_id : '+ event_id);
        console.log('entry_id : '+ entry_id);
        console.log('enter_at : '+ enter_at);
        console.log('exit_at : '+ exit_at);

        if(enter_at=='' && exit_at!='') {
            alert('入場日時を入力してください。');
            return;
        }

        var send_data = {
            'event_id' : event_id,
            'entry_id' : entry_id,
            'enter_at' : enter_at,
            'exit_at' : exit_at,
        };

        $.ajax({
            url:'/api/reception',
            type: 'post',
            data : JSON.stringify(send_data),
            contentType: 'application/json',
            dataType: "json",
        })
        .done(function(response){
        })
        .fail(function(error){
        });
    });
  });

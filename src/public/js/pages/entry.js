/**
 * エントリー情報画面 JavaScript
 */
$(function () {

  // 日付計算（２２年前）
  var dt = new Date();
  dt.setFullYear(dt.getFullYear() - 22);

  // 生年月日 DatePicker
  $('#birth').datepicker({
    format: 'yyyy/mm/dd',
    language:'ja',
    defaultViewDate:dt.getFullYear() + '/04/01'
  });

  // オーバーレイ
  $('#overlay').hide();
  $('#overlay_list').hide();
  $('#search_event').change(function(){
      $('#overlay').show();
      $('#overlay_list').show();
  });

  $('#form').submit(function(){
    $('#overlay').show();
  });

  /**
   * 削除処理
   */
  $('#delete_entry').click(function(){

    if(!confirm('本当に削除しますか？')){
        return;
    }

    $('#overlay').show();

    var id = $('#entry_id').val();

    var send_data = {
        'id' : id,
    };

    $.ajax({
        url:'/api/entry',
        type: 'delete',
        data : JSON.stringify(send_data),
        contentType: 'application/json',
        dataType: "json",
    })
    .done(function(response){
        // 削除成功
        location.href = '/entry?event_id='+response.result.event_id;
    })
    .fail(function(error){
        // 削除失敗
        $('#delete_message').css('display', 'block');
        $('#overlay').hide();
    });
  });
});

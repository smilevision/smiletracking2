/**
 * 出展企業情報画面 JavaScript
 */
$(function () {

  // オーバーレイ
  $('#overlay').hide();
  $('#overlay_list').hide();
  $('#search_event').change(function(){
    $('#overlay').show();
    $('#overlay_list').show();
  });

  $('#form').submit(function(){
    $('#overlay').show();
  });

  $('#operation_form').submit(function(){
    $('#overlay').show();
  })

  // ラジオボタンのデザイン
  $('.radio_btn_operation').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass   : 'iradio_flat-green'
  });

  //
  switch ($('#status').val()) {
    case 'success_update':
        toastr["success"]("出展企業情報を更新しました。", "成功");
        break;
    case 'error_update':
        toastr["error"]("出展企業情報の更新が失敗しました。", "エラー");
        break;
    case 'success_insert':
        toastr["success"]("出展企業情報を登録しました。", "成功");
        break;
    case 'error_insert':
        toastr["error"]("出展企業情報の登録が失敗しました。", "エラー");
        break;
    case 'delete_complete':
        toastr["success"]("出展企業情報を削除しました。", "成功");
        break;
    default:
        break;
}

  /** パスワード再設定 */
  $('#reset_password').click(function() {

    if(!confirm('本当に変更しますか？')){
        return false;
    } else {
        let target_id = $(this).data('id');
        console.log('id : ' + target_id);

        // 再設定
        $.ajax({
            url:'/api/password_reset/email?id='+target_id,
            type: 'post',
            dataType: "json",
        })
        .done(function(response){
            console.log(response);
            toastr["success"]("パスワード再設定が成功しました。", "成功");
        })
        .fail(function(error){
            console.log(error);
            toastr["error"]("パスワード再設定が失敗しました。", "エラー");
        });
    }
  });

  $('#delete').click(function() {

    if(!confirm('本当に削除しますか？')){
        // キャンセル
        return false;
    } else {
        let target_id = $('#exhibitors_id').val();

        // 新規登録
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url:'/api/exhibitors?id='+target_id,
            type: 'delete',
            dataType:'json',
        })
        .done(function(response){
            console.log(response);
            window.location.href = "/exhibitors?event_id="+response.result.event_id;
        })
        .fail(function(error){
            console.log(error);

            set_error_message(error.responseJSON.errors);

            toastr["error"]("出展企業情報 削除が失敗しました。", "エラー");
            $('#overlay').hide();
        });
    }
  })
});

/**
 * イベント作成画面 JavaScript
 */
$(function () {

  /** DateTimePicker 日本語化 */
  $.datetimepicker.setLocale('ja');

  /** [検索用]開催開始日時 */
  $('#search_started_at').datetimepicker({
    timepicker:false,
    format:"Y-m-d",
  });

  /** [検索用]開催終了日時 */
  $('#search_ended_at').datetimepicker({
    timepicker:false,
    format:"Y-m-d",
  });

  /** 開催開始日時 */
  $('#started_at').datetimepicker({
    format:"Y-m-d H:i",
  });

  /** 開催終了日時 */
  $('#ended_at').datetimepicker({
    format:"Y-m-d H:i",
  });

  /** 予約開始日時 */
  $('#reserv_started_at').datetimepicker({
    format:"Y-m-d H:i",
  });

  /** 予約開始日時 */
  $('#reserv_ended_at').datetimepicker({
    format:"Y-m-d H:i",
  });

  $('#overlay').hide();
  $('#form').submit(function(){
    $('#overlay').show();
  });
});

@extends('layouts.app')
@include('layouts.header')
@include('layouts.leftmenu')
@include('layouts.footer')

@section('pageCss')

@endsection

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-info-circle"></i> ご登録情報
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        @if(!empty($status))
        @if($status=='complete')
        <div class="alert alert-success alert-dismissible">
          <h4><i class="icon fa fa-check"></i> 更新成功！</h4>
          出展企業情報を更新しました。
        </div>
        @else
        <div class="alert alert-danger alert-dismissible">
          <h4><i class="icon fa fa-ban"></i> 更新失敗！</h4>
          出展企業情報の更新が失敗しました。
        </div>
        @endif
        @endif
        <form method="post" action="{{ route('user.update') }}" class="form-horizontal">
          @csrf
          <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
            <div class="col-md-12">
              <div class="form-group" style="margin-top:16px;">
                @if(Auth::user()->is_authority == config('const.authority.VISITOR'))
                <label class="col-xs-12 col-md-2 control-label">予約コード</label>
                @else
                <label class="col-xs-12 col-md-2 control-label">企業コード</label>
                @endif
                <label class="col-xs-12 col-md-8 control-value">{{ $user->code }}</label>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">ご登録者名　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="col-xs-12 col-md-8">
                  <input type="text" name="name" class="form-control" placeholder="ご登録者名" value="{{ old('name', $user->name) }}">
                </div>
              </div>
              @if(Auth::user()->is_authority == config('const.authority.EXHIBITORS'))
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">郵便番号　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="col-xs-6 col-md-2">
                  <input type="text" name="zip1" class="form-control" placeholder="000" value="{{ old('zip1', $user->zip1) }}">
                </div>
                <div class="col-xs-6 col-md-2">
                  <input type="text" name="zip2" class="form-control" placeholder="0000" value="{{ old('zip2', $user->zip2) }}">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">住所</label>
                <div class="col-xs-12 col-md-8">
                  <input type="text" name="address" class="form-control" placeholder="住所" value="{{ old('address', $user->address) }}">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">電話番号</label>
                <div class="col-xs-12 col-md-8">
                  <input type="text" name="phone" class="form-control" placeholder="電話番号" value="{{ old('phone', $user->phone) }}">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">担当者名</label>
                <div class="col-xs-12 col-md-8">
                  <input type="text" name="tanto" class="form-control" placeholder="担当者名" value="{{ old('tanto', '') }}">
                </div>
              </div>
              @endif
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">メールアドレス</label>
                <div class="col-xs-12 col-md-8">
                  <input type="text" name="email" class="form-control" placeholder="メールアドレス" value="{{ old('email', $user->email) }}">
                </div>
              </div>
              @if(Auth::user()->is_authority == config('const.authority.EXHIBITORS'))
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">追記事項等</label>
                <div class="col-xs-12 col-md-8">
                  <textarea class="form-control" rows="5" name="note" placeholder="追記事項等を入力してください。">{{ old('note', '') }}</textarea>
                </div>
              </div>
              @endif
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">認証情報</label>
                <div class="col-xs-12 col-md-4">
                  <button type="button" class="btn btn-sm btn-block btn-warning" data-toggle="modal" data-target="#modal-reset_password">認証情報変更</button>
                  <p style="margin-top:8px;">認証情報を変更される場合は、<font class="text-danger">現在のパスワードの入力が必要になります。</font></p>
                </div>
              </div>
              @if(Auth::user()->is_authority == config('const.authority.EXHIBITORS') || Auth::user()->is_authority == config('const.authority.VISITOR'))
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">QRコード</label>
                <div class="col-xs-12 col-md-4">
                  <div class="row">
                    @if(Auth::user()->is_authority == config('const.authority.EXHIBITORS'))
                    {!! QrCode::size(240)->generate(route('qrlogin', array('uuid'=>$user->uuid))) !!}
                    @elseif(Auth::user()->is_authority == config('const.authority.VISITOR'))
                    {!! QrCode::size(240)->generate('id='.$user->uuid) !!}
                    @endif
                  </div>
                  <div class="row">
                    <a href="{{ route('qrcode.download', array('uuid' => $user->uuid)) }}" class="btn btn-block btn-default btn-sm" target="_blank">ダウンロード</a>
                  </div>
                </div>
              </div>
              @endif
            </div>
          </div>
          <div class="box-footer">
            <div class="pull-right">
              <input type="submit" class="btn btn-block btn-success btn-sm" value="　内容更新　">
            </div>
            <div class="pull-right" style="margin-right: 8px;">
              <input type="button" class="btn btn-block btn-default btn-sm" onclick="location.href='{{ route('dashboard') }}'" value="　戻　　る　">
            </div>
          </div>
        </form>
      </div>
    </div>
  </section>
  <!-- /.content -->
@endsection

{{-- パスワード再設定 モーダル画面 --}}
<div class="modal fade" id="modal-reset_password" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span></button>
          <h4 class="modal-title">パスワード再設定</h4>
        </div>
        <div class="modal-body">
          <div id="reset_password_fail" class="alert alert-danger alert-dismissible col-md-offset-1 col-md-10" style="margin-top:8px; display: none;">
            <h4><i class="icon fa fa-check"></i>パスワードの更新が失敗しました。</h4>
            <div id="error_message">
            パスワードの更新が失敗しました。<br>
            システム管理者に連絡してください。
            </div>
          </div>
          <input type="hidden" id="uuid" value="{{ $user->uuid }}">
          <div class="form-group row">
            <label for="company_name" class="col-xs-12 col-md-4 control-label">旧パスワード</label>
            <div class="col-xs-12 col-md-8">
            <input type="password" id="old_password" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label for="company_name" class="col-xs-12 col-md-4 control-label">新パスワード</label>
            <div class="col-xs-12 col-md-8">
            <input type="password" id="password" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label for="company_name" class="col-xs-12 col-md-4 control-label">パスワード（確認用）</label>
            <div class="col-xs-12 col-md-8">
            <input type="password" id="password_check" class="form-control" value="">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-default pull-left" data-dismiss="modal">キャンセル</button>
          <button type="button" id="btn-reset_password" class="btn btn-sm btn-primary">再設定</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

@extends('layouts.app')
@include('layouts.header')
@include('layouts.leftmenu')
@include('layouts.footer')

@section('pageCss')

@endsection

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-users"></i> 訪問者管理 - 新規登録
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    @if (session('error'))
    <div class="alert alert-danger alert-dismissible">
      <h4><i class="icon fa fa-ban"></i> 受付失敗！</h4>
      {{ session('error') }}
    </div>
    @endif
    @if (session('complete'))
    <div class="alert alert-success alert-dismissible">
      <h4><i class="icon fa fa-check"></i> 更新成功！</h4>
      受付登録が成功しました。
    </div>
    @endif
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <!-- /.box-header -->
          <form method="post" action="{{ route('visitor.complete', array('event_id'=>$data->event_id)) }}" class="form-horizontal">
            @csrf
            <div class="box-body">
              <div class="col-md-12">
                <div class="form-group col-xs-12" style="margin-top:16px;">
                  <div class="col-xs-12 col-md-offset-2 col-md-6">
                    <button type="button" id="btn_qr_reader" class="btn btn-block btn-default btn-sm" onclick="location.href='{{ route('visitor.qr_regist', array('event_id'=>$data->event_id)) }}'">QRコード受付</button>
                  </div>
                </div>
                <div class="form-group" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-2 control-label">予約者コード</label>
                  <div class="col-xs-12 col-md-6 @if($errors->has('code')) has-error @endif">
                    <input type="text" name="code" class="form-control" placeholder="予約者コード" value="{{ old('code') }}">
                    <span class="help-block error">{{$errors->first('code')}}</span>
                  </div>
                </div>
                <input type="hidden" name="exhibitors_id" value="{{ $data->exhibitors_id }}">
              </div>
            </div>
            <div class="box-footer">
              <div class="pull-right" style="margin-right: 8px;">
                <input type="submit" class="btn btn-block btn-success btn-sm"  value="　訪問受付　">
              </div>
              <div class="pull-right" style="margin-right: 8px;">
                <input type="button" class="btn btn-block btn-default btn-sm" onclick="location.href='{{ route('visitor', array('event_id'=>$data->event_id)) }}'" value="　戻　　る　">
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
  <!-- /.content -->
@endsection

@extends('layouts.app')
@include('layouts.header')
@include('layouts.leftmenu')
@include('layouts.footer')

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-users"></i> 訪問者管理
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box">
      <form method="post" id="search_form" action="{{ route('visitor.find') }}" class="form-horizontal">
        @csrf
        <div class="box-body">
          <div class="col-xs-12 col-md-12">
            <div class="form-group">
              <label class="col-md-1 control-label">イベント</label>
              <div class="col-md-4">
                <select name="search_event" id="search_event" class="form-control" onchange="submit(this.form)">
                  <option value="">イベント選択なし</option>
                  @foreach ($findItems->event as $event)
                  <option value="{{ $event->id }}" @if(!empty($conditions) && $conditions['search_event'] == $event->id) selected @endif>{{ $event->name }}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-md-2">
                <a href="{{ route('event.detail', array('id'=>$target_event->id)) }}" class="btn btn-block btn-warning btn-sm @if(empty($target_event->id))disabled @endif"> イベント情報</a>
              </div>
            </div>
          </div>
        </div>
      </form>
      <div id="overlay" class="overlay">
        <i class="fa fa-refresh fa-spin" style="z-index:99"></i>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">@if(empty($visitors)) 訪問者情報はございません。 @else {{ $target_event->name }}：訪問者情報（{{ $visitors->total() }} 件）@endif</h3>
            <div class="pull-right">
              <a href="{{ route('visitor.regist', array('event_id'=>$target_event->id)) }}" class="btn btn-block btn-warning btn-sm @if(empty($target_event->id) || Auth::user()->is_authority < config('const.authority.EXHIBITORS'))disabled @endif"><i class="fa fa-play" style="margin-right:6px;"></i> 新規登録</a>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            @if(!empty($visitors))
            <input type="hidden" id="event_id" value="{{ $target_event->id }}">
            <div class="table-responsive">
              <table class="table table-bordered table-striped">
                <tr>
                  <th>予約コード</th>
                  <th>名前</th>
                  <th>名前（カナ）</th>
                  <th>訪問先企業</th>
                  <th>学校名</th>
                  <th>訪問日時</th>
                  {{--
                  @if(Auth::user()->is_authority < config('const.authority.EXHIBITORS'))
                  <th>操作</th>
                  @endif
                  --}}
                </tr>
                @foreach ($visitors as $visitor)
                <tr>
                  <input type="hidden" name="entry_id" value="{{ $visitor->id }}">
                  <td style="vertical-align:middle;">{{ $visitor->entry->code }}</td>
                  <td style="vertical-align:middle;">{{ $visitor->entry->name }}</td>
                  <td style="vertical-align:middle;">{{ $visitor->entry->kname }}</td>
                  <td style="vertical-align:middle;">{{ $visitor->exhibitors->name }}</td>
                  <td style="vertical-align:middle;">{{ $visitor->entry->school }}</td>
                  <td style="vertical-align:middle;">
                    {{ $visitor->created_at->format('Y-m-d H:i') }}
                    {{--
                    @if(Auth::user()->is_authority == config('const.authority.EXHIBITORS'))
                    {{ $visitor->created_at->format('Y-m-d H:i') }}
                    @else
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" id="created_at" name="created_at" class="created_at form-control pull-right" placeholder="{{ Date('Y-m-d H:00') }}" value="{{ old('enter_at', $visitor->created_at->format('Y-m-d H:i')) }}">
                    </div>
                    @endif
                    --}}
                  </td>
                  {{--
                  @if(Auth::user()->is_authority < config('const.authority.EXHIBITORS'))
                  <td style="vertical-align:middle;">
                    <input type="button" class="btn btn-block btn-warning btn-sm btn_save" value="更新">
                  </td>
                  @endif
                  --}}
                </tr>
                @endforeach
              </table>
              {{ $visitors->appends($visitors->params)->links() }}
            </div>
            @endif
          </div>
          <div id="overlay_list" class="overlay">
            <i class="fa fa-refresh fa-spin" style="z-index:99"></i>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- /.content -->
@endsection

@extends('layouts.app')
@include('layouts.header')
@include('layouts.leftmenu')
@include('layouts.footer')

@section('pageCss')

@endsection

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-users"></i> 訪問者管理 - QRコード受付
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    @if (session('error'))
    <div class="alert alert-danger alert-dismissible">
      <h4><i class="icon fa fa-ban"></i> 受付失敗！</h4>
      {{ session('error') }}
    </div>
    @endif
    @if (session('complete'))
    <div class="alert alert-success alert-dismissible">
      <h4><i class="icon fa fa-check"></i> 更新成功！</h4>
      受付登録が成功しました。
    </div>
    @endif
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <!-- /.box-header -->
          <form method="post" action="{{ route('visitor.qr_complete', array('event_id'=>$data->event_id)) }}" class="form-horizontal">
            @csrf
            <input type="hidden" id="uuid" name="uuid" value="">
            <div class="box-body">
              <div id="camera-container" class="col-md-12">
                <!-- カメラ映像 -->
                <video id="camera" autoplay playsinline muted></video>

                <!-- 処理用 -->
                <canvas id="picture" width="640" height="480"></canvas>

                <!-- 読み取り結果 -->
                <div id="result">
                  <small>※ここに読み取り結果が表示されます※</small>
                </div>
              </div>
            </div>
            <div class="box-footer">
              <div class="pull-right" style="margin-right: 8px;">
                <input type="submit" id="reception" class="btn btn-block btn-success btn-sm"  value="　受　　付　">
              </div>
              <div class="pull-right" style="margin-right: 8px;">
                <input type="button" class="btn btn-block btn-default btn-sm" onclick="location.href='{{ route('dashboard') }}'" value="　戻　　る　">
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
  <!-- /.content -->
@endsection

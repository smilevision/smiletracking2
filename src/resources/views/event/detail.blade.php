@extends('layouts.app')
@include('layouts.header')
@include('layouts.leftmenu')
@include('layouts.footer')

@section('pageCss')

@endsection

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-calendar"></i> イベント概要
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        @if(!empty($status))
        @if($status=='complete')
        <div class="alert alert-success alert-dismissible">
          <h4><i class="icon fa fa-check"></i> 更新成功！</h4>
          イベント情報を更新しました。
        </div>
        @else
        <div class="alert alert-danger alert-dismissible">
          <h4><i class="icon fa fa-ban"></i> 更新失敗！</h4>
          イベント情報の更新が失敗しました。
        </div>
        @endif
        @endif
        <form method="post" id="form" action="{{ route('event.update', array('id'=>$event->id)) }}" class="form-horizontal">
          @csrf
          <div class="box">
          <div class="box-body">
            <div class="col-md-12">
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">状態</label>
                  <div class="col-xs-12 col-md-8 checkbox">
                    <label for="is_view"><input type="checkbox" name="is_view" value="1" @if(old('is_view', $event->is_view)==1) checked @endif> 非表示</label>
                  </div>
                </div>
                <div class="form-group" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-2 control-label">開催名　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                  <div class="col-xs-12 col-md-8">
                    <input type="text" name="name" class="form-control" placeholder="開催名" value="{{ old('name', $event->name) }}" autocomplete="off">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">会場　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                  <div class="col-xs-12 col-md-8">
                    <input type="text" name="place_name" class="form-control" placeholder="会場" value="{{ old('place_name', $event->place->name) }}" autocomplete="off">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">会場 住所</label>
                  <div class="col-xs-12 col-md-8">
                    <input type="text" name="place_address" class="form-control" placeholder="会場 住所" value="{{ old('place_address', $event->place->address) }}" autocomplete="off">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">会場 電話番号</label>
                  <div class="col-xs-12 col-md-8">
                    <input type="text" name="place_phone" class="form-control" placeholder="会場 電話番号" value="{{ old('place_phone', $event->place->phone) }}" autocomplete="off">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">担当者/責任者</label>
                  <div class="col-xs-12 col-md-8">
                    <input type="text" name="tanto_name" class="form-control" placeholder="担当者/責任者" value="{{ old('tanto_name', $event->tanto_name) }}" autocomplete="off">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">担当者メール</label>
                  <div class="col-xs-12 col-md-8">
                    <input type="text" name="tanto_email" class="form-control" placeholder="担当者メール" value="{{ old('tanto_email', $event->tanto_email) }}" autocomplete="off">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">担当者コンタクト（電話・SNS等）</label>
                  <div class="col-xs-12 col-md-8">
                    <input type="text" name="tanto_contact" class="form-control" placeholder="担当者コンタクト（電話・SNS等）" value="{{ old('tanto_contact', $event->tanto_contact) }}" autocomplete="off">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">開催日時</label>
                  <div class="col-xs-12 col-md-8">
                    <div class="row" style="margin-bottom:8px;">
                      <label class="col-xs-2 col-md-1 control-label">開始</label>
                      <div class="col-xs-10 col-md-8">
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" id="started_at" name="started_at" class="form-control pull-right" placeholder="{{ Date('Y-m-d H:00') }}" value="{{ old('started_at', empty($event->started_at)?'':$event->started_at->format('Y-m-d H:i')) }}" autocomplete="off">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <label class="col-xs-2 col-md-1 control-label">終了</label>
                      <div class="col-xs-10 col-md-8">
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" id="ended_at" name="ended_at" class="form-control pull-right" placeholder="{{ Date('Y-m-d H:00') }}" value="{{ old('ended_at', empty($event->ended_at)?'':$event->ended_at->format('Y-m-d H:i')) }}" autocomplete="off">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">受付日時</label>
                  <div class="col-xs-12 col-md-8">
                    <div class="row" style="margin-bottom:8px;">
                      <label class="col-xs-2 col-md-1 control-label">開始</label>
                      <div class="col-xs-10 col-md-8">
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" id="reserv_started_at" name="reserv_started_at" class="form-control pull-right" placeholder="{{ Date('Y-m-d H:00') }}" value="{{ old('reserv_started_at', empty($event->reserv_started_at)?'':$event->reserv_started_at->format('Y-m-d H:i')) }}" autocomplete="off">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <label class="col-xs-2 col-md-1 control-label">終了</label>
                      <div class="col-xs-10 col-md-8">
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" id="reserv_ended_at" name="reserv_ended_at" class="form-control pull-right" placeholder="{{ Date('Y-m-d H:00') }}" value="{{ old('reserv_ended_at', empty($event->reserv_ended_at)?'':$event->reserv_ended_at->format('Y-m-d H:i')) }}" autocomplete="off">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">当日受付開始</label>
                  <label class="col-xs-4 col-md-1 control-label">開始時刻より：</label>
                  <div class="col-xs-4 col-md-1">
                    <input type="text" name="accepting" class="form-control" style="text-align:right;" placeholder="３０" value="{{ old('accepting', $event->accepting) }}" autocomplete="off">
                  </div>
                  <label class="col-xs-2 col-md-1 control-label" style="margin-left: -24px;margin-top: 6px;text-align:left;">分前</label>
                </div>
              </div>
          </div>
          <div class="box-footer">
            <div class="pull-right">
              <input type="submit" class="btn btn-block btn-success btn-sm" value="　更　　新　">
            </div>
            {{--
            <div class="pull-right" style="margin-right: 8px;">
              <input type="button" class="btn btn-block btn-danger btn-sm" value="　削　　除　">
            </div>
            --}}
            <div class="pull-right" style="margin-right: 8px;">
              <input type="button" class="btn btn-block btn-default btn-sm" onclick="location.href='{{ route('event') }}'" value="　戻　　る　">
            </div>
          </div>
        </form>
        <div id="overlay" class="overlay">
          <i class="fa fa-refresh fa-spin" style="z-index:99"></i>
        </div>
      </div>
    </div>
  </section>
  <!-- /.content -->
@endsection

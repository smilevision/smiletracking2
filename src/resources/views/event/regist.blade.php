@extends('layouts.app')
@include('layouts.header')
@include('layouts.leftmenu')
@include('layouts.footer')

@section('pageCss')

@endsection

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-pencil-square-o"></i> イベント作成
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <form method="post" action="{{ route('event.regist.check') }}" class="form-horizontal">
          @csrf
          <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
            <div class="col-md-12">
              <div class="form-group" style="margin-top:16px;">
                <label class="col-xs-12 col-md-2 control-label">開催名　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="col-xs-12 col-md-8">
                  <input type="text" name="name" class="form-control" placeholder="開催名" value="{{ old('name', '') }}" autocomplete="off">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">会場　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="col-xs-12 col-md-8">
                  <input type="text" name="place_name" class="form-control" placeholder="会場" value="{{ old('place_name', '') }}" autocomplete="off">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">会場 住所</label>
                <div class="col-xs-12 col-md-8">
                  <input type="text" name="place_address" class="form-control" placeholder="会場 住所" value="{{ old('place_address', '') }}" autocomplete="off">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">会場 電話番号</label>
                <div class="col-xs-12 col-md-8">
                  <input type="text" name="place_phone" class="form-control" placeholder="会場 電話番号" value="{{ old('place_phone', '') }}" autocomplete="off">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">担当者/責任者</label>
                <div class="col-xs-12 col-md-8">
                  <input type="text" name="tanto_name" class="form-control" placeholder="担当者/責任者" value="{{ old('tanto_name', '') }}" autocomplete="off">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">担当者メール</label>
                <div class="col-xs-12 col-md-8">
                  <input type="text" name="tanto_email" class="form-control" placeholder="担当者メール" value="{{ old('tanto_email', '') }}" autocomplete="off">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">担当者コンタクト（電話・SNS等）</label>
                <div class="col-xs-12 col-md-8">
                  <input type="text" name="tanto_contact" class="form-control" placeholder="担当者コンタクト（電話・SNS等）" value="{{ old('tanto_contact', '') }}" autocomplete="off">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">開催日時</label>
                <div class="col-xs-12 col-md-8">
                  <div class="row" style="margin-bottom:8px;">
                    <label class="col-xs-2 col-md-1 control-label">開始</label>
                    <div class="col-xs-10 col-md-8">
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" id="started_at" name="started_at" class="form-control pull-right" placeholder="{{ Date('Y-m-d H:00') }}" value="{{ old('started_at', '') }}" autocomplete="off">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <label class="col-xs-2 col-md-1 control-label">終了</label>
                    <div class="col-xs-10 col-md-8">
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" id="ended_at" name="ended_at" class="form-control pull-right" placeholder="{{ Date('Y-m-d H:00') }}" value="{{ old('ended_at', '') }}" autocomplete="off">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">受付日時</label>
                <div class="col-xs-12 col-md-8">
                  <div class="row" style="margin-bottom:8px;">
                    <label class="col-xs-2 col-md-1 control-label">開始</label>
                    <div class="col-xs-10 col-md-8">
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" id="reserv_started_at" name="reserv_started_at" class="form-control pull-right" placeholder="{{ Date('Y-m-d H:00') }}" value="{{ old('reserv_started_at', '') }}" autocomplete="off">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <label class="col-xs-2 col-md-1 control-label">終了</label>
                    <div class="col-xs-10 col-md-8">
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" id="reserv_ended_at" name="reserv_ended_at" class="form-control pull-right" placeholder="{{ Date('Y-m-d H:00') }}" value="{{ old('reserv_ended_at', '') }}" autocomplete="off">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">当日受付開始</label>
                <label class="col-xs-4 col-md-1 control-label">開始時刻より：</label>
                <div class="col-xs-4 col-md-1">
                  <input type="text" name="accepting" class="form-control" style="text-align:right;" placeholder="３０" value="{{ old('accepting', '') }}" autocomplete="off">
                </div>
                <label class="col-xs-2 col-md-1 control-label" style="margin-left: -24px;margin-top: 6px;text-align:left;">分前</label>
              </div>
            </div>
          </div>
          <div class="box-footer">
            <div class="pull-right">
              <input type="submit" class="btn btn-block btn-success btn-sm" value="　内容確認　">
            </div>
            <div class="pull-right" style="margin-right: 8px;">
              <input type="button" class="btn btn-block btn-default btn-sm" onclick="location.href='{{ route('event') }}'" value="　戻　　る　">
            </div>
          </div>
        </form>
      </div>
    </div>
  </section>
  <!-- /.content -->
@endsection

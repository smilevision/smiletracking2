@extends('layouts.app')
@include('layouts.header')
@include('layouts.leftmenu')
@include('layouts.footer')

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-calendar"></i> イベント一覧
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <form method="post" action="{{ route('event.find') }}" class="form-horizontal">
      @csrf
      <div class="box @if(empty($conditions)) collapsed-box @endif">
        <div class="box-header with-border">
          <h3 class="box-title">検索条件</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
              <i class="fa @if(empty($conditions)) fa-plus @else fa-minus @endif"></i>
            </button>
          </div>
        </div>
        <div class="box-body">
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-3 control-label">開催名</label>
              <div class="col-md-9">
                <input type="text" name="search_name" class="form-control" placeholder="開催名" value="{{ !empty($conditions['search_name']) ? $conditions['search_name'] : '' }}">
              </div>
            </div>
            <div class="form-group">
              <label for="delivery_date" class="col-md-3 control-label">開催日</label>
              <div class="col-md-4">
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" id="search_started_at" name="search_started_at" class="form-control pull-right" placeholder="{{ Date('Y-m-d') }}" value="{{ !empty($conditions['search_started_at']) ? $conditions['search_started_at'] : '' }}">
                </div>
              </div>
              <div class="col-md-1">
                <label for="">〜</label>
              </div>
              <div class="col-md-4">
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" id="search_ended_at" name="search_ended_at" class="form-control pull-right" placeholder="{{ Date('Y-m-d') }}" value="{{ !empty($conditions['search_ended_at']) ? $conditions['search_ended_at'] : '' }}">
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-3 control-label">状態</label>
              <div class="col-md-9">
                <select name="search_status" class="form-control">
                  <option value="">選択なし</option>
                  <option value="{{ config('const.event_status.NONE') }}"{{ config('const.event_status.NONE')==(!empty($conditions['search_status']) ? $conditions['search_status'] : '') ? ' selected' : '' }}>未定</option>
                  <option value="{{ config('const.event_status.BEFOR') }}"{{ config('const.event_status.BEFOR')==(!empty($conditions['search_status']) ? $conditions['search_status'] : '') ? ' selected' : '' }}>開催前</option>
                  <option value="{{ config('const.event_status.NOW') }}"{{ config('const.event_status.NOW')==(!empty($conditions['search_status']) ? $conditions['search_status'] : '') ? ' selected' : '' }}>開催中</option>
                  <option value="{{ config('const.event_status.ENDED') }}"{{ config('const.event_status.ENDED')==(!empty($conditions['search_status']) ? $conditions['search_status'] : '') ? ' selected' : '' }}>終了</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">会場</label>
              <div class="col-md-9">
                <input type="text" name="search_place_name" class="form-control" placeholder="会場" value="{{ !empty($conditions['search_place_name']) ? $conditions['search_place_name'] : '' }}">
              </div>
            </div>
          </div>
        </div>
        <div class="box-footer">
          <button type="submit" class="btn btn-primary btn-sm pull-right" style="margin-left:8px;">検　索</button>
          <button type="button" class="btn btn-default btn-sm pull-right" onclick="location.href='{{ route('event') }}'">クリア</button>
        </div>
      </div>
    </form>

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">イベント一覧</h3>
            <div class="pull-right">
              <a href="{{ route('event.regist') }}" class="btn btn-block btn-warning btn-sm" style="margin-right:6px;"></i> 新規登録</a>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="table-responsive">
              <table class="table table-bordered table-striped">
                <tr>
                  <th>状態</th>
                  <th>開催名</th>
                  <th>会場</th>
                  <th>開催日時</th>
                  <th>状態</th>
                  <th>出展数</th>
                  <th>予約数</th>
                  <th>参加数</th>
                  <th>予約参加率</th>
                  <th>応募URL</th>
                  <th width="80px">開催概要</th>
                  <th width="80px">出展一覧</th>
                  {{-- <th width="80px">事前/参加</th> --}}
                </tr>
                @foreach ($events as $event)
                <tr>
                  <td style="vertical-align:middle;">@if(empty($event->is_view)) <span class="label label-success">表示</span> @else <span class="label label-danger">非表示</span> @endif</td>
                  <td style="vertical-align:middle;">{{ $event->name }}</td>
                  <td style="vertical-align:middle;">{{ $event->place->name }}</td>
                  <td style="vertical-align:middle;">
                    <p>開：{{ empty($event->started_at)?'':$event->started_at->format('Y/m/d H:i') }}</p>
                    <p>終：{{ empty($event->ended_at)?'':$event->ended_at->format('Y/m/d H:i') }}</p>
                  </td>
                  <td style="vertical-align:middle;">{{ $event->status }}</td>
                  <td style="vertical-align:middle;">{{ mb_convert_kana($event->exhibitors_count, 'N') }} 社</td>
                  <td style="vertical-align:middle;">{{ mb_convert_kana($event->entry_count, 'N') }} 人</td>
                  <td style="vertical-align:middle;">{{ mb_convert_kana($event->reception_count, 'N') }} 人</td>
                  <td style="vertical-align:middle;">{{ mb_convert_kana($event->sanka_rate, 'N') }} ％</td>
                  <td style="vertical-align:middle;">
                    <a href="{{ $event->event_form_url->vip }}" target="_blank">{{ Str::limit($event->event_form_url->vip, 20) }}</a><br>
                    <a href="{{ $event->event_form_url->business }}" target="_blank">{{ Str::limit($event->event_form_url->business, 20) }}</a><br>
                    <a href="{{ $event->event_form_url->standard }}" target="_blank">{{ Str::limit($event->event_form_url->standard, 20) }}</a>
                  </td>
                  <td style="vertical-align:middle;">
                    <a href="{{ route('event.detail', array('id'=>$event->id)) }}" class="btn btn-block btn-warning btn-sm">概要</a>
                  </td>
                  <td style="vertical-align:middle;">
                    <a href="{{ route('exhibitors', array('event_id'=>$event->id)) }}" class="btn btn-block btn-warning btn-sm">出展一覧</a>
                  </td>
                </tr>
                @endforeach
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- /.content -->
@endsection

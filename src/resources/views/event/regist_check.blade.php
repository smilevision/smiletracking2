@extends('layouts.app')
@include('layouts.header')
@include('layouts.leftmenu')
@include('layouts.footer')

@section('pageCss')

@endsection

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-pencil-square-o"></i> イベント作成 - 入力内容確認
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <form method="post" action="{{ route('event.regist.complete') }}" class="form-horizontal">
          @csrf
          <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
            <div class="col-md-12">
              <div class="form-group" style="margin-top:16px;">
                <label class="col-xs-12 col-md-2 control-label">開催名　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <label class="col-xs-12 col-md-8 control-value">{{ $data->name }}</label>
                <input type="hidden" name="name" value="{{ $data->name }}">
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">会場　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <label class="col-xs-12 col-md-8 control-value">{{ $data->place_name }}</label>
                <input type="hidden" name="place_name" value="{{ $data->place_name }}">
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">会場 住所</label>
                <label class="col-xs-12 col-md-8 control-value">{{ $data->place_address }}</label>
                <input type="hidden" name="place_address" value="{{ $data->place_address }}">
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">会場 電話番号</label>
                <label class="col-xs-12 col-md-8 control-value">{{ $data->place_phone }}</label>
                <input type="hidden" name="place_phone" value="{{ $data->place_phone }}">
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">担当者/責任者</label>
                <label class="col-xs-12 col-md-8 control-value">{{ $data->tanto_name }}</label>
                <input type="hidden" name="tanto_name" value="{{ $data->tanto_name }}">
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">担当者メール</label>
                <label class="col-xs-12 col-md-8 control-value">{{ $data->tanto_email }}</label>
                <input type="hidden" name="tanto_email" value="{{ $data->tanto_email }}">
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">担当者コンタクト（電話・SNS等）</label>
                <label class="col-xs-12 col-md-8 control-value">{{ $data->tanto_contact }}</label>
                <input type="hidden" name="tanto_contact" value="{{ $data->tanto_contact }}">
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">開催日時</label>
                <div class="col-xs-12 col-md-8">
                  <div class="row" style="margin-bottom:8px;">
                    <label class="col-xs-2 col-md-1 control-label">開始</label>
                    <label class="col-xs-10 col-md-8 control-value">{{ $data->started_at }}</label>
                    <input type="hidden" name="started_at" value="{{ $data->started_at }}">
                  </div>
                  <div class="row">
                    <label class="col-xs-2 col-md-1 control-label">終了</label>
                    <label class="col-xs-10 col-md-8 control-value">{{ $data->ended_at }}</label>
                    <input type="hidden" name="ended_at" value="{{ $data->ended_at }}">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">予約日時</label>
                <div class="col-xs-12 col-md-8">
                  <div class="row" style="margin-bottom:8px;">
                    <label class="col-xs-2 col-md-1 control-label">開始</label>
                    <label class="col-xs-10 col-md-8 control-value">{{ $data->reserv_started_at }}</label>
                    <input type="hidden" name="reserv_started_at" value="{{ $data->reserv_started_at }}">
                  </div>
                  <div class="row">
                    <label class="col-xs-2 col-md-1 control-label">終了</label>
                    <label class="col-xs-10 col-md-8 control-value">{{ $data->reserv_ended_at }}</label>
                    <input type="hidden" name="reserv_ended_at" value="{{ $data->reserv_ended_at }}">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">当日受付開始</label>
                <label class="col-xs-4 col-md-2 control-value">開始時刻より：{{ $data->accepting }} 分前</label>
                <input type="hidden" name="accepting" value="{{ $data->accepting }}">
              </div>
            </div>
          </div>
          <div class="box-footer">
            <div class="pull-right">
              <input type="submit" class="btn btn-block btn-success btn-sm" value="　登　　録　">
            </div>
            <div class="pull-right" style="margin-right: 8px;">
              <input type="button" class="btn btn-block btn-default btn-sm" onclick="location.href='{{ route('event') }}'" value="　戻　　る　">
            </div>
          </div>
        </form>
      </div>
    </div>
  </section>
  <!-- /.content -->
@endsection

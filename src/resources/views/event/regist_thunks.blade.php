@extends('layouts.app')
@include('layouts.header')
@include('layouts.leftmenu')
@include('layouts.footer')

@section('pageCss')

@endsection

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-pencil-square-o"></i> イベント作成 - 登録完了
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
          <div class="col-md-12">
            <div class="form-group has-success">
              <label class="control-label" for="inputSuccess" style="font-size:24px;"><i class="fa fa-check"></i> イベント新規登録：ご登録完了</label>
            </div>
            <p>登録していただいたメールアドレス宛に、登録完了メールをお送りいたしましたのでご確認ください。</p>
          </div>
        </div>
        <div class="box-footer">
          <div class="col-xs-12 col-md-offset-5 col-md-2">
            <a href="{{ route('event') }}" class="btn btn-block btn-sm">　トップページへ戻る　</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- /.content -->
@endsection

<!doctype html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <style type="text/css">
    @font-face {
      font-family: ipag;
      font-style: normal;
      font-weight: normal;
      font-size: 48px;
      src: url('{{ storage_path('fonts/ipag.ttf') }}') format('truetype');
    }
    @font-face {
      font-family: ipag;
      font-style: bold;
      font-weight: bold;
      font-size: 48px;
      src: url('{{ storage_path('fonts/ipag.ttf') }}') format('truetype');
    }
    body {
      font-family: ipag !important;
    }
  </style>
</head>
<body>
  <div>
    <table>
      <tr>
        <td>
          <div style="padding:15px; font-size:36pxp;">
            @if($user->is_authority == config('const.authority.EXHIBITORS'))
            {{ $user->name }}
            @elseif($user->is_authority == config('const.authority.VISITOR'))
            お名前：{{ $user->entry->kname }}
            @endif
          </div>
        </td>
      </tr>
      <tr>
        <td>
          <div style="padding:15px; font-size:36pxp;">
            @if($user->is_authority == config('const.authority.EXHIBITORS'))
            企業コード：{{ $user->code }}
            @elseif($user->is_authority == config('const.authority.VISITOR'))
            エントリーコード：{{ $user->code }}
            @endif
          </div>
        </td>
      </tr>
      @if($user->is_authority == config('const.authority.EXHIBITORS'))
      <tr>
        <td>
          <div style="padding-left:15px; font-size:36pxp;">
            初期パスワード：
          </div>
          <div style="padding-left:15px; font-size:36pxp;">
            {{ $user->email }}
          </div>
        </td>
      </tr>
      @endif
      <tr>
        <td>
          <div class="center-block" style="padding:30px;">
            @if($user->is_authority == config('const.authority.EXHIBITORS'))
            <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(680)->generate(route('qrlogin', array('uuid'=>$user->uuid)))) !!} ">
            @elseif($user->is_authority == config('const.authority.VISITOR'))
            <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(680)->generate('id='.$user->uuid)) !!} ">
            @endif
          </div>
        </td>
      </tr>
    </table>
  </div>
</body>
</html>

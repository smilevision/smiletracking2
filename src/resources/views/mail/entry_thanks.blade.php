{{ $entry->user->name }}様<br>
<br>
この度は、「{{ $entry->event->name }}」にエントリー頂きまして、誠にありがとうございます。<br>
<br>
入場時に下のQRコードを提示してください。<br>
<br>
<img src="{{ asset('images/qr/entry/'.$entry->user->uuid.'.png') }}">
<br>
では、当日会場にてお会いできるのを楽しみにしております。<br>
<br>
予約コード：{{ $entry->code }}<br>
お名前　　：{{ $entry->user->name }}<br>
パスワード：{{ $entry->user->email }}<br>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{ asset('bower_components/jvectormap/jquery-jvectormap.css') }}">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">

  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('dist/css/skins/_all-skins.min.css') }}">

  <link rel="stylesheet" href="{{ asset('css/pages.css') }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-black-light sidebar-mini">
<div class="wrapper">

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-edit"></i> アンケート登録
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach (array_unique($errors->all()) as $error_message)
            <li>{{ $error_message }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    @if (session('error'))
    <div class="alert alert-danger alert-dismissible">
      <h4><i class="icon fa fa-ban"></i> 受付失敗！</h4>
      {{ session('error') }}
    </div>
    @endif
    <div class="row">
      <div class="col-md-12">
        <form method="post" action="{{ route('questionnaire.regist.complete', array('event_id'=>$data->event_id)) }}" class="form-horizontal">
          @csrf
          <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
            <div class="col-md-12">
                <input type="hidden" name="event_id" value="{{ $data->event_id }}">
                <div class="form-group" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-2 control-label">エントリーコード(予約コード) <span class="pull-right-container"><small class="label bg-red" style="margin-top:4px;">必須</small></span></label>
                  <div class="col-xs-12 col-md-7 @if($errors->has('code')) has-error @endif">
                    <input type="text" name="code" class="form-control" placeholder="エントリーコード" value="{{ old('code', '') }}">
                    <span class="help-block error">{{$errors->first('code')}}</span>
                  </div>
                </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-offset-1 col-md-10 control-label" style="text-align: left;">Q1.エントリーしたいと思える企業と出会えましたか？ <span class="pull-right-container"><small class="label bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="radio col-xs-12 col-md-offset-1 col-md-10 @if($errors->has('q1')) has-error @endif">
                  <label>
                    <input type="radio" name="q1" value="1" @if(old('q1')=='1') checked @endif> はい　
                  </label>
                  <label>
                    <input type="radio" name="q1" value="2" @if(old('q1')=='2') checked @endif> いいえ　
                  </label>
                  <label>
                    <input type="radio" name="q1" value="0" checked> どちらでもない
                  </label>
                  <span class="help-block error">{{$errors->first('q1')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-offset-1 col-md-10 control-label" style="text-align: left;">Q2.エントリーしたいと思った企業名を教えてください。 <span class="pull-right-container"><small class="label bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="col-xs-12 col-md-offset-1 col-md-8 @if($errors->has('q2_1')) has-error @endif">
                  <div class="row" style="margin-top: 8px;">
                    <div class="col-md-12">
                      <input type="text" name="q2_1" class="form-control" placeholder="１社目" value="{{ old('q2_1', '') }}">
                    </div>
                  </div>
                  <div class="row" style="margin-top: 8px;">
                    <div class="col-md-12">
                      <input type="text" name="q2_2" class="form-control" placeholder="２社目" value="{{ old('q2_2', '') }}">
                    </div>
                  </div>
                  <div class="row" style="margin-top: 8px;">
                    <div class="col-md-12">
                      <input type="text" name="q2_3" class="form-control" placeholder="３社目" value="{{ old('q2_3', '') }}">
                    </div>
                  </div>
                  <span class="help-block error">{{$errors->first('q2_1')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-offset-1 col-md-10 control-label" style="text-align: left;">Q3.今日のひょうご就職サミットついて、何でお知りになりましたか？　(＊チェックボックス複数選択可) <span class="pull-right-container"><small class="label bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="radio col-xs-12 col-md-offset-1 col-md-10 @if($errors->has('q3_1')) has-error @endif">
                  <label>
                    <input type="checkbox" name="q3_1" class="radio_btn_operation flat-red" value="1" @if(old('q3_1')==1) checked @endif>　柳本周介先生（将来塾・オフィスビギン）
                  </label>
                  <label>
                    <input type="checkbox" name="q3_2" class="radio_btn_operation flat-red" value="1" @if(old('q3_2')==1) checked @endif>　大学キャリアセンター
                  </label>
                  <label>
                    <input type="checkbox" name="q3_3" class="radio_btn_operation flat-red" value="1" @if(old('q3_3')==1) checked @endif>　ネット検索
                  </label>
                  <label>
                    <input type="checkbox" name="q3_4" class="radio_btn_operation flat-red" value="1" @if(old('q3_4')==1) checked @endif>　合説ドットコム
                  </label>
                  <label>
                    <input type="checkbox" name="q3_5" class="radio_btn_operation flat-red" value="1" @if(old('q3_5')==1) checked @endif>　友達の紹介
                  </label>
                  <label>
                    <input type="checkbox" name="q3_6" class="radio_btn_operation flat-red" value="1" @if(old('q3_6')==1) checked @endif>　Twitter
                  </label>
                  <label>
                    <input type="checkbox" name="q3_7" class="radio_btn_operation flat-red" value="1" @if(old('q3_7')==1) checked @endif>　Instagram
                  </label>
                  <label>
                    <input type="checkbox" name="q3_8" class="radio_btn_operation flat-red" value="1" @if(old('q3_8')==1) checked @endif>　Facebook
                  </label>
                  <label>
                    <input type="checkbox" name="q3_9" class="radio_btn_operation flat-red" value="1" @if(old('q3_9')==1) checked @endif>　YouTube
                  </label>
                  <label>
                    <input type="checkbox" name="q3_10" class="radio_btn_operation flat-red" value="1" @if(old('q3_10')==1) checked @endif>　ハガキDM・メール
                  </label>
                  <label>
                    <input type="checkbox" name="q3_11" class="radio_btn_operation flat-red" value="1" @if(old('q3_11')==1) checked @endif>　企業の採用ページでの案内
                  </label>
                  <label>
                    <input type="checkbox" name="q3_12" class="radio_btn_operation flat-red" value="1" @if(old('q3_12')==1) checked @endif>　Jobway
                  </label>
                  <label>
                    <input type="checkbox" name="q3_13" class="radio_btn_operation flat-red" value="1" @if(old('q3_13')==1) checked @endif>　ネットニュース（石渡嶺司氏）
                  </label>
                  <label>
                    <input type="checkbox" name="q3_14" class="radio_btn_operation flat-red" value="1" @if(old('q3_14')==1) checked @endif>　新聞記事（神戸新聞）
                  </label>
                  <label>
                    <input type="checkbox" name="q3_15" class="radio_btn_operation flat-red" value="1" @if(old('q3_15')==1) checked @endif>　佐保健太郎氏
                  </label>
                  <label>
                    <input type="checkbox" name="q3_16" class="radio_btn_operation flat-red" value="1" @if(old('q3_16')==1) checked @endif>　友達からの紹介
                  </label>
                  <label>
                    <input type="checkbox" name="q3_17" class="radio_btn_operation flat-red" value="1" @if(old('q3_17')==1) checked @endif>　その他
                  </label>
                  <div class="row" style="margin-top: 8px;">
                    <label class="col-xs-12 col-md-10 control-label" style="text-align: left;">紹介者名/その他の理由</label>
                    <div class="col-xs-12 col-md-8 @if($errors->has('q3_note')) has-error @endif">
                      <input type="text" name="q3_note" class="form-control" placeholder="紹介者名/その他の理由" value="{{ old('q3_note', '') }}">
                      <span class="help-block error">{{$errors->first('q3_note')}}</span>
                    </div>
                  </div>
                  <span class="help-block error">{{$errors->first('q3_1')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-offset-1 col-md-10 control-label" style="text-align: left;">Q4.就職サミットについて、魅力に感じた点を教えてください　(＊チェックボックス複数選択可) <span class="pull-right-container"><small class="label bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="radio col-xs-12 col-md-offset-1 col-md-10 @if($errors->has('q4_1')) has-error @endif">
                  <div class="row">
                    <label>
                      <input type="checkbox" name="q4_1" class="radio_btn_operation flat-red" value="1" @if(old('q4_1')==1) checked @endif>　会場が近い
                    </label>
                    <label>
                      <input type="checkbox" name="q4_2" class="radio_btn_operation flat-red" value="1" @if(old('q4_2')==1) checked @endif>　魅力的な企業があった
                    </label>
                    <label>
                      <input type="checkbox" name="q4_3" class="radio_btn_operation flat-red" value="1" @if(old('q4_3')==1) checked @endif>　日程が参加しやすかった
                    </label>
                    <label>
                      <input type="checkbox" name="q4_4" class="radio_btn_operation flat-red" value="1" @if(old('q4_4')==1) checked @endif>　地元（兵庫県）で働きたい
                    </label>
                    <label>
                      <input type="checkbox" name="q4_5" class="radio_btn_operation flat-red" value="1" @if(old('q4_5')==1) checked @endif>　中小企業で働きたい
                    </label>
                    <label>
                      <input type="checkbox" name="q4_6" class="radio_btn_operation flat-red" value="1" @if(old('q4_6')==1) checked @endif>　経営者と話してみたい
                    </label>
                    <label>
                      <input type="checkbox" name="q4_7" class="radio_btn_operation flat-red" value="1" @if(old('q4_7')==1) checked @endif>　来場特典（Amazonギフトカード）が魅力的だった
                    </label>
                    <label>
                      <input type="checkbox" name="q4_8" class="radio_btn_operation flat-red" value="1" @if(old('q4_8')==1) checked @endif>　その他
                    </label>
                  </div>
                  <div class="row" style="margin-top: 8px;">
                    <label class="col-xs-12 col-md-10 control-label" style="text-align: left;">その他の理由</label>
                    <div class="col-xs-12 col-md-8 @if($errors->has('q4_note')) has-error @endif">
                      <input type="text" name="q4_note" class="form-control" placeholder="その他の理由" value="{{ old('q4_note', '') }}">
                      <span class="help-block error">{{$errors->first('q4_note')}}</span>
                    </div>
                  </div>
                  <span class="help-block error">{{$errors->first('q4_1')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-offset-1 col-md-10 control-label" style="text-align: left;">Q5.企業選びのポイントについて教えてください　(＊チェックボックス複数選択可) <span class="pull-right-container"><small class="label bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="radio col-xs-12 col-md-offset-1 col-md-10 @if($errors->has('q5_1')) has-error @endif">
                  <div class="row">
                    <label>
                      <input type="checkbox" name="q5_1" class="radio_btn_operation flat-red" value="1" @if(old('q5_1')==1) checked @endif>　興味ある企業と出会えた
                    </label>
                    <label>
                      <input type="checkbox" name="q5_2" class="radio_btn_operation flat-red" value="1" @if(old('q5_2')==1) checked @endif>　今まで考えていなかった企業と出会えた
                    </label>
                    <label>
                      <input type="checkbox" name="q5_3" class="radio_btn_operation flat-red" value="1" @if(old('q5_3')==1) checked @endif>　中小企業への理解が深まった
                    </label>
                    <label>
                      <input type="checkbox" name="q5_4" class="radio_btn_operation flat-red" value="1" @if(old('q5_4')==1) checked @endif>　社長の話が聞けてよかった
                    </label>
                    <label>
                      <input type="checkbox" name="q5_5" class="radio_btn_operation flat-red" value="1" @if(old('q5_5')==1) checked @endif>　社員さんの話が聞けて良かった
                    </label>
                    <label>
                      <input type="checkbox" name="q5_6" class="radio_btn_operation flat-red" value="1" @if(old('q5_6')==1) checked @endif>　スタッフ(案内,誘導)の対応が助かった
                    </label>
                    <label>
                      <input type="checkbox" name="q5_7" class="radio_btn_operation flat-red" value="1" @if(old('q5_7')==1) checked @endif>　その他
                    </label>
                  </div>
                  <div class="row" style="margin-top: 8px;">
                    <label class="col-xs-12 col-md-10 control-label" style="text-align: left;">その他の理由</label>
                    <div class="col-xs-12 col-md-8 @if($errors->has('q5_note')) has-error @endif">
                      <input type="text" name="q5_note" class="form-control" placeholder="その他の理由" value="{{ old('q5_note', '') }}">
                      <span class="help-block error">{{$errors->first('q5_note')}}</span>
                    </div>
                  </div>
                  <span class="help-block error">{{$errors->first('q5_1')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-offset-1 col-md-10 control-label" style="text-align: left;">Q6.Amazonギフトカード (Eメールタイプ)を送信するメールアドレス ＊記入がなければエントリー時のメールアドレスにお送りします。 <span class="pull-right-container"><small class="label bg-green" style="margin-top:4px;">任意</small></span></label>
                <div class="col-xs-12 col-md-offset-1 col-md-8 @if($errors->has('amazon_gift')) has-error @endif">
                  <div class="row" style="margin-top: 8px;">
                    <div class="col-md-12">
                      <input type="text" name="amazon_gift" class="form-control" placeholder="xxx@gmail.com" value="{{ old('amazon_gift', '') }}">
                    </div>
                  </div>
                  <span class="help-block error">{{$errors->first('amazon_gift')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-offset-1 col-md-10 control-label" style="text-align: left;">Q7.その他ご意見、ご要望等があればご入力ください　<span class="pull-right-container"><small class="label bg-green" style="margin-top:4px;">任意</small></span></label>
                <div class="col-xs-12 col-md-offset-1 col-md-8 @if($errors->has('note')) has-error @endif">
                  <textarea class="form-control" rows="5" name="note" placeholder="備考等を入力してください。">{{ old('note', '') }}</textarea>
                  <span class="help-block error">{{$errors->first('note')}}</span>
                </div>
              </div>
            </div>
          </div>
          <div class="box-footer">
            <div class="pull-right">
              <input type="submit" class="btn btn-block btn-success btn-sm" value="　回　　答　">
            </div>
            {{--
            <div class="pull-right" style="margin-right: 8px;">
              <input type="button" class="btn btn-block btn-default btn-sm" onclick="location.href='{{ route('exhibitors') }}'" value="　戻　　る　">
            </div>
            --}}
          </div>
        </form>
      </div>
    </div>
  </section>
  <!-- /.content -->

</div>
<!-- /.content-wrapper -->

</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('bower_components/jquery/dist/jquery.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap  -->
<script src="{{ asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>

<!-- bootstrap datepicker -->
<script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ja.min.js') }}"></script>
<!-- iCheck 1.0.1 -->
<script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('js/ajaxzip3.js') }}"></script>
<script src="{{ asset('js/pages/questionnaire.js') }}"></script>
</body>
</html>

@extends('layouts.app')
@include('layouts.header')
@include('layouts.leftmenu')
@include('layouts.footer')

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-files-o"></i> WEB予約管理
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box">
      <form method="post" id="search_form" action="{{ route('entry.find') }}" class="form-horizontal">
        @csrf
        <div class="box-body">
          <div class="col-xs-12 col-md-12">
            <div class="form-group">
              <label class="col-md-1 control-label">イベント</label>
              <div class="col-md-4">
                <select name="search_event" id="search_event" class="form-control" onchange="submit(this.form)">
                  <option value="">イベント選択なし</option>
                  @foreach ($findItems->event as $event)
                  <option value="{{ $event->id }}" @if(!empty($conditions) && $conditions['search_event'] == $event->id) selected @endif>{{ $event->name }}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-md-2">
                <a href="{{ route('event.detail', array('id'=>$target_event->id)) }}" class="btn btn-block btn-warning btn-sm @if(empty($target_event->id))disabled @endif"> イベント情報</a>
              </div>
              <div class="col-md-2">
                <a href="{{ route('csv_download.entry', array('id'=>$target_event->id)) }}" class="btn btn-block btn-success btn-sm @if(empty($target_event->id))disabled @endif" target="_blank"><i class="fa fa-file-excel-o" style="margin-right:6px;"></i> CSVダウンロード</a>
              </div>
            </div>
          </div>
        </div>
      </form>
      <!--
      <div id="overlay" class="overlay">
        <i class="fa fa-refresh fa-spin" style="z-index:99"></i>
      </div>
    -->
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">@if(empty($entries)) エントリーはございません。 @else {{ $target_event->name }}：エントリー情報（{{ $entries->total() }} 件）@endif</h3>
            <div class="pull-right">
              <a href="{{ route('entry.regist', array('event_id'=>$target_event->id)) }}" class="btn btn-block btn-warning btn-sm @if(empty($target_event->id))disabled @endif"><i class="fa fa-play" style="margin-right:6px;"></i> 新規登録</a>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            @if(!empty($entries))
            <div class="table-responsive">
              <table class="table table-bordered table-striped">
                <tr>
                  @foreach ($data->entries_items as $item)
                  <th>{{ $item->title }}</th>
                  @endforeach
                  <th width="80px">QR</th>
                  <th width="80px">登録内容</th>
                </tr>
                @foreach ($entries as $entry)
                <tr>
                  @foreach ($entry->values as $value)
                  <td style="vertical-align:middle;">{{ $value->values }}</td>
                  @endforeach
                  <td style="text-align: center;"><a href="{{ route('qrcode.download', array('uuid' => $entry->user->uuid)) }}" target="_blank">{!! QrCode::size(48)->generate('id='.$entry->user->uuid) !!}</a></td>
                  <td style="vertical-align:middle;">
                    <a href="{{ route('entry.detail', array('id'=>$entry->id)) }}" class="btn btn-block btn-warning btn-sm">確認・編集</a>
                  </td>
                </tr>
                @endforeach
              </table>
              {{ $entries->appends($entries->params)->links() }}
            </div>
            @endif
          </div>
          <!--
          <div id="overlay_list" class="overlay">
            <i class="fa fa-refresh fa-spin" style="z-index:99"></i>
          </div>
        -->
        </div>
      </div>
    </div>
  </section>
  <!-- /.content -->
@endsection

@extends('layouts.app')
@include('layouts.header')
@include('layouts.leftmenu')
@include('layouts.footer')

@section('pageCss')

@endsection

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-bank"></i> WEB予約 確認・編集
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        @if(!empty($status))
        @if($status=='complete')
        <div class="alert alert-success alert-dismissible">
          <h4><i class="icon fa fa-check"></i> 更新成功！</h4>
          WEB予約情報を更新しました。
        </div>
        @else
        <div class="alert alert-danger alert-dismissible">
          <h4><i class="icon fa fa-ban"></i> 更新失敗！</h4>
          WEB予約情報の更新が失敗しました。
        </div>
        @endif
        @endif
        <div id="delete_message" class="alert alert-danger alert-dismissible" style="display:none;">
          <h4><i class="icon fa fa-ban"></i> 削除失敗！</h4>
          WEB予約情報の削除に失敗しました。
        </div>
        <form method="post" id="form" action="{{ route('entry.update', array('id'=>$entry->id)) }}" class="form-horizontal">
          @csrf
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-md-12">
                <input type="hidden" id="entry_id" value="{{ $entry->id }}">
                <div class="form-group" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-2 control-label">予約コード</label>
                  <label class="col-xs-12 col-md-8 control-value">{{ $entry->code }}</label>
                </div>
                <div class="form-group" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-2 control-label">お名前　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('name')) has-error @endif">
                    <input type="text" name="name" class="form-control" placeholder="お名前" value="{{ old('name', $entry->name) }}">
                    <span class="help-block error">{{$errors->first('name')}}</span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">フリガナ　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('kname')) has-error @endif">
                    <input type="text" name="kname" class="form-control" placeholder="フリガナ" value="{{ old('kname', $entry->kname) }}">
                    <span class="help-block error">{{$errors->first('kname')}}</span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">性別　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                  <div class="radio col-xs-12 col-md-8 @if($errors->has('gender')) has-error @endif">
                    <label>
                      <input type="radio" name="gender" value="1" @if($entry->gender==1) checked @endif> 男性　
                    </label>
                    <label>
                      <input type="radio" name="gender" value="2" @if($entry->gender==2) checked @endif> 女性　
                    </label>
                    <label>
                      <input type="radio" name="gender" value="0" @if($entry->gender==0) checked @endif> 未回答
                    </label>
                    <span class="help-block error">{{$errors->first('gender')}}</span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">生年月日　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('birth')) has-error @endif">
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right" id="birth" name="birth" value="{{ old('birth', $entry->birth) }}">
                    </div>
                    <span class="help-block error">{{$errors->first('birth')}}</span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">郵便番号　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('zip')) has-error @endif">
                    <input type="text" name="zip" class="form-control" placeholder="123-4567" value="{{ old('zip', $entry->zip) }}" onKeyUp="AjaxZip3.zip2addr(this,'','address1','address2');">
                    <span class="help-block error">{{$errors->first('zip')}}</span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">都道府県　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('address1')) has-error @endif">
                    <input type="text" name="address1" class="form-control" placeholder="〇〇県" value="{{ old('address1', $entry->address1) }}">
                    <span class="help-block error">{{$errors->first('address1')}}</span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">ご住所　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('address2')) has-error @endif">
                    <input type="text" name="address2" class="form-control" placeholder="〇〇市〇〇１−２−３" value="{{ old('address2', $entry->address2) }}">
                    <span class="help-block error">{{$errors->first('address2')}}</span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">マンション名など　<span class="pull-right-container"><small class="label pull-right bg-green" style="margin-top:4px;">任意</small></span></label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('address3')) has-error @endif">
                    <input type="text" name="address3" class="form-control" placeholder="〇〇マンソン 〇〇号室" value="{{ old('address3', $entry->address3) }}">
                    <span class="help-block error">{{$errors->first('address3')}}</span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">メールアドレス　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('email')) has-error @endif">
                    <input type="text" name="email" class="form-control" placeholder="メールアドレス" value="{{ old('email', $entry->email) }}">
                    <span class="help-block error">{{$errors->first('email')}}</span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">電話番号　<span class="pull-right-container"><small class="label pull-right bg-green" style="margin-top:4px;">任意</small></span></label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('phone')) has-error @endif">
                    <input type="text" name="phone" class="form-control" placeholder="電話番号" value="{{ old('phone', $entry->phone) }}">
                    <span class="help-block error">{{$errors->first('phone')}}</span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">在籍区分　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('division')) has-error @endif">
                    <select name="division" class="form-control">
                      <option value="">---</option>
                      <option value="大学" @if($entry->division == '大学') selected @endif)>大学</option>
                      <option value="大学院" @if($entry->division == '大学院') selected @endif>大学院</option>
                      <option value="短大・専門学校" @if($entry->division == '短大・専門学校') selected @endif>短大・専門学校</option>
                      <option value="既卒" @if($entry->division == '既卒') selected @endif>既卒</option>
                      <option value="その他" @if($entry->division == 'その他') selected @endif>その他</option>
                    </select>
                    <span class="help-block error">{{$errors->first('division')}}</span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">学校名　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('school')) has-error @endif">
                    <input type="text" name="school" class="form-control" placeholder="学校名" value="{{ old('school', $entry->school) }}">
                    <span class="help-block error">{{$errors->first('school')}}</span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">学部・コースなど　<span class="pull-right-container"><small class="label pull-right bg-green" style="margin-top:4px;">任意</small></span></label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('gakubu')) has-error @endif">
                    <input type="text" name="gakubu" class="form-control" placeholder="学部・コースなど" value="{{ old('gakubu', $entry->gakubu) }}">
                    <span class="help-block error">{{$errors->first('gakubu')}}</span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">学年　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('school_year')) has-error @endif">
                    <select name="school_year" class="form-control">
                      <option value="">---</option>
                      <option value="1年生" @if($entry->school_year == '1年生') selected @endif>1年生</option>
                      <option value="2年生" @if($entry->school_year == '2年生') selected @endif>2年生</option>
                      <option value="3年生" @if($entry->school_year == '3年生') selected @endif>3年生</option>
                      <option value="4年生" @if($entry->school_year == '4年生') selected @endif>4年生</option>
                      <option value="5年生" @if($entry->school_year == '5年生') selected @endif>5年生</option>
                      <option value="6年生" @if($entry->school_year == '6年生') selected @endif>6年生</option>
                      <option value="その他" @if($entry->school_year == 'その他') selected @endif>その他</option>
                    </select>
                    <span class="help-block error">{{$errors->first('school_year')}}</span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">在籍区分　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('graduate_year')) has-error @endif">
                    <select name="graduate_year" class="form-control">
                      <option value="">---</option>
                      <option value="2021年卒業予定" @if($entry->graduate_year == '2021年卒業予定') selected @endif>2021年卒業予定</option>
                      <option value="2022年卒業見込" @if($entry->graduate_year == '2022年卒業見込') selected @endif>2022年卒業見込</option>
                      <option value="2023年以降卒業見込" @if($entry->graduate_year == '2023年以降卒業見込') selected @endif>2023年以降卒業見込</option>
                      <option value="既卒" @if($entry->graduate_year == '既卒') selected @endif>既卒</option>
                      <option value="その他" @if($entry->graduate_year == 'その他') selected @endif>その他</option>
                    </select>
                    <span class="help-block error">{{$errors->first('graduate_year')}}</span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">説明欄　<span class="pull-right-container"><small class="label pull-right bg-green" style="margin-top:4px;">任意</small></span></label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('note')) has-error @endif">
                    <textarea class="form-control" rows="5" name="note" placeholder="備考等を入力してください。">{{ old('note', $entry->note) }}</textarea>
                    <span class="help-block error">{{$errors->first('note')}}</span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">QRコード</label>
                  <div class="col-xs-12 col-md-4">
                    <div class="row">
                      {!! QrCode::size(240)->generate('id='.$entry->user->uuid) !!}
                    </div>
                    <div class="row">
                      <a href="{{ route('qrcode.download', array('uuid' => $entry->user->uuid)) }}" class="btn btn-block btn-default btn-sm" target="_blank">ダウンロード</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="box-footer">
              <div class="pull-right">
                <input type="submit" class="btn btn-block btn-success btn-sm" value="　更　　新　">
              </div>
              <div class="pull-right" style="margin-right: 8px;">
                <input type="button" id="delete_entry" class="btn btn-block btn-danger btn-sm" value="　削　　除　">
              </div>
              <div class="pull-right" style="margin-right: 8px;">
                <input type="button" class="btn btn-block btn-default btn-sm" onclick="location.href='{{ route('entry', array('event_id'=>$entry->event->id)) }}'" value="　戻　　る　">
              </div>
            </div>
            <div id="overlay" class="overlay">
              <i class="fa fa-refresh fa-spin" style="z-index:99"></i>
            </div>
          </div>
        </form>
      </div>
    </div>
  </section>
  <!-- /.content -->
@endsection

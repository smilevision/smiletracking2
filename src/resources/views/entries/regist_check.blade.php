@extends('layouts.app')
@include('layouts.header')
@include('layouts.leftmenu')
@include('layouts.footer')

@section('pageCss')

@endsection

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-bank"></i> WEB予約 新規登録 - 入力内容確認
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <form method="post" action="{{ route('entry.regist.complete', array('event_id'=>$data->event_id)) }}" class="form-horizontal">
          @csrf
          <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
            <div class="col-md-12">
              <div class="form-group" style="margin-top:16px;">
                <label class="col-xs-12 col-md-2 control-label">お名前　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <label class="col-xs-12 col-md-8 control-value">{{ $data->name }}</label>
                <input type="hidden" name="name" value="{{ $data->name }}">
              </div>
              <div class="form-group" style="margin-top:16px;">
                <label class="col-xs-12 col-md-2 control-label">フリガナ　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <label class="col-xs-12 col-md-8 control-value">{{ $data->kname }}</label>
                <input type="hidden" name="kname" value="{{ $data->kname }}">
              </div>
              <div class="form-group" style="margin-top:16px;">
                <label class="col-xs-12 col-md-2 control-label">性別　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <label class="col-xs-12 col-md-8 control-value">@if($data->gender==1) 男性 @elseif($data->gender==2) 女性 @else 未回答 @endif</label>
                <input type="hidden" name="gender" value="{{ $data->gender }}">
              </div>
              <div class="form-group" style="margin-top:16px;">
                <label class="col-xs-12 col-md-2 control-label">生年月日　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <label class="col-xs-12 col-md-8 control-value">{{ $data->birth }}</label>
                <input type="hidden" name="birth" value="{{ $data->birth }}">
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">郵便番号　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <label class="col-xs-12 col-md-8 control-value">{{ $data->zip }}</label>
                <input type="hidden" name="zip" value="{{ $data->zip }}">
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">都道府県　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <label class="col-xs-12 col-md-8 control-value">{{ $data->address1 }}</label>
                <input type="hidden" name="address1" value="{{ $data->address1 }}">
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">ご住所　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <label class="col-xs-12 col-md-8 control-value">{{ $data->address2 }}</label>
                <input type="hidden" name="address2" value="{{ $data->address2 }}">
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">マンション名など　<span class="pull-right-container"><small class="label pull-right bg-green" style="margin-top:4px;">任意</small></span></label>
                <label class="col-xs-12 col-md-8 control-value">{{ $data->address3 }}</label>
                <input type="hidden" name="address3" value="{{ $data->address3 }}">
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">メールアドレス　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <label class="col-xs-12 col-md-8 control-value">{{ $data->email }}</label>
                <input type="hidden" name="email" value="{{ $data->email }}">
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">電話番号　<span class="pull-right-container"><small class="label pull-right bg-green" style="margin-top:4px;">任意</small></span></label>
                <label class="col-xs-12 col-md-8 control-value">{{ $data->phone }}</label>
                <input type="hidden" name="phone" value="{{ $data->phone }}">
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">在籍区分　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <label class="col-xs-12 col-md-8 control-value">{{ $data->division }}</label>
                <input type="hidden" name="division" value="{{ $data->division }}">
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">学校名　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <label class="col-xs-12 col-md-8 control-value">{{ $data->school }}</label>
                <input type="hidden" name="school" value="{{ $data->school }}">
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">学部・コースなど　<span class="pull-right-container"><small class="label pull-right bg-green" style="margin-top:4px;">任意</small></span></label>
                <label class="col-xs-12 col-md-8 control-value">{{ $data->gakubu }}</label>
                <input type="hidden" name="gakubu" value="{{ $data->gakubu }}">
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">学年　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <label class="col-xs-12 col-md-8 control-value">{{ $data->school_year }}</label>
                <input type="hidden" name="school_year" value="{{ $data->school_year }}">
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">在籍区分　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <label class="col-xs-12 col-md-8 control-value">{{ $data->graduate_year }}</label>
                <input type="hidden" name="graduate_year" value="{{ $data->graduate_year }}">
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">説明欄　<span class="pull-right-container"><small class="label pull-right bg-green" style="margin-top:4px;">任意</small></span></label>
                <label class="col-xs-12 col-md-8 control-value">{!! nl2br($data->note) !!}</label>
                <input type="hidden" name="note" value="{{ $data->note }}">
              </div>
            </div>
          </div>
          <div class="box-footer">
            <div class="pull-right">
              <input type="submit" class="btn btn-block btn-success btn-sm" value="　登　　録　">
            </div>
            <div class="pull-right" style="margin-right: 8px;">
              <input type="button" class="btn btn-block btn-default btn-sm" onclick="history.back()" value="　戻　　る　">
            </div>
          </div>
        </form>
      </div>
    </div>
  </section>
  <!-- /.content -->
@endsection

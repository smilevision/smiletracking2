@extends('layouts.app')
@include('layouts.header')
@include('layouts.leftmenu')
@include('layouts.footer')

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-edit"></i> アンケート管理
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box">
      <form method="post" id="search_form" action="{{ route('questionnaire_management.find') }}" class="form-horizontal">
        @csrf
        <div class="box-body">
          <div class="col-xs-12 col-md-12">
            <div class="form-group">
              <label class="col-md-1 control-label">イベント</label>
              <div class="col-md-4">
                <select name="search_event" id="search_event" class="form-control" onchange="submit(this.form)">
                  <option value="">イベント選択なし</option>
                  @foreach ($findItems->event as $event)
                  <option value="{{ $event->id }}" @if(!empty($conditions) && $conditions['search_event'] == $event->id) selected @endif>{{ $event->name }}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-md-2">
                <a href="{{ route('event.detail', array('id'=>$target_event->id)) }}" class="btn btn-block btn-warning btn-sm @if(empty($target_event->id))disabled @endif"> イベント情報</a>
              </div>
              <div class="col-md-2">
                <a href="{{ route('csv_download.questionnaire', array('event_id'=>$target_event->id)) }}" class="btn btn-block btn-success btn-sm @if(empty($target_event->id))disabled @endif" target="_blank"><i class="fa fa-file-excel-o" style="margin-right:6px;"></i> CSVダウンロード</a>
              </div>
            </div>
          </div>
        </div>
      </form>
      <div id="overlay" class="overlay">
        <i class="fa fa-refresh fa-spin" style="z-index:99"></i>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">@if(empty($questionnaires)) アンケート情報はございません。 @else {{ $target_event->name }}：アンケート情報（{{ $questionnaires->total() }} 件）@endif</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            @if(!empty($questionnaires))
            <input type="hidden" id="event_id" value="{{ $target_event->id }}">
            <div class="table-responsive">
              <table class="table table-bordered table-striped">
                <tr>
                  <th>予約コード</th>
                  <th>名前</th>
                  <th>フリガナ</th>
                  <th>学校名</th>
                  <th>回答日時</th>
                </tr>
                @foreach ($questionnaires as $questionnaire)
                <tr>
                  {{-- <td style="vertical-align:middle;"><a href="{{ route('questionnaire_management.detail', array('id'=>$questionnaire->id)) }}">{{ $questionnaire->entry->name }}</a></td> --}}
                  <td style="vertical-align:middle;">{{ $questionnaire->entry->code }}</td>
                  <td style="vertical-align:middle;">{{ $questionnaire->entry->name }}</td>
                  <td style="vertical-align:middle;">{{ $questionnaire->entry->kname }}</td>
                  <td style="vertical-align:middle;">{{ $questionnaire->entry->school }}</td>
                  <td style="vertical-align:middle;">{{ $questionnaire->created_at->format('Y-m-d H:i:s') }}</td>
                </tr>
                @endforeach
              </table>
              {{ $questionnaires->appends($questionnaires->params)->links() }}
            </div>
            @endif
          </div>
          <div id="overlay_list" class="overlay">
            <i class="fa fa-refresh fa-spin" style="z-index:99"></i>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- /.content -->
@endsection

@extends('layouts.app')
@include('layouts.header')
@include('layouts.leftmenu')
@include('layouts.footer')

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-th"></i> 入退場管理
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box">
      <form method="post" id="search_form" action="{{ route('reception.find') }}" class="form-horizontal">
        @csrf
        <div class="box-body">
          <div class="col-xs-12 col-md-12">
            <div class="form-group row">
              <label class="col-md-1 control-label">イベント</label>
              <div class="col-md-4">
                <select name="search_event" id="search_event" class="form-control" onchange="submit(this.form)">
                  <option value="">イベント選択なし</option>
                  @foreach ($findItems->event as $event)
                  <option value="{{ $event->id }}" @if(!empty($conditions) && $conditions['search_event'] == $event->id) selected @endif>{{ $event->name }}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-md-2">
                <a href="{{ route('event.detail', array('id'=>$target_event->id)) }}" class="btn btn-block btn-warning btn-sm @if(empty($target_event->id))disabled @endif"> イベント情報</a>
              </div>
              <div class="col-md-2">
                <a href="{{ route('csv_download.reception', array('id'=>$target_event->id)) }}" class="btn btn-block btn-success btn-sm @if(empty($target_event->id))disabled @endif" target="_blank"><i class="fa fa-file-excel-o" style="margin-right:6px;"></i> CSVダウンロード</a>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-1 control-label">お名前</label>
              <div class="col-md-4">
                <input type="text" name="find_name" class="form-control" placeholder="お名前" value="{{ !empty($conditions['find_name']) ? $conditions['find_name'] : '' }}" autocomplete="off">
              </div>
              <label class="col-md-1 control-label">フリガナ</label>
              <div class="col-md-4">
                <input type="text" name="find_kname" class="form-control" placeholder="フリガナ" value="{{ !empty($conditions['find_kname']) ? $conditions['find_kname'] : '' }}" autocomplete="off">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-1 control-label">学校名</label>
              <div class="col-md-4">
                <input type="text" name="find_school" class="form-control" placeholder="学校名" value="{{ !empty($conditions['find_school']) ? $conditions['find_school'] : '' }}" autocomplete="off">
              </div>
            </div>
          </div>
        </div>
        <div class="box-footer clearfix no-border">
          <button type="submit" class="btn block btn-sm btn-primary pull-right" style="margin-left: 8px;">検　索</button>
          <button type="button" class="btn block btn-sm btn-default pull-right" onclick="location.href='{{ route('reception') }}'" style="margin-left: 8px;">クリア</button>
        </div>
      </form>
      <div id="overlay" class="overlay">
        <i class="fa fa-refresh fa-spin" style="z-index:99"></i>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">@if(empty($entries)) 入退場情報はございません。 @else {{ $target_event->name }}：入退場情報（{{ $entries->total() }} 件）@endif</h3>
            <div class="pull-right">
              <a href="{{ route('reception.regist', array('event_id'=>$target_event->id)) }}" class="btn btn-block btn-warning btn-sm @if(empty($target_event->id))disabled @endif"><i class="fa fa-play" style="margin-right:6px;"></i> 新規登録</a>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            @if(!empty($entries))
            <input type="hidden" id="event_id" value="{{ $target_event->id }}">
            <div class="table-responsive">
              <table class="table table-bordered table-striped">
                <tr>
                  <th>入退場</th>
                  <th>名前</th>
                  <th>フリガナ</th>
                  <th>学校名</th>
                  <th>入場</th>
                  <th>退場</th>
                  <th>操作</th>
                </tr>
                @foreach ($entries as $entry)
                <tr>
                  <input type="hidden" name="entry_id" value="{{ $entry->id }}">
                  <td style="vertical-align:middle;">@if(!empty($entry->reception->enter_at) && empty($entry->reception->exit_at)) <span class="label label-info">入場</span> @elseif(!empty($entry->reception->enter_at) && !empty($entry->reception->exit_at)) <span class="label label-danger">退場</span> @endif</td>
                  <td style="vertical-align:middle;">{{ $entry->name }}</td>
                  <td style="vertical-align:middle;">{{ $entry->kname }}</td>
                  <td style="vertical-align:middle;">{{ $entry->school }}</td>
                  <td style="vertical-align:middle;">
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" id="enter_at" name="enter_at" class="enter_at form-control pull-right" placeholder="{{ Date('Y-m-d H:00') }}" value="{{ old('enter_at', empty($entry->reception->enter_at)?'':$entry->reception->enter_at->format('Y-m-d H:i')) }}">
                    </div>
                  </td>
                  <td style="vertical-align:middle;">
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" id="exit_at" name="exit_at" class="exit_at form-control pull-right" placeholder="{{ Date('Y-m-d H:00') }}" value="{{ old('exit_at', empty($entry->reception->exit_at)?'':$entry->reception->exit_at->format('Y-m-d H:i')) }}">
                    </div>
                  </td>
                  <td style="vertical-align:middle;">
                    <input type="button" class="btn btn-block btn-warning btn-sm btn_save" value="更新">
                  </td>
                </tr>
                @endforeach
              </table>
              {{ $entries->appends($entries->params)->links() }}
            </div>
            @endif
          </div>
          <div id="overlay_list" class="overlay">
            <i class="fa fa-refresh fa-spin" style="z-index:99"></i>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- /.content -->
@endsection

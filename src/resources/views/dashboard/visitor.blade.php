@extends('layouts.app')
@include('layouts.header')
@include('layouts.leftmenu')
@include('layouts.footer')

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-dashboard"></i> ダッシュボード
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <form method="post" id="search_form" action="{{ route('dashboard.find') }}" class="form-horizontal">
            @csrf
            <div class="box-body">
              <div class="col-xs-12 col-md-12">
                <div class="form-group">
                  <label class="col-md-3 col-xs-4 control-label">イベント</label>
                  <div class="col-md-4 col-xs-8">
                    <select name="search_event" id="search_event" class="form-control" onchange="submit(this.form)">
                      @foreach ($findItems->event as $event)
                      <option value="{{ $event->id }}" @if(!empty($conditions) && $conditions['search_event'] == $event->id) selected @endif>{{ $event->name }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="row col-md-2 col-xs-12">
                    <a href="{{ route('questionnaire.regist', array('event_id'=>$conditions['search_event'])) }}" class="btn btn-sm btn-flat btn-block bg-orange" target="_blank"> アンケート</a>
                  </div>
                </div>
              </div>
            </div>
          </form>
          <div id="overlay" class="overlay">
            <i class="fa fa-refresh fa-spin" style="z-index:99"></i>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
        <div class="col-md-12">
          <!-- PRODUCT LIST -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">QRコード</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div style="text-align: center;">
                {!! QrCode::size(256)->generate(route('qrlogin', array('uuid'=>Auth::user()->uuid))) !!}
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>

    <div class="row">
      <div class="col-md-12">
        <!-- PRODUCT LIST -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">出展企業</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">

            @foreach ($data->exhibitors as $key => $exhibitors)
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="info-box" style="background-color: #ecf0f5;">
                <span class="info-box-icon bg-aqua"><i class="fa fa-bookmark-o"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">{{ $exhibitors->name }}</span>
                  <span class="info-box-text">@if($exhibitors->is_join==1) <span class="label label-success">参加</span> @else <span class="label label-default">未定</span> @endif</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            @endforeach

            {{--
            <ul class="products-list product-list-in-box">
              @foreach ($data->exhibitors as $exhibitors)
              <li class="item">
                <div class="product-info">
                  <a href="" class="product-title">{{ $exhibitors->name }}</a>
                  <span class="product-description">
                    @if($exhibitors->is_join==1) <span class="label label-success">参加</span> @else <span class="label label-default">未定</span> @endif
                  </span>
                </div>
              </li>
              @endforeach
            </ul>
            --}}
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </div>
  </section>
  <!-- /.content -->
@endsection

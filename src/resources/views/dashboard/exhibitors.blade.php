@extends('layouts.app')
@include('layouts.header')
@include('layouts.leftmenu')
@include('layouts.footer')

@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-dashboard"></i> ダッシュボード（{{ Auth::user()->name }}）
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <form method="post" id="search_form" action="{{ route('dashboard.find') }}" class="form-horizontal">
            @csrf
            <div class="box-body">
              <div class="col-xs-12 col-md-12">
                <div class="form-group">
                  <label class="col-md-1 control-label">イベント</label>
                  <div class="col-md-4">
                    <select name="search_event" id="search_event" class="form-control" onchange="submit(this.form)">
                      @foreach ($findItems->event as $event)
                      <option value="{{ $event->id }}" @if(!empty($conditions) && $conditions['search_event'] == $event->id) selected @endif>{{ $event->name }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="col-md-2">
                    <button type="button" id="btn_qr_reader" class="btn btn-block btn-default btn-sm" onclick="location.href='{{ route('visitor.qr_regist', array('event_id'=>$target_event->id)) }}'">QRコード受付</button>
                  </div>
                </div>
              </div>
            </div>
          </form>
          <div id="overlay" class="overlay">
            <i class="fa fa-refresh fa-spin" style="z-index:99"></i>
          </div>
          <input type="hidden" id="target_event_id" value="{{ $target_event->id }}">
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">

        <!-- TABLE: LATEST ORDERS -->
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">訪問履歴一覧</h3>

            <div class="box-tools pull-right">
              <a href="{{ route('csv_download.visit', array('event_id'=>$target_event->id, 'exhibitors_id'=>Auth::user()->relation->exhibitors->id)) }}" class="btn btn-block btn-success btn-sm @if(empty($target_event->id))disabled @endif"><i class="fa fa-file-excel-o" style="margin-right:6px;"></i> CSVダウンロード</a>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="table-responsive">
              <table class="table no-margin">
                <thead>
                  <tr>
                    <th>予約コード</th>
                    <th>お名前</th>
                    <th>お名前（カナ）</th>
                    <th>学校名</th>
                    <th>訪問日時</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($data->visitors as $visit)
                  <tr>
                    <td>{{ $visit->entry->code }}</td>
                    <td>{{ $visit->entry->name }}</td>
                    <td>{{ $visit->entry->school }}</td>
                    <td>{{ $visit->entry->school }}</td>
                    <td>{{ $visit->created_at }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.table-responsive -->
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- /.content -->
@endsection

@extends('layouts.app')
@include('layouts.header')
@include('layouts.leftmenu')
@include('layouts.footer')

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-dashboard"></i> ダッシュボード（運営者）
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <form method="post" id="search_form" action="{{ route('dashboard.find') }}" class="form-horizontal">
            @csrf
            <div class="box-body">
              <div class="col-xs-12 col-md-12">
                <div class="form-group">
                  <label class="col-md-2 control-label">イベント</label>
                  <div class="col-md-4">
                    <select name="search_event" id="search_event" class="form-control" onchange="submit(this.form)">
                      @foreach ($findItems->event as $event)
                      <option value="{{ $event->id }}" @if(!empty($conditions) && $conditions['search_event'] == $event->id) selected @endif>{{ $event->name }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </form>
          <div id="overlay" class="overlay">
            <i class="fa fa-refresh fa-spin" style="z-index:99"></i>
          </div>
          <input type="hidden" id="target_event_id" value="{{ $target_event->id }}">
        </div>
      </div>
    </div>

    {{--
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">最近の開催イベント</h3>　<small>開催日が新しい順に表示しています（最大１０件）</small>
            <div class="pull-right">
              <button class="btn btn-block btn-warning btn-sm"><i class="fa fa-play" style="margin-right:6px;"></i>イベント一覧</button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table class="table table-bordered table-striped">
              <tr>
                <th>開催名</th>
                <th>会場</th>
                <th>開催日時</th>
                <th>状態</th>
                <th>出展数</th>
                <th>予約数</th>
                <th>参加数</th>
                <th width="80px">開催概要</th>
                <th width="80px">出展一覧</th>
              </tr>
              <tr>
                <td style="vertical-align:middle;">サンプルイベント</td>
                <td style="vertical-align:middle;">サンプル会場</td>
                <td style="vertical-align:middle;">
                  <p>開：2021/03/04 13:00</p>
                  <p>終：2021/03/04 19:00</p>
                </td>
                <td style="vertical-align:middle;">終了</td>
                <td style="vertical-align:middle;">１社</td>
                <td style="vertical-align:middle;">１人</td>
                <td style="vertical-align:middle;">０人</td>
                <td style="vertical-align:middle;"><button class="btn btn-block btn-warning btn-sm">開催概要</button></td>
                <td style="vertical-align:middle;"><button class="btn btn-block btn-warning btn-sm">出展一覧</button></td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
    --}}

    <div class="row">
      <div class="col-md-8">

        <!-- TABLE: LATEST ORDERS -->
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">WEB予約一覧</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
            {{--
            <div class="pull-right">
              <a href="{{ route('csv_import.entry') }}" class="btn btn-block btn-warning btn-sm @if(empty($target_event->id))disabled @endif" target="_blank"><i class="fa fa-play" style="margin-right:6px;"></i> CSV登録</a>
            </div>
            --}}
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="table-responsive">
              <table class="table no-margin">
                <thead>
                  <tr>
                    <th>予約コード</th>
                    <th>お名前</th>
                    <th>大学</th>
                    <th>住所</th>
                    <th>性別</th>
                    <th>卒業年度</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($data->entry as $entry)
                  <tr>
                    <td><a href="{{ route('entry.detail', array('id'=>$entry->id)) }}">{{ $entry->code }}</a></td>
                    <td>{{ $entry->name }}</td>
                    <td>{{ $entry->school }}</td>
                    <td>{{ $entry->address1 }} {{ $entry->address2 }}</td>
                    <td>@if($entry->gender==1) 男性 @elseif($entry->gender==2) 女性 @else その他 @endif</td>
                    <td>{{ $entry->graduate_year }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.table-responsive -->
          </div>
          <!-- /.box-body -->
          <div class="box-footer clearfix">
            <a href="@if(empty($conditions)) {{ route('entry') }} @else {{ route('entry', array('event_id'=>$conditions['search_event'])) }} @endif" class="btn btn-sm btn-info btn-flat pull-left">全て見る</a>
          </div>
          <!-- /.box-footer -->
        </div>

        <!-- TABLE: LATEST ORDERS -->
        <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title">訪問者数一覧</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="table-responsive">
              <table class="table no-margin">
                <thead>
                  <tr>
                    <th>企業名</th>
                    <th>訪問数</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($data->visit as $visit)
                  <tr>
                    <td>{{ $visit->name }}</td>
                    <td>{{ $visit->visit_count }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.table-responsive -->
          </div>
          <!-- /.box-body -->
        </div>

        <!-- TABLE: LATEST ORDERS -->
        <div class="box box-danger">
          <div class="box-header with-border">
            <h3 class="box-title">入退場一覧</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="table-responsive">
              <table class="table no-margin">
                <thead>
                  <tr>
                    <th>予約コード</th>
                    <th>お名前</th>
                    <th>大学</th>
                    <th>住所</th>
                    <th>性別</th>
                    <th>状態</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($data->reception as $reception)
                  <tr>
                    <td><a href="{{ route('reception') }}">{{ $reception->entry->code }}</a></td>
                    <td>{{ $reception->entry->name }}</td>
                    <td>{{ $reception->entry->school }}</td>
                    <td>{{ $reception->entry->address1 }} {{ $reception->entry->address2 }}</td>
                    <td>@if($reception->entry->gender==1) 男性 @elseif($reception->entry->gender==2) 女性 @else その他 @endif</td>
                    <td>@if(empty($reception->exit_at)) <span class="label label-info">入場</span> @else <span class="label label-danger">退場</span> @endif</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.table-responsive -->
          </div>
          <!-- /.box-body -->
          <div class="box-footer clearfix">
            <a href="@if(empty($conditions)) {{ route('reception') }} @else {{ route('reception', array('event_id'=>$conditions['search_event'])) }} @endif" class="btn btn-sm btn-info btn-flat pull-left">全て見る</a>
          </div>
          <!-- /.box-footer -->
        </div>
      </div>
      <div class="col-md-4">
        <!-- Info Boxes Style 2 -->
        <div class="info-box bg-yellow">
          <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">WEB予約</span>
            <span class="info-box-number">{{ $data->entry_count }}</span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
        <div class="info-box bg-green">
          <span class="info-box-icon"><i class="ion ion-ios-heart-outline"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">当日来場者数</span>
            <span class="info-box-number">{{ $data->reception_count }}</span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
        <div class="info-box bg-red">
          <span class="info-box-icon"><i class="ion ion-ios-cloud-download-outline"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">平均滞在時間</span>
            <span class="info-box-number">@if(!empty($data->average_stay_time)) {{ round($data->average_stay_time, 2) }} @else 0.00 @endif 時間</span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
        <div class="info-box bg-aqua">
          <span class="info-box-icon"><i class="ion-ios-chatbubble-outline"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">平均ブース来訪者数</span>
            <span class="info-box-number">@if(!empty($data->average_visitor_count)) {{ $data->average_visitor_count }} @else 0 @endif</span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->

        <div class="box box-default">
          <div class="box-header with-border">
            <h3 class="box-title">WEB予約 男女比</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-md-8">
                <div class="chart-responsive">
                  <canvas id="pieChart" height="170" width="329" style="width: 329px; height: 170px;"></canvas>
                </div>
                <!-- ./chart-responsive -->
              </div>
              <!-- /.col -->
              <div class="col-md-4">
                <ul class="chart-legend clearfix">
                  <li><i class="fa fa-circle-o text-aqua"></i> 男性</li>
                  <li><i class="fa fa-circle-o text-red"></i> 女性</li>
                  <li><i class="fa fa-circle-o text-gray"></i> その他</li>
                </ul>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

        <!-- PRODUCT LIST -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">出展企業</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <ul class="products-list product-list-in-box">
              @foreach ($data->exhibitors as $exhibitors)
              <li class="item">
                <div class="product-img">
                  {!! QrCode::size(50)->generate(route('qrlogin', array('uuid'=>$exhibitors->relation->user->uuid))) !!}
                </div>
                <div class="product-info">
                  <a href="{{ route('exhibitors.detail', array('id'=>$exhibitors->id)) }}" class="product-title">{{ $exhibitors->name }}</a>
                  <span class="product-description">
                    @if($exhibitors->is_join==1) <span class="label label-success">参加</span> @else <span class="label label-default">未定</span> @endif
                  </span>
                </div>
              </li>
              @endforeach
            </ul>
          </div>
          <!-- /.box-body -->
          <div class="box-footer text-center">
            <a href="{{ route('exhibitors', array('event_id'=>$event->id)) }}" class="uppercase">全出展企業情報を見る</a>
          </div>
          <!-- /.box-footer -->
        </div>
        <!-- /.box -->
      </div>
    </div>
  </section>
  <!-- /.content -->
@endsection

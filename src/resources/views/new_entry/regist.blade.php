<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{ asset('bower_components/jvectormap/jquery-jvectormap.css') }}">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">

  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('dist/css/skins/_all-skins.min.css') }}">

  <link rel="stylesheet" href="{{ asset('css/pages.css') }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-black-light sidebar-mini">
<div class="wrapper">

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-bank"></i> WEB予約 新規登録
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <form method="post" action="{{ route('new_entry.regist.check', array('event_id'=>$data->event_id)) }}" class="form-horizontal">
          @csrf
          <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
            <div class="col-md-12">
              <div class="form-group" style="margin-top:16px;">
                <label class="col-xs-12 col-md-2 control-label">お名前　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('name')) has-error @endif">
                  <input type="text" name="name" class="form-control" placeholder="お名前" value="{{ old('name', '') }}">
                  <span class="help-block error">{{$errors->first('name')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">フリガナ　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('kname')) has-error @endif">
                  <input type="text" name="kname" class="form-control" placeholder="フリガナ" value="{{ old('kname', '') }}">
                  <span class="help-block error">{{$errors->first('kname')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">性別　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="radio col-xs-12 col-md-8 @if($errors->has('gender')) has-error @endif">
                  <label>
                    <input type="radio" name="gender" value="1" @if(old('kname')=='1') checked @endif> 男性　
                  </label>
                  <label>
                    <input type="radio" name="gender" value="2" @if(old('kname')=='2') checked @endif> 女性　
                  </label>
                  <label>
                    <input type="radio" name="gender" value="0" checked> 未回答
                  </label>
                  <span class="help-block error">{{$errors->first('gender')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">生年月日　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('birth')) has-error @endif">
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="birth" name="birth" value="{{ old('birth', '') }}">
                  </div>
                  <span class="help-block error">{{$errors->first('birth')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">郵便番号　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('zip')) has-error @endif">
                  <input type="text" name="zip" class="form-control" placeholder="123-4567" value="{{ old('zip', '') }}" onKeyUp="AjaxZip3.zip2addr(this,'','address1','address2');">
                  <span class="help-block error">{{$errors->first('zip')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">都道府県　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('address1')) has-error @endif">
                  <input type="text" name="address1" class="form-control" placeholder="〇〇県" value="{{ old('address1', '') }}">
                  <span class="help-block error">{{$errors->first('address1')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">ご住所　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('address2')) has-error @endif">
                  <input type="text" name="address2" class="form-control" placeholder="〇〇市〇〇１−２−３" value="{{ old('address2', '') }}">
                  <span class="help-block error">{{$errors->first('address2')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">マンション名など　<span class="pull-right-container"><small class="label pull-right bg-green" style="margin-top:4px;">任意</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('address3')) has-error @endif">
                  <input type="text" name="address3" class="form-control" placeholder="〇〇マンソン 〇〇号室" value="{{ old('address3', '') }}">
                  <span class="help-block error">{{$errors->first('address3')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">メールアドレス　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('email')) has-error @endif">
                  <input type="text" name="email" class="form-control" placeholder="メールアドレス" value="{{ old('email', '') }}">
                  <span class="help-block error">{{$errors->first('email')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">電話番号　<span class="pull-right-container"><small class="label pull-right bg-green" style="margin-top:4px;">任意</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('phone')) has-error @endif">
                  <input type="text" name="phone" class="form-control" placeholder="電話番号" value="{{ old('phone', '') }}">
                  <span class="help-block error">{{$errors->first('phone')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">在籍区分　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('division')) has-error @endif">
                  <select name="division" class="form-control">
                    <option value="">---</option>
                    <option value="大学" @if(old('division')=='大学') selected @endif>大学</option>
                    <option value="大学院" @if(old('division')=='大学院') selected @endif>大学院</option>
                    <option value="短大・専門学校" @if(old('division')=='短大・専門学校') selected @endif>短大・専門学校</option>
                    <option value="既卒" @if(old('division')=='既卒') selected @endif>既卒</option>
                    <option value="その他" @if(old('division')=='その他') selected @endif>その他</option>
                  </select>
                  <span class="help-block error">{{$errors->first('division')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">学校名　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('school')) has-error @endif">
                  <input type="text" name="school" class="form-control" placeholder="学校名" value="{{ old('school', '') }}">
                  <span class="help-block error">{{$errors->first('school')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">学部・コースなど　<span class="pull-right-container"><small class="label pull-right bg-green" style="margin-top:4px;">任意</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('gakubu')) has-error @endif">
                  <input type="text" name="gakubu" class="form-control" placeholder="学部・コースなど" value="{{ old('gakubu', '') }}">
                  <span class="help-block error">{{$errors->first('gakubu')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">学年　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('school_year')) has-error @endif">
                  <select name="school_year" class="form-control">
                    <option value="">---</option>
                    <option value="1年生" @if(old('school_year')=='1年生') selected @endif>1年生</option>
                    <option value="2年生" @if(old('school_year')=='2年生') selected @endif>2年生</option>
                    <option value="3年生" @if(old('school_year')=='3年生') selected @endif>3年生</option>
                    <option value="4年生" @if(old('school_year')=='4年生') selected @endif>4年生</option>
                    <option value="5年生" @if(old('school_year')=='5年生') selected @endif>5年生</option>
                    <option value="6年生" @if(old('school_year')=='6年生') selected @endif>6年生</option>
                    <option value="その他" @if(old('school_year')=='その他') selected @endif>その他</option>
                  </select>
                  <span class="help-block error">{{$errors->first('school_year')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">卒業見込み年度　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('graduate_year')) has-error @endif">
                  <select name="graduate_year" class="form-control">
                    <option value="">---</option>
                    <option value="2021年卒業予定" @if(old('graduate_year')=='2021年卒業予定') selected @endif>2021年卒業予定</option>
                    <option value="2022年卒業見込" @if(old('graduate_year')=='2022年卒業見込') selected @endif>2022年卒業見込</option>
                    <option value="2023年以降卒業見込" @if(old('graduate_year')=='2023年以降卒業見込') selected @endif>2023年以降卒業見込</option>
                    <option value="既卒" @if(old('graduate_year')=='既卒') selected @endif>既卒</option>
                    <option value="その他" @if(old('graduate_year')=='その他') selected @endif>その他</option>
                  </select>
                  <span class="help-block error">{{$errors->first('graduate_year')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">説明欄　<span class="pull-right-container"><small class="label pull-right bg-green" style="margin-top:4px;">任意</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('note')) has-error @endif">
                  <textarea class="form-control" rows="5" name="note" placeholder="備考等を入力してください。">{{ old('note', '') }}</textarea>
                  <span class="help-block error">{{$errors->first('note')}}</span>
                </div>
              </div>
            </div>
          </div>
          <div class="box-footer">
            <div class="pull-right">
              <input type="submit" class="btn btn-block btn-success btn-sm" value="　内容確認　">
            </div>
            <div class="pull-right" style="margin-right: 8px;">
              <input type="button" class="btn btn-block btn-default btn-sm" onclick="location.href='{{ route('exhibitors') }}'" value="　戻　　る　">
            </div>
          </div>
        </form>
      </div>
    </div>
  </section>
  <!-- /.content -->

</div>
<!-- /.content-wrapper -->

</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('bower_components/jquery/dist/jquery.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap  -->
<script src="{{ asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>

<!-- bootstrap datepicker -->
<script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ja.min.js') }}"></script>
<script src="{{ asset('js/ajaxzip3.js') }}"></script>
<script src="{{ asset('js/pages/entry.js') }}"></script>
</body>
</html>

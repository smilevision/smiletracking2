@section('leftmenu')
<section class="sidebar">
  <ul class="sidebar-menu" data-widget="tree">
    <li @if(Request::is('dashboard*'))class="active"@endif>
      <a href="{{ route('dashboard') }}">
        <i class="fa fa-dashboard"></i> <span>ダッシュボード</span>
      </a>
    </li>
    @if(Auth::user()->is_authority <= config('const.authority.EVENTOR'))
    <li @if(Request::is('event*'))class="active"@endif>
      <a href="{{ route('event') }}">
        <i class="fa fa-calendar"></i> <span>イベント管理</span>
      </a>
    </li>
    @endif
    @if(Auth::user()->is_authority <= config('const.authority.EVENTOR'))
    <li @if(Request::is('exhibitors*'))class="active"@endif>
      <a href="{{ route('exhibitors') }}">
        <i class="fa fa-pie-chart"></i> <span>出展企業情報</span>
      </a>
    </li>
    @endif
    @if(Auth::user()->is_authority <= config('const.authority.EVENTOR'))
    <li @if(Request::is('entry*'))class="active"@endif>
      <a href="{{ route('entry') }}">
        <i class="fa fa-files-o"></i> <span>WEB予約管理</span>
      </a>
    </li>
    @endif
    {{--
    @if(Auth::user()->is_authority <= config('const.authority.EVENTOR'))
    <li @if(Request::is('entries*'))class="active"@endif>
      <a href="{{ route('entries') }}">
        <i class="fa fa-files-o"></i> <span>WEB予約管理</span>
      </a>
    </li>
    @endif
    --}}
    @if(Auth::user()->is_authority <= config('const.authority.EVENTOR'))
    <li @if(Request::is('reception*'))class="active"@endif>
      <a href="{{ route('reception') }}">
        <i class="fa fa-th"></i> <span>入退場管理</span>
      </a>
    </li>
    @endif
    @if(Auth::user()->is_authority < config('const.authority.EXHIBITORS'))
    <li @if(Request::is('visitor*'))class="active"@endif>
      <a href="{{ route('visitor') }}">
        <i class="fa fa-users"></i> <span>訪問者管理</span>
      </a>
    </li>
    @endif
    @if(Auth::user()->is_authority < config('const.authority.EXHIBITORS'))
    <li @if(Request::is('questionnaire_management*'))class="active"@endif>
      <a href="{{ route('questionnaire_management') }}">
        <i class="fa fa-users"></i> <span>アンケート管理</span>
      </a>
    </li>
    @endif
    <li class="visible-xs @if(Request::is('user*'))active @endif">
      <a href="{{ route('user.detail') }}">
        <i class="fa fa-info-circle"></i> <span>ご登録情報</span>
      </a>
    </li>
    <li>
      <a href="{{ route('logout') }}"
          onclick="event.preventDefault();
          document.getElementById('logout-form').submit();">
        <i class="fa fa-sign-out"></i> <span>ログアウト</span>
      </a>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
      @csrf
      </form>
    </li>
  </ul>
</section>
@endsection

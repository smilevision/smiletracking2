<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{ asset('bower_components/jvectormap/jquery-jvectormap.css') }}">
  @if(Request::is('exhibitors*'))
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css') }}">
  <link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}">
  @endif
  @if(Request::is('event*'))
  <!-- datetimepicker-master -->
  <link rel="stylesheet" href="{{ asset('bower_components/datetimepicker-master/jquery.datetimepicker.css') }}">
  @endif
  @if(Request::is('entry*'))
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
  @endif
  @if(Request::is('reception*'))
  <!-- datetimepicker-master -->
  <link rel="stylesheet" href="{{ asset('bower_components/datetimepicker-master/jquery.datetimepicker.css') }}">
  @endif

  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('dist/css/skins/_all-skins.min.css') }}">

  <link rel="stylesheet" href="{{ asset('css/pages.css') }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-black-light sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    {{-- ヘッダー部 --}}
    @yield('header')
  </header>

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    {{-- 左メニュー部 --}}
    @yield('leftmenu')
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @yield('content')
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    @yield('footer')
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('bower_components/jquery/dist/jquery.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap  -->
<script src="{{ asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>

@if(Request::is('dashboard*'))
<!-- ChartJS -->
<script src="{{ asset('bower_components/chart.js/Chart.js') }}"></script>
<!-- datetimepicker-master -->
<script src="{{ asset('js/pages/dashboard.js') }}"></script>
@endif

@if(Request::is('event*'))
<!-- datetimepicker-master -->
<script src="{{ asset('bower_components/datetimepicker-master/jquery.datetimepicker.full.js') }}"></script>
<script src="{{ asset('js/pages/event.js') }}"></script>
@endif

@if(Request::is('exhibitors*'))
<script src="{{ asset('js/toastr.min.js') }}"></script>
<!-- iCheck 1.0.1 -->
<script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('js/ajaxzip3.js') }}"></script>
<script src="{{ asset('js/pages/exhibitors.js') }}"></script>
@endif

@if(Request::is('entry*'))
<!-- bootstrap datepicker -->
<script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ja.min.js') }}"></script>
<script src="{{ asset('js/ajaxzip3.js') }}"></script>
<script src="{{ asset('js/pages/entry.js') }}"></script>
@endif

@if(Request::is('reception*'))
<!-- datetimepicker-master -->
<script src="{{ asset('bower_components/datetimepicker-master/jquery.datetimepicker.full.js') }}"></script>
<script src="{{ asset('js/pages/reception.js') }}"></script>
@endif

@if(Request::is('reception/qr_regist'))
<script src="{{ asset('js/jsQR.js') }}"></script>
<script src="{{ asset('js/pages/qr_reader.js') }}"></script>
@endif

@if(Request::is('visitor*'))
<!-- datetimepicker-master -->
<script src="{{ asset('bower_components/datetimepicker-master/jquery.datetimepicker.full.js') }}"></script>
<script src="{{ asset('js/pages/visitor.js') }}"></script>
@endif

@if(Request::is('visitor/qr_regist'))
<script src="{{ asset('js/jsQR.js') }}"></script>
<script src="{{ asset('js/pages/qr_reader.js') }}"></script>
@endif

@if(Request::is('user*'))
<script src="{{ asset('js/pages/user.js') }}"></script>
@endif

@if(Request::is('questionnaire_management*'))
<script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('js/pages/questionnaire.js') }}"></script>
@endif

</body>
</html>

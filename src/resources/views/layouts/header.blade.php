@section('header')

<!-- Logo -->
<a href="{{ route('dashboard') }}" class="logo">
  <!-- mini logo for sidebar mini 50x50 pixels -->
  <span class="logo-mini"><b>S</b>T</span>
  <!-- logo for regular state and mobile devices -->
  <span class="logo-lg"><b>Smile</b>Tracking</span>
</a>

<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top">
  <!-- Sidebar toggle button-->
  <a href="#" class="sidebar-toggle hidden-lg hidden-md" data-toggle="push-menu" role="button">
    <span class="sr-only">Toggle navigation</span>
  </a>
  <!-- Navbar Right Menu -->
  <div class="navbar-custom-menu hidden-xs">
    <ul class="nav navbar-nav">
      {{--
      <li>
        <a href="#">
          <span class="hidden-xs">ヘルプ</span>
        </a>
      </li>
      --}}
      <li>
        <a href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
          <span class="hidden-xs">ログアウト</span>
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
        </form>
      </li>
      <li>
        <a href="{{ route('user.detail') }}">
          <span class="hidden-xs">
            @switch(Auth::user()->is_authority)
                @case(config('const.authority.KANRISHA'))
                    ご登録情報（管理者）
                    @break
                @case(config('const.authority.EVENTOR'))
                    ご登録情報（運営者）
                    @break
                @default
                ご登録情報
                @break
            @endswitch
          </span>
        </a>
      </li>
    </ul>
  </div>
</nav>
@endsection

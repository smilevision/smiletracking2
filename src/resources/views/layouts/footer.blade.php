@section('footer')
  <div class="pull-right hidden-xs">
    <b>Version</b> {{ config('app.version', '1.0.0') }}
  </div>
  <strong>Copyright &copy; <a href="https://www.smilevision.co.jp" target="_blank">SmileVision</a> CO.LTD.</strong> All rights reserved.
@endsection

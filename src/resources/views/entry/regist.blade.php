@extends('layouts.app')
@include('layouts.header')
@include('layouts.leftmenu')
@include('layouts.footer')

@section('pageCss')

@endsection

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-bank"></i> WEB予約 新規登録
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <form method="post" action="{{ route('entry.regist.check', array('event_id'=>$data->event_id)) }}" class="form-horizontal">
          @csrf
          <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
            <div class="col-md-12">
              <div class="form-group" style="margin-top:16px;">
                <label class="col-xs-12 col-md-2 control-label">お名前　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('name')) has-error @endif">
                  <input type="text" name="name" class="form-control" placeholder="お名前" value="{{ old('name', '') }}">
                  <span class="help-block error">{{$errors->first('name')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">フリガナ　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('kname')) has-error @endif">
                  <input type="text" name="kname" class="form-control" placeholder="フリガナ" value="{{ old('kname', '') }}">
                  <span class="help-block error">{{$errors->first('kname')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">性別　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="radio col-xs-12 col-md-8 @if($errors->has('gender')) has-error @endif">
                  <label>
                    <input type="radio" name="gender" value="1" @if(old('gender')=='1') checked @endif)> 男性
                  </label>
                  <label>
                    <input type="radio" name="gender" value="2" @if(old('gender')=='2') checked @endif)> 女性　
                  </label>
                  <label>
                    <input type="radio" name="gender" value="0" @if(old('gender')=='0') checked @endif)> 未回答
                  </label>
                  <span class="help-block error">{{$errors->first('gender')}}</span>
                </div>
              </div>
              {{-- <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">生年月日　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('birth')) has-error @endif">
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="birth" name="birth" value="{{ old('birth', '') }}">
                  </div>
                  <span class="help-block error">{{$errors->first('birth')}}</span>
                </div>
              </div> --}}
              {{-- <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">郵便番号　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('zip')) has-error @endif">
                  <input type="text" name="zip" class="form-control" placeholder="123-4567" value="{{ old('zip', '') }}" onKeyUp="AjaxZip3.zip2addr(this,'','address1','address2');">
                  <span class="help-block error">{{$errors->first('zip')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">都道府県　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('address1')) has-error @endif">
                  <input type="text" name="address1" class="form-control" placeholder="〇〇県" value="{{ old('address1', '') }}">
                  <span class="help-block error">{{$errors->first('address1')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">ご住所　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('address2')) has-error @endif">
                  <input type="text" name="address2" class="form-control" placeholder="〇〇市〇〇１−２−３" value="{{ old('address2', '') }}">
                  <span class="help-block error">{{$errors->first('address2')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">マンション名など　<span class="pull-right-container"><small class="label pull-right bg-green" style="margin-top:4px;">任意</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('address3')) has-error @endif">
                  <input type="text" name="address3" class="form-control" placeholder="〇〇マンソン 〇〇号室" value="{{ old('address3', '') }}">
                  <span class="help-block error">{{$errors->first('address3')}}</span>
                </div>
              </div> --}}
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">メールアドレス　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('email')) has-error @endif">
                  <input type="text" name="email" class="form-control" placeholder="メールアドレス" value="{{ old('email', '') }}">
                  <span class="help-block error">{{$errors->first('email')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">電話番号　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('phone')) has-error @endif">
                  <input type="text" name="phone" class="form-control" placeholder="電話番号" value="{{ old('phone', '') }}">
                  <span class="help-block error">{{$errors->first('phone')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">在籍区分　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('division')) has-error @endif">
                  <select name="division" class="form-control">
                    <option value="">選択してください</option>
                    <option value="大学" @if(old('division') == '大学') selected @endif>大学</option>
                    <option value="大学院" @if(old('division') == '大学院') selected @endif>大学院</option>
                    <option value="短大・専門学校" @if(old('division') == '短大・専門学校') selected @endif>短大・専門学校</option>
                    <option value="既卒" @if(old('division') == '既卒') selected @endif>既卒</option>
                    <option value="その他" @if(old('division') == 'その他') selected @endif>その他</option>
                  </select>
                  <span class="help-block error">{{$errors->first('division')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">学校名　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('school')) has-error @endif">
                  <input type="text" name="school" class="form-control" placeholder="学校名" value="{{ old('school', '') }}">
                  <span class="help-block error">{{$errors->first('school')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">学部・コースなど　<span class="pull-right-container"><small class="label pull-right bg-green" style="margin-top:4px;">任意</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('gakubu')) has-error @endif">
                  <input type="text" name="gakubu" class="form-control" placeholder="学部・コースなど" value="{{ old('gakubu', '') }}">
                  <span class="help-block error">{{$errors->first('gakubu')}}</span>
                </div>
              </div>
              {{-- <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">学年　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('school_year')) has-error @endif">
                  <select name="school_year" class="form-control">
                    <option value="">---</option>
                    <option value="1年生">1年生</option>
                    <option value="2年生">2年生</option>
                    <option value="3年生">3年生</option>
                    <option value="4年生">4年生</option>
                    <option value="5年生">5年生</option>
                    <option value="6年生">6年生</option>
                    <option value="その他">その他</option>
                  </select>
                  <span class="help-block error">{{$errors->first('school_year')}}</span>
                </div>
              </div> --}}
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">卒業見込み年度　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('graduate_year')) has-error @endif">
                  <select name="graduate_year" class="form-control">
                    <option value="">選択してください</option>
                    <option value="2025年卒業予定" @if(old('graduate_year') == '2025年卒業予定') selected @endif>2025年卒業予定</option>
                    <option value="2024年卒業" @if(old('graduate_year') == '2024年卒業') selected @endif>2024年卒業</option>
                    <!-- <option value="2025年以降卒業予定" @if(old('graduate_year') == '2025年以降卒業予定') selected @endif>2025年以降卒業予定</option> -->
                    <option value="既卒" @if(old('graduate_year') == '既卒') selected @endif>既卒</option>
                    <option value="その他" @if(old('graduate_year') == 'その他') selected @endif>その他</option>
                  </select>
                  <span class="help-block error">{{$errors->first('graduate_year')}}</span>
                </div>
              </div>
              <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">このイベントを何で知りましたか（紹介者）<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('introduction')) has-error @endif">
                    <select name="introduction" class="form-control">
                      <option value="">選択してください</option>
                      <option value="柳本周介先生（将来塾・オフィスビギン）" @if(old('introduction') == '柳本周介先生（将来塾・オフィスビギン）') selected @endif>柳本周介先生（将来塾・オフィスビギン）</option>
                      <!-- <option value="Y-PU ENTERTAINMENT株式会社（Y-PU LAND）" @if(old('introduction') == 'Y-PU ENTERTAINMENT株式会社（Y-PU LAND）') selected @endif>Y-PU ENTERTAINMENT株式会社（Y-PU LAND）</option> -->
                      <!-- <option value="T-Pro consulting" @if(old('introduction') == 'T-Pro consulting') selected @endif>T-Pro consulting</option> -->
                      <option value="大学キャリアセンター" @if(old('introduction') == '大学キャリアセンター') selected @endif>大学キャリアセンター</option>
                      <option value="ネット検索" @if(old('introduction') == 'ネット検索') selected @endif>ネット検索</option>
                      <!-- <option value="合説ドットコム" @if(old('introduction') == '合説ドットコム') selected @endif>合説ドットコム</option> -->
                      <!-- <option value="ダイヤモンドDM・メール" @if(old('introduction') == 'ダイヤモンドDM・メール') selected @endif>ダイヤモンドDM・メール</option> -->
                      <option value="Instagram" @if(old('introduction') == 'Instagram') selected @endif>Instagram</option>
                      <option value="X(twitter)" @if(old('introduction') == 'X(twitter)') selected @endif>X(twitter)</option>
                      <option value="TikTok" @if(old('introduction') == 'TikTok') selected @endif>TikTok</option>
                      <option value="Facebook" @if(old('introduction') == 'Facebook') selected @endif>Facebook</option>
                      <option value="その他" @if(old('introduction') == 'その他') selected @endif>その他</option>
                    </select>
                    <span class="help-block error">{{$errors->first('introduction')}}</span>
                  </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">紹介者名　<span class="pull-right-container"><small class="label pull-right bg-green" style="margin-top:4px;">任意</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('introduction_name')) has-error @endif">
                  <input type="text" name="introduction_name" class="form-control" placeholder="紹介者名" value="{{ old('introduction_name', '') }}">
                  <span class="help-block error">{{$errors->first('introduction_name')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">紹介者学校名　<span class="pull-right-container"><small class="label pull-right bg-green" style="margin-top:4px;">任意</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('introduction_school_name')) has-error @endif">
                  <input type="text" name="introduction_school_name" class="form-control" placeholder="紹介者学校名" value="{{ old('introduction_school_name', '') }}">
                  <span class="help-block error">{{$errors->first('introduction_school_name')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">質問など　<span class="pull-right-container"><small class="label pull-right bg-green" style="margin-top:4px;">任意</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('note')) has-error @endif">
                  <textarea class="form-control" rows="5" name="note" placeholder="備考等を入力してください。">{{ old('note', '') }}</textarea>
                  <span class="help-block error">{{$errors->first('note')}}</span>
                </div>
              </div>
            </div>
          </div>
          <div class="box-footer">
            <div class="pull-right">
              <input type="submit" class="btn btn-block btn-success btn-sm" value="　内容確認　">
            </div>
            <div class="pull-right" style="margin-right: 8px;">
              <input type="button" class="btn btn-block btn-default btn-sm" onclick="location.href='{{ route('exhibitors') }}'" value="　戻　　る　">
            </div>
          </div>
        </form>
      </div>
    </div>
  </section>
  <!-- /.content -->
@endsection

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ $data->name }}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('/bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('/dist/css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/_all-skins.min.css') }}">

  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="{{ asset('/plugins/iCheck/all.css') }}">

  <link rel="stylesheet" href="{{ asset('css/pages.css') }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">

  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="#" class="navbar-brand"><b>{{ $data->name }}</b></a>
        </div>
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
        <form action="{{ route('event_form.thanks', array('code'=>Crypt::encrypt($data->code))) }}" method="post" id="form">
         @csrf
          <div class="box box-default">
            <div class="box-header with-border" style="text-align: center;">
              <h2 class="box-title" style="font-size: 24px;">事前エントリーフォーム</h2>
            </div>
            <div class="box-body">
              <div class="col-md-12">
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-red">必須</small></span> お名前</label>
                  <div class="col-xs-12 col-md-8">
                    <input type="text" name="name" readonly class="control-plaintext" value="{{ $data->request->name }}">
                  </div>
                </div>
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-red">必須</small></span> フリガナ</label>
                  <div class="col-xs-12 col-md-8">
                    <input type="text" name="kname" readonly class="control-plaintext" value="{{ $data->request->kname }}">
                  </div>
                </div>
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-red">必須</small></span> 性別</label>
                  <div class="col-xs-12 col-md-8">
                    @switch($data->request->gender)
                        @case(1)
                            <input type="text" readonly class="control-plaintext" value="男性">
                            @break
                        @case(2)
                        <input type="text" readonly class="control-plaintext" value="女性">
                            @break
                        @default
                        <input type="text" readonly class="control-plaintext" value="未回答">
                    @endswitch
                    <input type="hidden" name="gender" value="{{ $data->request->gender }}">
                  </div>
                </div>
                {{-- <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-red">必須</small></span> 生年月日</label>
                  <div class="col-xs-12 col-md-8">
                    <input type="text" name="birth" readonly class="control-plaintext" value="{{ $data->request->birth }}">
                    <input type="hidden" name="birth_y" value="{{ $data->request->birth_y }}">
                    <input type="hidden" name="birth_m" value="{{ $data->request->birth_m }}">
                    <input type="hidden" name="birth_d" value="{{ $data->request->birth_d }}">
                  </div>
                </div> --}}
                {{--
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-red">必須</small></span> 郵便番号</label>
                  <div class="col-xs-12 col-md-8">
                    <input type="text" name="zip" readonly class="control-plaintext" value="{{ $data->request->zip }}">
                  </div>
                </div>
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-red">必須</small></span> 都道府県</label>
                  <div class="col-xs-12 col-md-8">
                    <input type="text" name="address1" readonly class="control-plaintext" value="{{ $data->request->address1 }}">
                  </div>
                </div>
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-red">必須</small></span> ご住所</label>
                  <div class="col-xs-12 col-md-8">
                    <input type="text" name="address2" readonly class="control-plaintext" value="{{ $data->request->address2 }}">
                  </div>
                </div>
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-primary">任意</small></span> マンション名など</label>
                  <div class="col-xs-12 col-md-8">
                    <input type="text" name="address3" readonly class="control-plaintext" value="{{ $data->request->address3 }}">
                  </div>
                </div>
                --}}
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-red">必須</small></span> メールアドレス</label>
                  <div class="col-xs-12 col-md-8">
                    <input type="text" name="email" readonly class="control-plaintext" value="{{ $data->request->email }}">
                  </div>
                </div>
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-primary">任意</small></span> 電話番号</label>
                  <div class="col-xs-12 col-md-8">
                    <input type="text" name="phone" readonly class="control-plaintext" value="{{ $data->request->phone }}">
                  </div>
                </div>
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-red">必須</small></span> 在籍区分</label>
                  <div class="col-xs-12 col-md-8">
                    <input type="text" name="division" readonly class="control-plaintext" value="{{ $data->request->division }}">
                  </div>
                </div>
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-red">必須</small></span> 学校名</label>
                  <div class="col-xs-12 col-md-8">
                    <input type="text" name="school" readonly class="control-plaintext" value="{{ $data->request->school }}">
                  </div>
                </div>
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-primary">任意</small></span> 学部・コースなど</label>
                  <div class="col-xs-12 col-md-8">
                    <input type="text" name="gakubu" readonly class="control-plaintext" value="{{ $data->request->gakubu }}">
                  </div>
                </div>
                {{-- <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-red">必須</small></span> 学年</label>
                  <div class="col-xs-12 col-md-8">
                    <input type="text" name="school_year" readonly class="control-plaintext" value="{{ $data->request->school_year }}">
                  </div>
                </div> --}}
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-red">必須</small></span> 卒業見込年度</label>
                  <div class="col-xs-12 col-md-8">
                    <input type="text" name="graduate_year" readonly class="control-plaintext" value="{{ $data->request->graduate_year }}">
                  </div>
                </div>
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-red">必須</small></span> このイベントを何で知りましたか（紹介者）</label>
                  <div class="col-xs-12 col-md-8">
                    <input type="text" name="introduction" readonly class="control-plaintext" value="{{ $data->request->introduction }}">
                  </div>
                </div>
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-primary">任意</small></span> 紹介者名</label>
                  <div class="col-xs-12 col-md-8">
                    <input type="text" name="introduction_name" readonly class="control-plaintext" value="{{ $data->request->introduction_name }}">
                  </div>
                </div>
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-primary">任意</small></span> 紹介者学校名</label>
                  <div class="col-xs-12 col-md-8">
                    <input type="text" name="introduction_school_name" readonly class="control-plaintext" value="{{ $data->request->introduction_school_name }}">
                  </div>
                </div>
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-primary">任意</small></span> 質問など</label>
                  <div class="col-xs-12 col-md-8">
                    <input type="text" name="note" readonly class="control-plaintext" value="{{ $data->request->note }}">
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix no-border">
              <div class="center-block hidden-xs" style="text-align: center;">
                <button type="submit" name="back" class="btn btn-lg btn-default" value="back" style="margin-right: 8px;">　<i class="fa fa-arrow-circle-o-left"></i> 入力画面へ戻る　</button>
                <button type="submit" name="entry" class="btn btn-lg btn-warning" value="entry">　<i class="fa fa-arrow-circle-o-right"></i> エントリーする　</button>
              </div>
              <div class="center-block visible-xs" style="text-align: center;">
                <button type="submit" name="entry" class="btn btn-lg btn-warning" value="entry" style="margin-bottom: 8px;">　<i class="fa fa-arrow-circle-o-right"></i> エントリーする　</button>
                <button type="submit" name="back" class="btn btn-lg btn-default" value="back">　<i class="fa fa-arrow-circle-o-left"></i> 入力画面へ戻る　</button>
              </div>
            </div>

            <div id="overlay" class="overlay">
              <i class="fa fa-refresh fa-spin" style="z-index:99"></i>
            </div>
          </div>
          <!-- /.box -->
        </form>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>Version</b> {{ config('app.version', '1.0.0') }}
      </div>
      <strong>Copyright &copy; <a href="https://www.smilevision.co.jp" target="_blank">SmileVision</a> CO.LTD.</strong> All rights reserved.
    </div>
    <!-- /.container -->
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{ asset('/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('/bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/dist/js/adminlte.min.js') }}"></script>
<!-- iCheck 1.0.1 -->
<script src="{{ asset('/plugins/iCheck/icheck.min.js') }}"></script>

<!-- InputMask -->
<script src="{{ asset('/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>

<script src="{{ asset('js/pages/event_form.js') }}"></script>

<!-- ajaxzip3 -->
<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>

</body>
</html>

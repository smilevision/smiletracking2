<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ $data->name }}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('/bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('/dist/css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/_all-skins.min.css') }}">

  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="{{ asset('/plugins/iCheck/all.css') }}">

  <link rel="stylesheet" href="{{ asset('/css/event_form.css') }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PM2DZL8');
  </script>
  <!-- End Google Tag Manager -->
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-PM2DZL8');
</script>
<!-- End Google Tag Manager -->
<div class="wrapper">

  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="#" class="navbar-brand"><b>{{ $data->name }}</b></a>
        </div>
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
        <form action="{{ route('event_form.confirm', array('slug'=>$data->slug)) }}" method="post" id="form">
         @csrf
          <div class="box box-default">
            <div class="box-header with-border" style="text-align: center;">
              <h2 class="box-title" style="font-size: 24px;">{{ $data->categories->view_name }}用エントリーフォーム</h2>
            </div>
            <div class="box-body">
              <input type="hidden" name="event_id" value="{{ $data->id }}">
              <input type="hidden" name="categories_id" value="{{ $data->categories->id }}">
              <div class="col-md-12">
                @foreach ($entries_items as $item)
                <div class="form-group row" style="margin-top:16px;">
                    @if($item->is_required)
                    <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-red">必須</small></span> {{ $item->title }}</label>
                    @else
                    <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-primary">任意</small> {{ $item->title }}</span></label>
                    @endif
                  <div class="col-xs-12 col-md-8 @if($errors->has($item->name)) has-error @endif">
                    @if($item->style_id==4 && $item->style->is_option)
                    {{-- selectbox --}}
                    <select class="form-control" name="{{ $item->name }}">
                        <option value="">選択してください</option>
                        @foreach ($item->options as $option)
                        <option value="{{ $option->title }}" @if(old($item->name)==$option->title) selected @endif>{{ $option->title }}</option>
                        @endforeach
                    </select>
                    @else
                    {{-- input textbox --}}
                    <input
                        type="text"
                        class="form-control"
                        @if($item->style_id==10)
                        {{-- 郵便番号での検索 --}}
                        onchange="AjaxZip3.zip2addr(this,'','address1','address1');"
                        @endif
                        maxlength="{{ $item->digits }}"
                        name="{{ $item->name }}"
                        placeholder="{{ $item->placeholder }}"
                        value="{{ old($item->name) }}"
                        autocomplete="off">
                    @endif
                    <span class="help-block error">{{$errors->first($item->name)}}</span>
                  </div>
                </div>
                @endforeach
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix no-border">
              <button type="submit" class="btn btn-lg btn-warning center-block">　<i class="fa fa-arrow-circle-o-right"></i> 確認画面へ進む　</button>
            </div>

            <div id="overlay" class="overlay">
              <i class="fa fa-refresh fa-spin" style="z-index:99"></i>
            </div>
          </div>
          <!-- /.box -->
        </form>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>Version</b> {{ config('app.version', '1.0.0') }}
      </div>
      <strong>Copyright &copy; <a href="https://www.smilevision.co.jp" target="_blank">SmileVision</a> CO.LTD.</strong> All rights reserved.
    </div>
    <!-- /.container -->
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{ asset('/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('/bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/dist/js/adminlte.min.js') }}"></script>
<!-- iCheck 1.0.1 -->
<script src="{{ asset('/plugins/iCheck/icheck.min.js') }}"></script>

<!-- InputMask -->
<script src="{{ asset('/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>

<script src="{{ asset('js/pages/event_form.js') }}"></script>

<!-- ajaxzip3 -->
<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>

</body>
</html>

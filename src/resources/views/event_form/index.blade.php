<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ $data->name }}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('/bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('/dist/css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/_all-skins.min.css') }}">

  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="{{ asset('/plugins/iCheck/all.css') }}">

  <link rel="stylesheet" href="{{ asset('/css/event_form.css') }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PM2DZL8');
  </script>
  <!-- End Google Tag Manager -->
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PM2DZL8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="wrapper">

  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="#" class="navbar-brand"><b>{{ $data->name }}</b></a>
        </div>
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
        <form action="{{ route('event_form.confirm', array('code'=>Crypt::encrypt($data->code))) }}" method="post" id="form">
         @csrf
          <div class="box box-default">
            <div class="box-header with-border" style="text-align: center;">
              <h2 class="box-title" style="font-size: 24px;">事前エントリーフォーム</h2>
            </div>
            <div class="box-body">
              <div class="col-md-12">
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-red">必須</small></span> お名前</label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('name')) has-error @endif">
                    <input type="text" name="name" class="form-control" placeholder="兵庫 太郎" value="{{ old('name') }}" autocomplete="off">
                    <span class="help-block error">{{$errors->first('name')}}</span>
                  </div>
                </div>
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-red">必須</small></span> フリガナ</label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('kname')) has-error @endif">
                    <input type="text" name="kname" class="form-control" placeholder="ヒョウゴ タロウ" value="{{ old('kname') }}" autocomplete="off">
                    <span class="help-block error">{{$errors->first('kname')}}</span>
                  </div>
                </div>
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-red">必須</small></span> 性別</label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('gender')) has-error @endif">
                    <label style="margin-right: 16px;">
                      <input type="radio" name="gender" class="flat-red" value="1" @if(old('gender')=='1') checked @endif)> 男性
                    </label>
                    <label style="margin-right: 16px;">
                      <input type="radio" name="gender" class="flat-red" value="2" @if(old('gender')=='2') checked @endif)> 女性
                    </label>
                    <label style="margin-right: 16px;">
                      <input type="radio" name="gender" class="flat-red" value="0" @if(old('gender')=='0') checked @endif)> 未回答
                    </label>
                    <span class="help-block error">{{$errors->first('gender')}}</span>
                  </div>
                </div>
                {{-- <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-red">必須</small></span> 生年月日</label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('birth')) has-error @endif">
                    <div class="col-md-4 birth_y">
                      <input type="number" name="birth_y" class="form-control" placeholder="年" value="{{ old('birth_y') }}">
                    </div>
                    <div class="col-md-4 birth_m">
                      <input type="number" name="birth_m" class="form-control" placeholder="月" value="{{ old('birth_m') }}">
                    </div>
                    <div class="col-md-4 birth_d">
                      <input type="number" name="birth_d" class="form-control" placeholder="日" value="{{ old('birth_d') }}">
                    </div>
                    <span class="text-danger">※ 半角数字で入力してください。</span>
                    <span class="help-block error">{{$errors->first('birth')}}</span>
                  </div>
                </div> --}}
                {{--
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-red">必須</small></span> 郵便番号</label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('zip')) has-error @endif">
                    <input type="text" id="zip" name="zip" class="form-control" value="{{ old('zip') }}" autocomplete="off" placeholder="000-0000" onchange="AjaxZip3.zip2addr(this,'','address1','address2');">
                    <span class="help-block error">{{$errors->first('zip')}}</span>
                  </div>
                </div>
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-red">必須</small></span> 都道府県</label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('address1')) has-error @endif">
                    <select class="form-control" name="address1" style="width: 100%;">
                      <option value="">選択してください</option>
                      <option value="北海道" data-pref-id="01" @if(old('address1') == '北海道') selected @endif>北海道</option>
                      <option value="青森県" data-pref-id="02" @if(old('address1') == '青森県') selected @endif>青森県</option>
                      <option value="岩手県" data-pref-id="03" @if(old('address1') == '岩手県') selected @endif>岩手県</option>
                      <option value="宮城県" data-pref-id="04" @if(old('address1') == '宮城県') selected @endif>宮城県</option>
                      <option value="秋田県" data-pref-id="05" @if(old('address1') == '秋田県') selected @endif>秋田県</option>
                      <option value="山形県" data-pref-id="06" @if(old('address1') == '山形県') selected @endif>山形県</option>
                      <option value="福島県" data-pref-id="07" @if(old('address1') == '福島県') selected @endif>福島県</option>
                      <option value="茨城県" data-pref-id="08" @if(old('address1') == '茨城県') selected @endif>茨城県</option>
                      <option value="栃木県" data-pref-id="09" @if(old('address1') == '栃木県') selected @endif>栃木県</option>
                      <option value="群馬県" data-pref-id="10" @if(old('address1') == '群馬県') selected @endif>群馬県</option>
                      <option value="埼玉県" data-pref-id="11" @if(old('address1') == '埼玉県') selected @endif>埼玉県</option>
                      <option value="千葉県" data-pref-id="12" @if(old('address1') == '千葉県') selected @endif>千葉県</option>
                      <option value="東京都" data-pref-id="13" @if(old('address1') == '東京都') selected @endif>東京都</option>
                      <option value="神奈川県" data-pref-id="14" @if(old('address1') == '神奈川県') selected @endif>神奈川県</option>
                      <option value="新潟県" data-pref-id="15" @if(old('address1') == '新潟県') selected @endif>新潟県</option>
                      <option value="富山県" data-pref-id="16" @if(old('address1') == '富山県') selected @endif>富山県</option>
                      <option value="石川県" data-pref-id="17" @if(old('address1') == '石川県') selected @endif>石川県</option>
                      <option value="福井県" data-pref-id="18" @if(old('address1') == '福井県') selected @endif>福井県</option>
                      <option value="山梨県" data-pref-id="19" @if(old('address1') == '山梨県') selected @endif>山梨県</option>
                      <option value="長野県" data-pref-id="20" @if(old('address1') == '長野県') selected @endif>長野県</option>
                      <option value="岐阜県" data-pref-id="21" @if(old('address1') == '岐阜県') selected @endif>岐阜県</option>
                      <option value="静岡県" data-pref-id="22" @if(old('address1') == '静岡県') selected @endif>静岡県</option>
                      <option value="愛知県" data-pref-id="23" @if(old('address1') == '愛知県') selected @endif>愛知県</option>
                      <option value="三重県" data-pref-id="24" @if(old('address1') == '三重県') selected @endif>三重県</option>
                      <option value="滋賀県" data-pref-id="25" @if(old('address1') == '滋賀県') selected @endif>滋賀県</option>
                      <option value="京都府" data-pref-id="26" @if(old('address1') == '京都府') selected @endif>京都府</option>
                      <option value="大阪府" data-pref-id="27" @if(old('address1') == '大阪府') selected @endif>大阪府</option>
                      <option value="兵庫県" data-pref-id="28" @if(old('address1') == '兵庫県') selected @endif>兵庫県</option>
                      <option value="奈良県" data-pref-id="29" @if(old('address1') == '奈良県') selected @endif>奈良県</option>
                      <option value="和歌山県" data-pref-id="30" @if(old('address1') == '和歌山県') selected @endif>和歌山県</option>
                      <option value="鳥取県" data-pref-id="31" @if(old('address1') == '鳥取県') selected @endif>鳥取県</option>
                      <option value="島根県" data-pref-id="32" @if(old('address1') == '島根県') selected @endif>島根県</option>
                      <option value="岡山県" data-pref-id="33" @if(old('address1') == '岡山県') selected @endif>岡山県</option>
                      <option value="広島県" data-pref-id="34" @if(old('address1') == '広島県') selected @endif>広島県</option>
                      <option value="山口県" data-pref-id="35" @if(old('address1') == '山口県') selected @endif>山口県</option>
                      <option value="徳島県" data-pref-id="36" @if(old('address1') == '徳島県') selected @endif>徳島県</option>
                      <option value="香川県" data-pref-id="37" @if(old('address1') == '香川県') selected @endif>香川県</option>
                      <option value="愛媛県" data-pref-id="38" @if(old('address1') == '愛媛県') selected @endif>愛媛県</option>
                      <option value="高知県" data-pref-id="39" @if(old('address1') == '高知県') selected @endif>高知県</option>
                      <option value="福岡県" data-pref-id="40" @if(old('address1') == '福岡県') selected @endif>福岡県</option>
                      <option value="佐賀県" data-pref-id="41" @if(old('address1') == '佐賀県') selected @endif>佐賀県</option>
                      <option value="長崎県" data-pref-id="42" @if(old('address1') == '長崎県') selected @endif>長崎県</option>
                      <option value="熊本県" data-pref-id="43" @if(old('address1') == '熊本県') selected @endif>熊本県</option>
                      <option value="大分県" data-pref-id="44" @if(old('address1') == '大分県') selected @endif>大分県</option>
                      <option value="宮崎県" data-pref-id="45" @if(old('address1') == '宮崎県') selected @endif>宮崎県</option>
                      <option value="鹿児島県" data-pref-id="46" @if(old('address1') == '鹿児島県') selected @endif>鹿児島県</option>
                      <option value="沖縄県" data-pref-id="47" @if(old('address1') == '沖縄県') selected @endif>沖縄県</option>
                    </select>
                    <span class="help-block error">{{$errors->first('address1')}}</span>
                  </div>
                </div>
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-red">必須</small></span> ご住所</label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('address2')) has-error @endif">
                    <input type="text" name="address2" class="form-control" placeholder="123-4567" value="{{ old('address2') }}" autocomplete="off">
                    <span class="help-block error">{{$errors->first('address2')}}</span>
                  </div>
                </div>
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-primary">任意</small></span> マンション名など</label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('address3')) has-error @endif">
                    <input type="text" name="address3" class="form-control" placeholder="マンション名など" value="{{ old('address3') }}" autocomplete="off">
                    <span class="help-block error">{{$errors->first('address3')}}</span>
                  </div>
                </div>
                --}}
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-red">必須</small></span> メールアドレス</label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('email')) has-error @endif">
                    <input type="text" name="email" class="form-control" placeholder="メールアドレス" value="{{ old('email') }}" autocomplete="off">
                    <span class="help-block error">{{$errors->first('email')}}</span>
                  </div>
                </div>
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-red">必須</small></span> 電話番号</label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('phone')) has-error @endif">
                    <input type="text" name="phone" class="form-control" placeholder="000-0000-0000" value="{{ old('phone') }}" autocomplete="off">
                    <span class="help-block error">{{$errors->first('phone')}}</span>
                  </div>
                </div>
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-red">必須</small></span> 在籍区分</label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('division')) has-error @endif">
                    <select class="form-control" name="division" style="width: 100%;">
                      <option value="">選択してください</option>
                      <option value="大学" @if(old('division') == '大学') selected @endif>大学</option>
                      <option value="大学院" @if(old('division') == '大学院') selected @endif>大学院</option>
                      <option value="短大・専門学校" @if(old('division') == '短大・専門学校') selected @endif>短大・専門学校</option>
                      <option value="既卒" @if(old('division') == '既卒') selected @endif>既卒</option>
                      <option value="その他" @if(old('division') == 'その他') selected @endif>その他</option>
                    </select>
                    <span class="help-block error">{{$errors->first('division')}}</span>
                  </div>
                </div>
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-red">必須</small></span> 学校名</label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('school')) has-error @endif">
                    <input type="text" name="school" class="form-control" placeholder="学校名" value="{{ old('school') }}" autocomplete="off">
                    <span class="help-block error">{{$errors->first('school')}}</span>
                  </div>
                </div>
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-primary">任意</small></span> 学部・コースなど</label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('gakubu')) has-error @endif">
                    <input type="text" name="gakubu" class="form-control" placeholder="学部・コースなど" value="{{ old('gakubu') }}" autocomplete="off">
                    <span class="help-block error">{{$errors->first('gakubu')}}</span>
                  </div>
                </div>
                {{-- <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-red">必須</small></span> 学年</label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('school_year')) has-error @endif">
                    <select class="form-control" name="school_year" style="width: 100%;">
                      <option value="">選択してください</option>
                      <option value="1年生" @if(old('school_year') == '1年生') selected @endif>１年生</option>
                      <option value="2年生" @if(old('school_year') == '2年生') selected @endif>２年生</option>
                      <option value="3年生" @if(old('school_year') == '3年生') selected @endif>３年生</option>
                      <option value="4年生" @if(old('school_year') == '4年生') selected @endif>４年生</option>
                      <option value="その他" @if(old('school_year') == 'その他') selected @endif>その他</option>
                    </select>
                    <span class="help-block error">{{$errors->first('school_year')}}</span>
                  </div>
                </div> --}}
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-red">必須</small></span> 卒業見込年度</label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('graduate_year')) has-error @endif">
                    <select class="form-control" name="graduate_year" style="width: 100%;">
                      <option value="">選択してください</option>
                      <option value="2025年卒業予定" @if(old('graduate_year') == '2025年卒業予定') selected @endif>2025年卒業予定</option>
                      <option value="2024年卒業" @if(old('graduate_year') == '2024年卒業') selected @endif>2024年卒業</option>
                      <!-- <option value="2025年以降卒業予定" @if(old('graduate_year') == '2025年以降卒業予定') selected @endif>2025年以降卒業予定</option> -->
                      <option value="既卒" @if(old('graduate_year') == '既卒') selected @endif>既卒</option>
                      <option value="その他" @if(old('graduate_year') == 'その他') selected @endif>その他</option>
                    </select>
                    <span class="help-block error">{{$errors->first('graduate_year')}}</span>
                  </div>
                </div>
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-red">必須</small></span> このイベントを何で知りましたか（紹介者）</label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('introduction')) has-error @endif">
                    <select class="form-control" name="introduction" style="width: 100%;">
                      <option value="">選択してください</option>
                      <option value="柳本周介先生（将来塾・オフィスビギン）" @if(old('introduction') == '柳本周介先生（将来塾・オフィスビギン）') selected @endif>柳本周介先生（将来塾・オフィスビギン）</option>
                      <!-- <option value="Y-PU ENTERTAINMENT株式会社（Y-PU LAND）" @if(old('introduction') == 'Y-PU ENTERTAINMENT株式会社（Y-PU LAND）') selected @endif>Y-PU ENTERTAINMENT株式会社（Y-PU LAND）</option> -->
                      <!-- <option value="T-Pro consulting" @if(old('introduction') == 'T-Pro consulting') selected @endif>T-Pro consulting</option> -->
                      <option value="大学キャリアセンター" @if(old('introduction') == '大学キャリアセンター') selected @endif>大学キャリアセンター</option>
                      <option value="ネット検索" @if(old('introduction') == 'ネット検索') selected @endif>ネット検索</option>
                      <!-- <option value="合説ドットコム" @if(old('introduction') == '合説ドットコム') selected @endif>合説ドットコム</option> -->
                      <!-- <option value="ダイヤモンドDM・メール" @if(old('introduction') == 'ダイヤモンドDM・メール') selected @endif>ダイヤモンドDM・メール</option> -->
                      <option value="Instagram" @if(old('introduction') == 'Instagram') selected @endif>Instagram</option>
                      <option value="X(twitter)" @if(old('introduction') == 'X(twitter)') selected @endif>X(twitter)</option>
                      <option value="TikTok" @if(old('introduction') == 'TikTok') selected @endif>TikTok</option>
                      <option value="Facebook" @if(old('introduction') == 'Facebook') selected @endif>Facebook</option>
                      <option value="その他" @if(old('introduction') == 'その他') selected @endif>その他</option>
                    </select>
                    <span class="help-block error">{{$errors->first('introduction')}}</span>
                  </div>
                </div>
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-primary">任意</small></span> 紹介者名</label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('introduction_name')) has-error @endif">
                    <input type="text" name="introduction_name" class="form-control" placeholder="紹介者名" value="{{ old('introduction_name') }}" autocomplete="off">
                    <span class="help-block error">{{$errors->first('introduction_name')}}</span>
                  </div>
                </div>
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-primary">任意</small></span> 紹介者学校名</label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('introduction_school_name')) has-error @endif">
                    <input type="text" name="introduction_school_name" class="form-control" placeholder="紹介者学校名" value="{{ old('introduction_school_name') }}" autocomplete="off">
                    <span class="help-block error">{{$errors->first('introduction_school_name')}}</span>
                  </div>
                </div>
                <div class="form-group row" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-3 control-label"><span class="pull-right-container"><small class="label bg-primary">任意</small></span> 質問など</label>
                  <div class="col-xs-12 col-md-8 @if($errors->has('note')) has-error @endif">
                    <textarea class="form-control" name="note" id="" cols="30" rows="10" placeholder="">{{ old('note') }}</textarea>
                    <span class="help-block error">{{$errors->first('note')}}</span>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix no-border">
              <button type="submit" class="btn btn-lg btn-warning center-block">　<i class="fa fa-arrow-circle-o-right"></i> 確認画面へ進む　</button>
            </div>

            <div id="overlay" class="overlay">
              <i class="fa fa-refresh fa-spin" style="z-index:99"></i>
            </div>
          </div>
          <!-- /.box -->
        </form>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>Version</b> {{ config('app.version', '1.0.0') }}
      </div>
      <strong>Copyright &copy; <a href="https://www.smilevision.co.jp" target="_blank">SmileVision</a> CO.LTD.</strong> All rights reserved.
    </div>
    <!-- /.container -->
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{ asset('/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('/bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/dist/js/adminlte.min.js') }}"></script>
<!-- iCheck 1.0.1 -->
<script src="{{ asset('/plugins/iCheck/icheck.min.js') }}"></script>

<!-- InputMask -->
<script src="{{ asset('/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>

<script src="{{ asset('js/pages/event_form.js') }}"></script>

<!-- ajaxzip3 -->
<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>

</body>
</html>

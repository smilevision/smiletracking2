@extends('layouts.app')
@include('layouts.header')
@include('layouts.leftmenu')
@include('layouts.footer')

@section('pageCss')

@endsection

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-bank"></i> 出展企業 新規登録
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <form method="post" action="{{ route('exhibitors.regist.check', array('event_id'=>$data->event_id)) }}" class="form-horizontal">
          @csrf
          <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
            <div class="col-md-12">
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">出展情報</label>
                <div class="col-xs-12 col-md-2">
                  <select name="is_join" id="is_join" class="form-control">
                    <option value="0">未定</option>
                    <option value="1">出展</option>
                  </select>
                </div>
              </div>
              <div class="form-group" style="margin-top:16px;">
                <label class="col-xs-12 col-md-2 control-label">会社名・団体名 等　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('name')) has-error @endif">
                  <input type="text" name="name" class="form-control" placeholder="会社名・団体名 等" value="{{ old('name', '') }}">
                  <span class="help-block error">{{$errors->first('name')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">担当者名</label>
                <div class="col-xs-12 col-md-8 @if($errors->has('tanto')) has-error @endif">
                  <input type="text" name="tanto" class="form-control" placeholder="担当者名" value="{{ old('tanto', '') }}">
                  <span class="help-block error">{{$errors->first('tanto')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">郵便番号</label>
                <div class="col-xs-12 col-md-8">
                  <div class="col-xs-4 col-md-2 @if($errors->has('zip1')) has-error @endif" style="padding:0;">
                    <input type="text" name="zip1" class="form-control" placeholder="000" value="{{ old('zip1', '') }}" onKeyUp="AjaxZip3.zip2addr('zip1','zip2','address','address');">
                    <span class="help-block error">{{$errors->first('zip1')}}</span>
                  </div>
                  <div class="col-xs-1 col-md-1" style="padding:0; text-align:center; margin-top: 4px;">
                    ー
                  </div>
                  <div class="col-xs-4 col-md-2 @if($errors->has('zip2')) has-error @endif" style="padding:0;">
                    <input type="text" name="zip2" class="form-control" placeholder="0000" value="{{ old('zip2', '') }}" onKeyUp="AjaxZip3.zip2addr('zip1','zip2','address','address');">
                    <span class="help-block error">{{$errors->first('zip2')}}</span>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">住所</label>
                <div class="col-xs-12 col-md-8 @if($errors->has('address')) has-error @endif">
                  <input type="text" name="address" class="form-control" placeholder="住所" value="{{ old('address', '') }}">
                  <span class="help-block error">{{$errors->first('address')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">電話番号</label>
                <div class="col-xs-12 col-md-8 @if($errors->has('phone')) has-error @endif">
                  <input type="text" name="phone" class="form-control" placeholder="電話番号" value="{{ old('phone', '') }}">
                  <span class="help-block error">{{$errors->first('phone')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">メールアドレス　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <div class="col-xs-12 col-md-8 @if($errors->has('email')) has-error @endif">
                  <input type="text" name="email" class="form-control" placeholder="メールアドレス" value="{{ old('email', '') }}">
                  <span class="help-block error">{{$errors->first('email')}}</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">その他・備考・説明 等</label>
                <div class="col-xs-12 col-md-8 @if($errors->has('note')) has-error @endif">
                  <textarea class="form-control" rows="5" name="note" placeholder="その他・備考・説明 等を入力してください。">{{ old('note', '') }}</textarea>
                  <span class="help-block error">{{$errors->first('note')}}</span>
                </div>
              </div>
            </div>
          </div>
          <div class="box-footer">
            <div class="pull-right">
              <input type="submit" class="btn btn-block btn-success btn-sm" value="　内容確認　">
            </div>
            <div class="pull-right" style="margin-right: 8px;">
              <input type="button" class="btn btn-block btn-default btn-sm" onclick="location.href='{{ route('exhibitors') }}'" value="　戻　　る　">
            </div>
          </div>
        </form>
      </div>
    </div>
  </section>
  <!-- /.content -->
@endsection

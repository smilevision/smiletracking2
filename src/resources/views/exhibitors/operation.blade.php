@extends('layouts.app')
@include('layouts.header')
@include('layouts.leftmenu')
@include('layouts.footer')

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-bank"></i> 出展企業情報 一括出展操作
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <form method="post" id="operation_form" action="{{ route('exhibitors.join') }}" class="form-horizontal">
          @csrf
            <input type="hidden" name="event_id" value="{{ $target_event->id }}">
            <div class="box-header with-border">
              <h3 class="box-title">{{ $target_event->name }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              @if(count($exhibitors)==0)
              <p>出展未定の企業はございません。</p>
              @else
              <table class="table table-bordered table-striped">
                <tr>
                  <th width="80px">出展情報</th>
                  <th>会社名</th>
                </tr>
                @foreach ($exhibitors as $exhibitor)
                <tr>
                  <td style="vertical-align:middle; text-align:center;">
                    <label>
                      <input type="checkbox" name="exhibitor_ids[]" class="radio_btn_operation flat-red" value="{{ $exhibitor->id }}">
                    </label>
                  </td>
                  <td style="vertical-align:middle;">{{ $exhibitor->name }}</td>
                </tr>
                @endforeach
              </table>
              @endif
            </div>
            <div class="box-footer">
              <div class="pull-right">
                <input type="submit" class="btn btn-block btn-success btn-sm" @if(count($exhibitors)==0) disabled @endif value="　更　　新　">
              </div>
              <div class="pull-right" style="margin-right: 8px;">
                <input type="button" class="btn btn-block btn-default btn-sm" onclick="location.href='{{ route('exhibitors', array('event_id'=>$target_event->id)) }}'" value="　戻　　る　">
              </div>
            </div>
            <div id="overlay" class="overlay">
              <i class="fa fa-refresh fa-spin" style="z-index:99"></i>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
  <!-- /.content -->
@endsection

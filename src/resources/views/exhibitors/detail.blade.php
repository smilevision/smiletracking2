@extends('layouts.app')
@include('layouts.header')
@include('layouts.leftmenu')
@include('layouts.footer')

@section('pageCss')

@endsection

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-bank"></i> 出展企業情報 確認・編集
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        @if(!empty($status))
        @if($status=='complete')
        <div class="alert alert-success alert-dismissible">
          <h4><i class="icon fa fa-check"></i> 更新成功！</h4>
          出展企業情報を更新しました。
        </div>
        @else
        <div class="alert alert-danger alert-dismissible">
          <h4><i class="icon fa fa-ban"></i> 更新失敗！</h4>
          出展企業情報の更新が失敗しました。
        </div>
        @endif
        @endif
        <form method="post" id="form" action="{{ route('exhibitors.update', array('id'=>$exhibitors->id)) }}" class="form-horizontal">
          @csrf
          <input type="hidden" id="exhibitors_id" value="{{ $exhibitors->id }}">
          <input type="hidden" id="status" value="{{ !empty($status)?$status:'' }}">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">出展情報</label>
                  <div class="col-xs-12 col-md-2">
                    <select name="is_join" id="" class="form-control">
                      <option value="0" @if($exhibitors->is_join==0) selected @endif>未定</option>
                      <option value="1" @if($exhibitors->is_join==1) selected @endif>出展</option>
                    </select>
                  </div>
                </div>
                <div class="form-group" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-2 control-label">企業コード</label>
                  <label class="col-xs-12 col-md-8 control-value">{{ $exhibitors->code }}</label>
                </div>
                <div class="form-group" style="margin-top:16px;">
                  <label class="col-xs-12 col-md-2 control-label">会社名・団体名 等　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                  <div class="col-xs-12 col-md-8">
                    <input type="text" name="name" class="form-control" placeholder="会社名・団体名 等" value="{{ old('name', $exhibitors->name) }}">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">担当者名</label>
                  <div class="col-xs-12 col-md-8">
                    <input type="text" name="tanto" class="form-control" placeholder="担当者名" value="{{ old('tanto', $exhibitors->tanto) }}">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">郵便番号</label>
                  <div class="col-xs-12 col-md-8">
                    <div class="col-xs-4 col-md-2" style="padding:0;">
                      <input type="text" name="zip1" class="form-control" placeholder="000" value="{{ old('zip1', $exhibitors->zip1) }}" onKeyUp="AjaxZip3.zip2addr('zip1','zip2','address','address');">
                    </div>
                    <div class="col-xs-1 col-md-1" style="padding:0; text-align:center; margin-top: 4px;">
                      ー
                    </div>
                    <div class="col-xs-4 col-md-2" style="padding:0;">
                      <input type="text" name="zip2" class="form-control" placeholder="0000" value="{{ old('zip2', $exhibitors->zip2) }}" onKeyUp="AjaxZip3.zip2addr('zip1','zip2','address','address');">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">住所</label>
                  <div class="col-xs-12 col-md-8">
                    <input type="text" name="address" class="form-control" placeholder="会場 電話番号" value="{{ old('address', $exhibitors->address) }}">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">電話番号</label>
                  <div class="col-xs-12 col-md-8">
                    <input type="text" name="phone" class="form-control" placeholder="電話番号" value="{{ old('phone', $exhibitors->phone) }}">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">メールアドレス　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                  <div class="col-xs-12 col-md-8">
                    <input type="text" name="email" class="form-control" placeholder="担当者メール" value="{{ old('email', $exhibitors->email) }}">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-12 col-md-2 control-label">その他・備考・説明 等</label>
                  <div class="col-xs-12 col-md-8">
                    <textarea class="form-control" rows="5" name="note" placeholder="その他・備考・説明 等を入力してください。">{{ old('note', $exhibitors->note) }}</textarea>
                  </div>
                </div>
              </div>
            </div>
            <div class="box-footer">
              <div class="pull-right">
                <input type="submit" class="btn btn-block btn-success btn-sm" value="　更　　新　">
              </div>
              <div class="pull-right" style="margin-right: 8px;">
                <input type="button" id="delete" class="btn btn-block btn-danger btn-sm" value="　削　　除　">
              </div>
              {{-- <div class="pull-right" style="margin-right: 8px;">
                <input type="button" id="reset_password" data-id="{{ $exhibitors->id }}" class="btn btn-block btn-warning btn-sm" value="　パスワード再設定　">
              </div> --}}
              <div class="pull-right" style="margin-right: 8px;">
                <input type="button" class="btn btn-block btn-default btn-sm" onclick="location.href='{{ route('exhibitors', array('event_id'=>$exhibitors->event->id)) }}'" value="　戻　　る　">
              </div>
            </div>
            <div id="overlay" class="overlay">
              <i class="fa fa-refresh fa-spin" style="z-index:99"></i>
            </div>
          </div>
        </form>
      </div>
    </div>
  </section>
  <!-- /.content -->
@endsection

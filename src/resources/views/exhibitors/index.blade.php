@extends('layouts.app')
@include('layouts.header')
@include('layouts.leftmenu')
@include('layouts.footer')

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-bank"></i> 出展企業情報
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box">
      <form method="post" id="search_form" action="{{ route('exhibitors.find') }}" class="form-horizontal">
      <input type="hidden" id="status" value="{{ !empty($status)?$status:'' }}">
      @csrf
        <div class="box-body">
          <div class="col-xs-12 col-md-12">
            <div class="form-group">
              <label class="col-md-1 control-label">イベント</label>
              <div class="col-md-4">
              <select name="search_event" id="search_event" class="form-control" onchange="submit(this.form)">
                <option value="">イベント選択なし</option>
                @foreach ($findItems->event as $event)
                <option value="{{ $event->id }}" @if(!empty($conditions) && $conditions['search_event'] == $event->id) selected @endif>{{ $event->name }}</option>
                @endforeach
              </select>
              </div>
              <div class="col-md-2">
                <a id="btn_event_detail" href="{{ route('event.detail', array('id'=>$target_event->id)) }}" class="btn btn-block btn-warning btn-sm @if(empty($target_event->id))disabled @endif"> イベント情報</a>
              </div>
              <div class="col-md-2">
                <a id="btn_event_operation" href="{{ route('exhibitors.operation', array('event_id'=>$target_event->id)) }}" class="btn btn-block btn-warning btn-sm @if(empty($target_event->id))disabled @endif"><i class="fa fa-play" style="margin-right:6px;"></i> 一括出展操作</a>
              </div>
            </div>
          </div>
        </div>
      </form>
      <div id="overlay" class="overlay">
        <i class="fa fa-refresh fa-spin" style="z-index:99"></i>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">@if(empty($exhibitors)) 出展店舗はございません。 @else {{ $target_event->name }}：出展企業（{{ $exhibitors->total() }} 件）@endif</h3>
            <div class="pull-right">
              <a href="{{ route('exhibitors.regist', array('event_id'=>$target_event->id)) }}" class="btn btn-block btn-warning btn-sm @if(empty($target_event->id))disabled @endif"><i class="fa fa-play" style="margin-right:6px;"></i> 新規登録</a>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            @if(!empty($exhibitors))
            <div class="table-responsive">
              <table class="table table-bordered table-striped">
                <tr>
                  <th width="80px">出展情報</th>
                  <th>企業コード</th>
                  <th>会社名<a href="{{ route('exhibitors', array('event_id'=>$target_event->id, 'sort_item'=>'name', 'sort_mode'=>'desc')) }}" class="sort">▼</a><a href="{{ route('exhibitors', array('event_id'=>$target_event->id, 'sort_item'=>'name', 'sort_mode'=>'asc')) }}" class="sort">▲</a></th>
                  <th>メールアドレス</th>
                  <th>担当者</th>
                  <th>訪問人数<a href="{{ route('exhibitors', array('event_id'=>$target_event->id, 'sort_item'=>'visit_count', 'sort_mode'=>'desc')) }}" class="sort">▼</a><a href="{{ route('exhibitors', array('event_id'=>$target_event->id, 'sort_item'=>'visit_count', 'sort_mode'=>'asc')) }}" class="sort">▲</a></th>
                  {{--<th width="120px">ログインQR</th>--}}
                  <th width="80px">印刷</th>
                  <th width="80px">登録内容</th>
                </tr>
                @foreach ($exhibitors as $exhibitor)
                <tr>
                  <td style="vertical-align:middle; text-align:center;">
                  @if($exhibitor->is_join==1)
                    <span class="pull-right-container">
                      <small class="label bg-green">参加</small>
                    </span>
                  @else
                  <span class="pull-right-container">
                    <small class="label bg-gray">未定</small>
                  </span>
                  @endif
                  </td>
                  <td style="vertical-align:middle;">{{ $exhibitor->code }}</td>
                  <td style="vertical-align:middle;">{{ $exhibitor->name }}</td>
                  <td style="vertical-align:middle;">{{ $exhibitor->email }}</td>
                  <td style="vertical-align:middle;">{{ $exhibitor->tanto }}</td>
                  <td style="vertical-align:middle;">{{ $exhibitor->visit_count }}</td>
                  {{--<td style="text-align: center;"><img src="{{ asset('images/qr/sample.png') }}" alt="" style="height:48px;"></td>--}}
                  <td style="vertical-align:middle;">
                    <a href="{{ route('qrcode.download', array('uuid' => $exhibitor->relation->user->uuid)) }}" class="btn btn-app" target="_blank">
                      <i class="fa fa-print"></i>
                    </a>
                  </td>
                  <td style="vertical-align:middle;">
                    <a href="{{ route('exhibitors.detail', array('id'=>$exhibitor->id)) }}" class="btn btn-block btn-warning btn-sm">確認・編集</a>
                  </td>
                </tr>
                @endforeach
              </table>
              {{ $exhibitors->links() }}
            </div>
            @endif
          </div>
          <div id="overlay_list" class="overlay">
            <i class="fa fa-refresh fa-spin" style="z-index:99"></i>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- /.content -->
@endsection

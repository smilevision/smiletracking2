@extends('layouts.app')
@include('layouts.header')
@include('layouts.leftmenu')
@include('layouts.footer')

@section('pageCss')

@endsection

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-bank"></i> 出展企業 - 入力内容確認
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <form method="post" action="{{ route('exhibitors.regist.complete', array('event_id'=>$data->event_id)) }}" class="form-horizontal">
          @csrf
          <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
            <input type="hidden" name="code" value="{{ $data->code }}">
            <div class="col-md-12">
              <div class="form-group" style="margin-top:16px;">
                <label class="col-xs-12 col-md-2 control-label">出展情報</label>
                <label class="col-xs-12 col-md-8 control-value">@if($data->is_join==1) 参加 @else 未定 @endif</label>
                <input type="hidden" name="is_join" value="{{ $data->is_join }}">
              </div>
              <div class="form-group" style="margin-top:16px;">
                <label class="col-xs-12 col-md-2 control-label">会社名・団体名 等　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <label class="col-xs-12 col-md-8 control-value">{{ $data->name }}</label>
                <input type="hidden" name="name" value="{{ $data->name }}">
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">担当者名</label>
                <label class="col-xs-12 col-md-8 control-value">{{ $data->tanto }}</label>
                <input type="hidden" name="tanto" value="{{ $data->tanto }}">
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">郵便番号</label>
                <label class="col-xs-12 col-md-8 control-value">{{ $data->zip1 }} @if(!empty($data->zip1)) - @endif {{ $data->zip2 }}</label>
                <input type="hidden" name="zip1" value="{{ $data->zip1 }}">
                <input type="hidden" name="zip2" value="{{ $data->zip2 }}">
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">住所</label>
                <label class="col-xs-12 col-md-8 control-value">{{ $data->address }}</label>
                <input type="hidden" name="address" value="{{ $data->address }}">
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">電話番号</label>
                <label class="col-xs-12 col-md-8 control-value">{{ $data->phone }}</label>
                <input type="hidden" name="phone" value="{{ $data->phone }}">
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">メールアドレス　<span class="pull-right-container"><small class="label pull-right bg-red" style="margin-top:4px;">必須</small></span></label>
                <label class="col-xs-12 col-md-8 control-value">{{ $data->email }}</label>
                <input type="hidden" name="email" value="{{ $data->email }}">
              </div>
              <div class="form-group">
                <label class="col-xs-12 col-md-2 control-label">その他・備考・説明 等</label>
                <label class="col-xs-12 col-md-8 control-value">{!! nl2br($data->note) !!}</label>
                <input type="hidden" name="note" value="{{ $data->note }}">
              </div>
            </div>
          </div>
          <div class="box-footer">
            <div class="pull-right">
              <input type="submit" class="btn btn-block btn-success btn-sm" value="　登　　録　">
            </div>
            <div class="pull-right" style="margin-right: 8px;">
              <input type="button" class="btn btn-block btn-default btn-sm" onclick="history.back()" value="　戻　　る　">
            </div>
          </div>
        </form>
      </div>
    </div>
  </section>
  <!-- /.content -->
@endsection

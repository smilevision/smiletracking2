#!/bin/sh

# ==============================
# 事前準備
# 1. mysqldumpで、xmlファイルを「data」フォルダに出力する。
#    mysqldump -u smiletracking -p --xml --no-data smiletracking > smiletracking.xml
# 2. Sequel proで、dotファイルを「data」フォルダに出力する。
# ==============================

# DB設計書出力
xsltproc -o ./smiletracking.md style.xsl ./data/smiletracking.xml

# ER図出力
dot -Tpng ./data/smiletracking.dot > ./smiletracking.png

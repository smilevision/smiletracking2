

# テーブル定義


## ■ company
TBL物理名|TBL論理名（コメント）
--------|--------
company|出展企業情報

#### カラム情報
物理名|データ型|NULL|デフォルト|キー|論理名（コメント）|Extra
----|----|----|----|----|---|---|
id|bigint(20) unsigned|NO||PRI|出展企業ID|auto_increment|
name|varchar(1024)|NO|||企業・団体名||
tanto|varchar(128)|NO|||担当者名||
zip|varchar(10)|YES|||郵便番号||
address|varchar(1024)|NO|||住所||
phone|varchar(24)|NO|||電話番号||
email|varchar(128)|NO|||メールアドレス||
is_join|tinyint(1)|NO|0||参加フラグ||
created_at|datetime|NO|CURRENT_TIMESTAMP||作成日時||
updated_at|datetime|NO|CURRENT_TIMESTAMP||更新日時||
is_deleetd|tinyint(1)|NO|0||削除フラグ||
deleted_at|datetime|YES|||削除日時||


#### インデックス情報
インデックス名|カラム|複合キー順序|NULL|UNIQ
----|----|----|----|----
PRIMARY|id|1|NO|YES|


## ■ company_events_relation
TBL物理名|TBL論理名（コメント）
--------|--------
company_events_relation|

#### カラム情報
物理名|データ型|NULL|デフォルト|キー|論理名（コメント）|Extra
----|----|----|----|----|---|---|
company_id|bigint(20) unsigned|NO||MUL|企業・団体名ID||
events_id|bigint(20) unsigned|NO||MUL|イベントID||


#### インデックス情報
インデックス名|カラム|複合キー順序|NULL|UNIQ
----|----|----|----|----
company_id|company_id|1|NO|NO|
events_id|events_id|1|NO|NO|


## ■ contents
TBL物理名|TBL論理名（コメント）
--------|--------
contents|予約ページコンテンツ

#### カラム情報
物理名|データ型|NULL|デフォルト|キー|論理名（コメント）|Extra
----|----|----|----|----|---|---|
id|bigint(20) unsigned|NO||PRI|コンテンツID|auto_increment|
event_id|bigint(20) unsigned|NO||MUL|イベントID||
title|varchar(1024)|NO|||タイトル||
is_require|tinyint(1)|NO|0||必須有無||
tag|int(11)|NO|||HTMLタグ||
type|varchar(128)|YES|||type||
name|varchar(1024)|YES|||name||
class|varchar(1024)|YES|||クラス名||
placeholder|varchar(1024)|YES|||placeholder||
rows|varchar(128)|YES|||textareaのrows||
number|int(11)|NO|||順序||
created_at|datetime|NO|CURRENT_TIMESTAMP||作成日時||
updated_at|datetime|NO|CURRENT_TIMESTAMP||更新日時||
is_deleted|tinyint(1)|NO|0||削除フラグ||
deleted_at|datetime|YES|||削除日時||


#### インデックス情報
インデックス名|カラム|複合キー順序|NULL|UNIQ
----|----|----|----|----
PRIMARY|id|1|NO|YES|
event_id|event_id|1|NO|NO|


## ■ contents_validation
TBL物理名|TBL論理名（コメント）
--------|--------
contents_validation|

#### カラム情報
物理名|データ型|NULL|デフォルト|キー|論理名（コメント）|Extra
----|----|----|----|----|---|---|
id|bigint(20) unsigned|NO||PRI|バリデーションID|auto_increment|
contents_id|bigint(20) unsigned|NO||MUL|コンテンツID||
rule|varchar(1024)|NO|||ルール||
message|varchar(2048)|YES|||メッセージ||
created_at|datetime|NO|CURRENT_TIMESTAMP||作成日時||
updated_at|datetime|NO|CURRENT_TIMESTAMP||更新日時||
is_deleted|tinyint(1)|NO|0||削除フラグ||
deleted_at|datetime|YES|||削除日時||


#### インデックス情報
インデックス名|カラム|複合キー順序|NULL|UNIQ
----|----|----|----|----
PRIMARY|id|1|NO|YES|
contents_id|contents_id|1|NO|NO|


## ■ contents_values
TBL物理名|TBL論理名（コメント）
--------|--------
contents_values|

#### カラム情報
物理名|データ型|NULL|デフォルト|キー|論理名（コメント）|Extra
----|----|----|----|----|---|---|
id|bigint(20) unsigned|NO||PRI|ID|auto_increment|
contents_id|bigint(20) unsigned|NO||MUL|コンテンツID||
value|varchar(1024)|NO|||値||
created_at|datetime|NO|CURRENT_TIMESTAMP||作成日時||
updated_at|datetime|NO|CURRENT_TIMESTAMP||更新日時||
is_deleted|tinyint(1)|NO|0||削除フラグ||
deleted_at|datetime|YES|||削除日時||


#### インデックス情報
インデックス名|カラム|複合キー順序|NULL|UNIQ
----|----|----|----|----
PRIMARY|id|1|NO|YES|
contents_id|contents_id|1|NO|NO|


## ■ entry
TBL物理名|TBL論理名（コメント）
--------|--------
entry|エントリー管理

#### カラム情報
物理名|データ型|NULL|デフォルト|キー|論理名（コメント）|Extra
----|----|----|----|----|---|---|
id|bigint(20) unsigned|NO||PRI||auto_increment|
event_id|bigint(20) unsigned|NO||MUL|イベントID||
uuid|varchar(255)|YES|uuid||UUID||
code|varchar(8)|NO||UNI|予約コード||
name|varchar(255)|NO|||氏名||
kname|varchar(255)|NO|||氏名（カナ）||
gender|tinyint(1)|NO|||（１：男性、２：女性、０：未回答）||
birth|date|NO|||生年月日||
zip|varchar(8)|NO|||郵便番号||
address1|varchar(128)|NO|||都道府県||
address2|varchar(1024)|NO|||住所||
address3|varchar(1024)|YES|||住所（マンション名など）||
email|varchar(255)|NO|||メールアドレス||
phone|varchar(16)|YES|||電話番号||
division|varchar(64)|NO|||在籍区分（１：大学、２：大学院、３：短大・専門学校、４：既卒、９：その他）||
school|varchar(128)|NO|||学校名||
gakubu|varchar(128)|YES|||学部||
school_year|varchar(64)|NO|||学年||
graduate_year|varchar(64)|NO|||卒業見込み年度||
note|text|YES|||備考欄||
created_at|datetime|NO|CURRENT_TIMESTAMP||作成日時||
updated_at|datetime|NO|CURRENT_TIMESTAMP||更新日時||
is_deleted|tinyint(1)|NO|0||削除フラグ||
deleted_at|datetime|YES|||削除日時||


#### インデックス情報
インデックス名|カラム|複合キー順序|NULL|UNIQ
----|----|----|----|----
PRIMARY|id|1|NO|YES|
code|code|1|NO|YES|
event_id|event_id|1|NO|NO|


## ■ events
TBL物理名|TBL論理名（コメント）
--------|--------
events|イベント情報

#### カラム情報
物理名|データ型|NULL|デフォルト|キー|論理名（コメント）|Extra
----|----|----|----|----|---|---|
id|bigint(20) unsigned|NO||PRI|イベントID|auto_increment|
parent_id|bigint(20) unsigned|YES|||親イベントID||
code|varchar(8)|NO||UNI|主催者コード||
name|varchar(256)|NO|||イベント名||
started_at|datetime|NO|||開始日時||
ended_at|datetime|NO|||終了日時||
reserv_started_at|datetime|NO|||予約 開始日時||
reserv_ended_at|datetime|NO|||予約 終了日時||
accepting|int(11)|YES|||当日受付開始||
created_at|datetime|NO|CURRENT_TIMESTAMP||作成日時||
updated_at|datetime|NO|CURRENT_TIMESTAMP||更新日時||
is_deleted|tinyint(1)|NO|0||削除フラグ||
deleted_at|datetime|YES|||削除日時||


#### インデックス情報
インデックス名|カラム|複合キー順序|NULL|UNIQ
----|----|----|----|----
PRIMARY|id|1|NO|YES|
code|code|1|NO|YES|


## ■ events_users_relation
TBL物理名|TBL論理名（コメント）
--------|--------
events_users_relation|

#### カラム情報
物理名|データ型|NULL|デフォルト|キー|論理名（コメント）|Extra
----|----|----|----|----|---|---|
event_id|bigint(20) unsigned|NO||MUL|||
users_id|bigint(20) unsigned|NO||MUL|||


#### インデックス情報
インデックス名|カラム|複合キー順序|NULL|UNIQ
----|----|----|----|----
event_id|event_id|1|NO|NO|
users_id|users_id|1|NO|NO|


## ■ exhibitors
TBL物理名|TBL論理名（コメント）
--------|--------
exhibitors|出展企業情報

#### カラム情報
物理名|データ型|NULL|デフォルト|キー|論理名（コメント）|Extra
----|----|----|----|----|---|---|
id|bigint(20) unsigned|NO||PRI|出展企業ID|auto_increment|
event_id|bigint(20) unsigned|NO||MUL|イベントID||
code|varchar(8)|NO||UNI|出展企業コード||
name|varchar(1024)|NO|||企業・団体名||
tanto|varchar(128)|YES|||担当者名||
zip1|varchar(5)|YES|||郵便番号||
zip2|varchar(5)|YES|||郵便番号||
address|varchar(1024)|YES|||住所||
phone|varchar(24)|YES|||電話番号||
email|varchar(128)|YES|||メールアドレス||
note|text|YES|||その他備考、説明等||
is_join|tinyint(1)|NO|0||参加フラグ||
created_at|datetime|NO|CURRENT_TIMESTAMP||作成日時||
updated_at|datetime|NO|CURRENT_TIMESTAMP||更新日時||
is_deleted|tinyint(1)|NO|0||削除フラグ||
deleted_at|datetime|YES|||削除日時||


#### インデックス情報
インデックス名|カラム|複合キー順序|NULL|UNIQ
----|----|----|----|----
PRIMARY|id|1|NO|YES|
code|code|1|NO|YES|
event_id|event_id|1|NO|NO|


## ■ exhibitors_user_relation
TBL物理名|TBL論理名（コメント）
--------|--------
exhibitors_user_relation|出展企業・ユーザー連携（出展者ログイン時に使用）

#### カラム情報
物理名|データ型|NULL|デフォルト|キー|論理名（コメント）|Extra
----|----|----|----|----|---|---|
exhibitor_id|bigint(20) unsigned|NO||MUL|||
users_id|bigint(20) unsigned|NO||MUL|||


#### インデックス情報
インデックス名|カラム|複合キー順序|NULL|UNIQ
----|----|----|----|----
exhibitor_id|exhibitor_id|1|NO|NO|
users_id|users_id|1|NO|NO|


## ■ migrations
TBL物理名|TBL論理名（コメント）
--------|--------
migrations|

#### カラム情報
物理名|データ型|NULL|デフォルト|キー|論理名（コメント）|Extra
----|----|----|----|----|---|---|
id|int(10) unsigned|NO||PRI||auto_increment|
migration|varchar(255)|NO|||||
batch|int(11)|NO|||||


#### インデックス情報
インデックス名|カラム|複合キー順序|NULL|UNIQ
----|----|----|----|----
PRIMARY|id|1|NO|YES|


## ■ password_resets
TBL物理名|TBL論理名（コメント）
--------|--------
password_resets|

#### カラム情報
物理名|データ型|NULL|デフォルト|キー|論理名（コメント）|Extra
----|----|----|----|----|---|---|
email|varchar(255)|NO||MUL|||
token|varchar(255)|NO|||||
created_at|timestamp|YES|||||


#### インデックス情報
インデックス名|カラム|複合キー順序|NULL|UNIQ
----|----|----|----|----
password_resets_email_index|email|1|NO|NO|


## ■ place
TBL物理名|TBL論理名（コメント）
--------|--------
place|会場情報

#### カラム情報
物理名|データ型|NULL|デフォルト|キー|論理名（コメント）|Extra
----|----|----|----|----|---|---|
id|bigint(20) unsigned|NO||PRI|会場ID|auto_increment|
event_id|bigint(20) unsigned|NO||MUL|イベントID||
name|varchar(128)|NO|||会場名||
zip1|varchar(5)|YES|||郵便番号||
zip2|varchar(5)|YES|||郵便番号||
address|varchar(2048)|NO|||会場住所||
phone|varchar(24)|NO|||電話番号||
created_at|datetime|NO|CURRENT_TIMESTAMP||作成日時||
updated_at|datetime|NO|CURRENT_TIMESTAMP||更新日時||
is_deleted|tinyint(1)|NO|0||削除フラグ||
deleted_at|datetime|YES|||削除日時||


#### インデックス情報
インデックス名|カラム|複合キー順序|NULL|UNIQ
----|----|----|----|----
PRIMARY|id|1|NO|YES|
event_id|event_id|1|NO|NO|


## ■ questionnaire
TBL物理名|TBL論理名（コメント）
--------|--------
questionnaire|アンケート

#### カラム情報
物理名|データ型|NULL|デフォルト|キー|論理名（コメント）|Extra
----|----|----|----|----|---|---|
id|bigint(20) unsigned|NO||PRI|アンケートID|auto_increment|
entry_id|bigint(20) unsigned|NO||MUL|エントリーID||
created_at|datetime|NO|CURRENT_TIMESTAMP||作成日時||
updated_at|datetime|NO|CURRENT_TIMESTAMP||更新日時||
is_deleted|tinyint(1)|NO|0||削除フラグ||
deleted_at|datetime|YES|||削除日時||


#### インデックス情報
インデックス名|カラム|複合キー順序|NULL|UNIQ
----|----|----|----|----
PRIMARY|id|1|NO|YES|
entry_id|entry_id|1|NO|NO|


## ■ reception
TBL物理名|TBL論理名（コメント）
--------|--------
reception|イベント受付情報

#### カラム情報
物理名|データ型|NULL|デフォルト|キー|論理名（コメント）|Extra
----|----|----|----|----|---|---|
id|bigint(20) unsigned|NO||PRI||auto_increment|
entry_id|bigint(20) unsigned|NO||MUL|エントリーID||
event_id|bigint(20) unsigned|NO||MUL|イベントID||
enter_at|datetime|YES|||入場日時||
exit_at|datetime|YES|||退場日時||
created_at|datetime|NO|CURRENT_TIMESTAMP||作成日時||
updated_at|datetime|NO|CURRENT_TIMESTAMP||更新日時||
is_deleted|tinyint(1)|NO|0||削除フラグ||
deleted_at|datetime|YES|||削除日時||


#### インデックス情報
インデックス名|カラム|複合キー順序|NULL|UNIQ
----|----|----|----|----
PRIMARY|id|1|NO|YES|
entry_id|entry_id|1|NO|NO|
event_id|event_id|1|NO|NO|


## ■ users
TBL物理名|TBL論理名（コメント）
--------|--------
users|

#### カラム情報
物理名|データ型|NULL|デフォルト|キー|論理名（コメント）|Extra
----|----|----|----|----|---|---|
id|bigint(20) unsigned|NO||PRI|ユーザーID|auto_increment|
is_authority|int(11)|NO|||権限（１：管理者、２：イベンター、３：出展企業担当、４：ユーザー）||
code|varchar(7)|NO||UNI|ユーザーコード||
uuid|varchar(255)|YES|||||
name|varchar(255)|NO|||氏名||
email|varchar(255)|YES|||メールアドレス||
email_verified_at|timestamp|YES|||メールアドレス確認||
password|varchar(255)|NO|||パスワード||
remember_token|varchar(100)|YES|||トークンキー||
created_at|timestamp|YES|||削除日時||
updated_at|timestamp|YES|||更新日時||
is_deleted|tinyint(1)|NO|0||削除フラグ||
deleted_at|datetime|YES|||削除日時||


#### インデックス情報
インデックス名|カラム|複合キー順序|NULL|UNIQ
----|----|----|----|----
PRIMARY|id|1|NO|YES|
code|code|1|NO|YES|


## ■ users_detail
TBL物理名|TBL論理名（コメント）
--------|--------
users_detail|イベント担当者情報

#### カラム情報
物理名|データ型|NULL|デフォルト|キー|論理名（コメント）|Extra
----|----|----|----|----|---|---|
id|bigint(20) unsigned|NO||PRI|担当者ID|auto_increment|
users_id|bigint(20) unsigned|NO||MUL|ユーザーID||
contact|varchar(1024)|YES|||担当者コンタクト（電話・SNS等）||
created_at|datetime|NO|CURRENT_TIMESTAMP||作成日時||
updated_at|datetime|NO|CURRENT_TIMESTAMP||更新日時||
is_deleted|tinyint(1)|NO|0||削除フラグ||
deleted_at|datetime|YES|||削除日時||


#### インデックス情報
インデックス名|カラム|複合キー順序|NULL|UNIQ
----|----|----|----|----
PRIMARY|id|1|NO|YES|
users_id|users_id|1|NO|NO|


## ■ visit
TBL物理名|TBL論理名（コメント）
--------|--------
visit|企業訪問受付

#### カラム情報
物理名|データ型|NULL|デフォルト|キー|論理名（コメント）|Extra
----|----|----|----|----|---|---|
id|bigint(20) unsigned|NO||PRI|訪問受付ID|auto_increment|
exhibitors_id|bigint(20) unsigned|NO||MUL|出展企業ID||
entry_id|bigint(20) unsigned|NO||MUL|エントリーID||
created_at|datetime|NO|CURRENT_TIMESTAMP||作成日時||
updated_at|datetime|NO|CURRENT_TIMESTAMP||更新日時||
is_deleted|tinyint(1)|NO|0||削除フラグ||
deleted_at|datetime|YES|||削除日時||


#### インデックス情報
インデックス名|カラム|複合キー順序|NULL|UNIQ
----|----|----|----|----
PRIMARY|id|1|NO|YES|
exhibitors_id|exhibitors_id|1|NO|NO|
entry_id|entry_id|1|NO|NO|

